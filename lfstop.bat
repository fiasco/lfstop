@echo off
:BEGIN
lfstop.exe --restart-possible
if errorlevel 21 (
   echo "returned code %errorlevel%, restarting"
   goto BEGIN:
)

echo "returned code %errorlevel%, shutting down"
pause
