// Copyright 2015 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of LFSTop.
//
// LFSTop is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LFSTop is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LFSTop.  If not, see <http://www.gnu.org/licenses/>.


#include <QFile>
#include <QIODevice>
#include <QFileInfo>
#include <QSharedMemory>
#include <QSystemSemaphore>
#include <QDir>


#include "Databases/UsersDb.hpp"
#include "Utils/MKeyFile.hpp"
#include "Settings.hpp"
#include "Databases/DatabasesManager.hpp"
#include "Databases/LapsDbStorage.hpp"
#include "LfsInsimUtils.hpp"
#include "Logging/Log.hpp"
#include "Logging/LogFile.hpp"
#include "Logging/LogStdOut.hpp"

#include <iostream>

bool is_tab_file_format (QFile &pFile) {
    bool is_tab = false;
    while (!pFile.atEnd()) {
        const QByteArray l = pFile.readLine();

        if(l.startsWith('#') || l.isEmpty()) {
            continue;
        }

        if (l.contains('\t')) {
            is_tab = true;
            break;
        } else {
            is_tab = false;
            break;
        }
    }

    pFile.seek(0);
    return is_tab;
}

void import_users_database_tab_separated (Log &pLog,
                                          QFile &pFile) {
    LfsInsimUtils::StringDecoder decoder;
    UsersDb users_db(pLog);

    size_t imported_users_num = 0;

    while (!pFile.atEnd()) {
        const QByteArray l = pFile.readLine().trimmed();

        if(l.startsWith('#') || l.isEmpty()) {
            continue;
        }

        const QByteArrayList ll = l.split('\t');

        users_db.userSetNickname(ll[0],
                                 decoder.airioNicknameToLfs(ll[1]));
        ++imported_users_num;
    }

    pLog() << "imported " << imported_users_num << " user nicknames";
}

void import_users_database_newline_separated (Log &pLog,
                                              QFile &pFile) {
    LfsInsimUtils::StringDecoder decoder;
    UsersDb users_db(pLog);

    int current_token_n = 0;

    size_t imported_users_num = 0;

    QByteArray username;

    while (!pFile.atEnd()) {
        const QByteArray l = pFile.readLine().trimmed();

        if(l.startsWith('#') || l.isEmpty()) {
            current_token_n = 0;
            continue;
        }

        if (current_token_n == 0) {
            username = l;
        } else if (current_token_n == 1) {
            users_db.userSetNickname(username,
                                     decoder.airioNicknameToLfs(l));
            ++imported_users_num;
        }
        ++current_token_n;
    }

    pLog() << "imported " << imported_users_num << " user nicknames";
}

LapsDbStorage::Laptime build_laptime (const QByteArray &usr,
                                      const QByteArray &sp1str,
                                      const QByteArray &sp2str,
                                      const QByteArray &sp3str,
                                      const QByteArray &sp4str,
                                      const QByteArray &timestamp) {
    const LaptimeT sp1 = sp1str.toUInt() * 10;
    const LaptimeT sp2 = sp2str.toUInt() * 10;
    const LaptimeT sp3 = sp3str.toUInt() * 10;
    const LaptimeT lt = sp4str.toUInt() * 10;
    const qint64 ts = timestamp.toLongLong();

    std::vector<LaptimeT> splits;

    if (sp1 != lt) {
        splits.push_back(sp1);
    }

    if (sp2 != lt) {
        splits.push_back(sp2);
    }

    if (sp3 != lt) {
        splits.push_back(sp3);
    }

    return { splits, lt, usr, ts };
}

void import_laptimes_tab_separated (Log &pLog,
                                    QFile &pFile,
                                    LfsDatabasesManager &pDbManager) {
    QMap<QByteArray, std::shared_ptr<LapsDb>> dbs;

    size_t imported_laptimes_num = 0;

    while (!pFile.atEnd()) {
        const QByteArray l = pFile.readLine().trimmed();

        if(l.startsWith('#') || l.isEmpty()) {
            continue;
        }

        QByteArrayList items = l.split(' ');
        if (!dbs.contains(items[1])) {
            dbs[items[1]] = pDbManager.getDatabase(items[1].toStdString());
        }

        const LapsDbAddAnswer answer=
                dbs[items[1]]->addLaptime("top",
                                          dbs[items[1]]->getCarByName(items[2]),
                                          TyreT::airioTyres(),
                                          build_laptime(items[0],
                                                        items[4],
                                                        items[5],
                                                        items[6],
                                                        items[7],
                                                        items[8]),
                                          false);
        if (answer.isAdded) {
            ++imported_laptimes_num;
        }
    }

    pLog(Log::INFO) << "imported " << imported_laptimes_num
                    << " laptimes from " << pFile.fileName();
}

void import_laptimes_newline_separated (Log &pLog,
                                        QFile &pFile,
                                        LfsDatabasesManager &pDbManager) {
    QMap<QByteArray, std::shared_ptr<LapsDb>> dbs;

    int current_token_n = 0;
    QByteArray l;

    QByteArrayList items;

    size_t imported_laptimes_num = 0;

    while (!pFile.atEnd()) {
        l = pFile.readLine().trimmed();

        if(l.startsWith('#') || l.isEmpty()) {
            current_token_n = 0;
            continue;
        }

        if (current_token_n < 32) {
            items.append(l);
        } else if (current_token_n == 32) {
            if (!dbs.contains(items[1])) {
                dbs[items[1]] = pDbManager.getDatabase(items[1].toStdString());
            }

            const LapsDbAddAnswer answer=
                dbs[items[1]]->addLaptime("top",
                                          dbs[items[1]]->getCarByName(items[2]),
                                          TyreT::airioTyres(),
                                          build_laptime(items[0],
                                                        items[4],
                                                        items[5],
                                                        items[6],
                                                        items[7],
                                                        items[8]),
                                          false);

            if (answer.isAdded) {
                ++imported_laptimes_num;
            }

            items.clear();
            current_token_n = 0;
            continue;
        }
        ++current_token_n;
    }

    pLog() << "imported " << imported_laptimes_num
           << " laptimes from " << pFile.fileName();
}

int main () {
    LogFile f("logs/airio_import.log");
    LogStdOut std_out;
    Log log("airio_import",
            f, "%datetime %level %msg",
            std_out, "%datetime %level %msg");



    QFileInfo inf(".");
    QString path = inf.canonicalFilePath();

    bool isRunning = false;
    QSystemSemaphore sema("LfsTopSemaphore " + path, 1);
    sema.acquire();

    {
        // cleanup the shared memory if we crashed
        QSharedMemory shmem("LfsTopMutex " + path);
        shmem.attach();
    }

    QSharedMemory shmem("LfsTopMutex " + path);
    if (shmem.attach()) {
        isRunning = true;
    } else {
        shmem.create(1);
    }

    sema.release();

    if (isRunning) {
        log(Log::ERROR) << "LfsTop (or importer) is running in " << path;
        return -1;
    }

    QDir dir("logs");
    if (!dir.exists()) {
        dir.mkpath(".");
    }


    QFile users_file("Airio.sta.un.txt");

    if(users_file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        if (is_tab_file_format(users_file)) {
            import_users_database_tab_separated(log, users_file);
        } else {
            import_users_database_newline_separated(log, users_file);
        }
    } else {
        log() << "Not importing usernames, can not open Airio.sta.un.txt";
    }

    MKeyFile config_file ("config.ini");
    Settings::SettingsMap settings_map (Settings::get_settings_map(config_file));
    LfsDatabasesManager db_manager (log, config_file, settings_map, true);

    QFile lt_file("Airio.sta.cr.txt");
    QFile oc_lt_file("Airio.sta.ac.txt");

    if(lt_file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        if (is_tab_file_format(lt_file)) {
            import_laptimes_tab_separated(log, lt_file, db_manager);
        } else {
            import_laptimes_newline_separated(log, lt_file, db_manager);
        }
    }
    else {
        log() << "Not importing laptimes, can not open Airio.sta.cr.txt";
    }

    if(oc_lt_file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        if (is_tab_file_format(oc_lt_file)) {
            import_laptimes_tab_separated(log, oc_lt_file, db_manager);
        } else {
            import_laptimes_newline_separated(log, oc_lt_file, db_manager);
        }
    } else {
        log() << "Not importing open config laptimes, "
                 "can not open Airio.sta.ac.txt";
    }

    return 0;
}
