#!/bin/bash

lfstop_executable_path="./lfstop"



if [ "$#" -eq 1 ]; then
    lfstop_executable_path="$1"
fi

exit_status=21
while [ "$exit_status" -eq 21 ]
do
    "$lfstop_executable_path" --restart-possible
    exit_status="$?"
    echo -n "returned code $exit_status, "
    if [ "$exit_status" -eq 21 ]
    then
        echo "restarting"
    else
        echo "shutting down"
    fi
done

exit $exit_status
