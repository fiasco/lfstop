// Copyright 2015 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of LFSTop.
//
// LFSTop is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LFSTop is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LFSTop.  If not, see <http://www.gnu.org/licenses/>.


#include "LfsInsimUtils.hpp"


#include <cstring>
#include <cctype>


#include <vector>
#include <utility>


#include "LfsInsim.hpp"
#include "Utils/StringUtils.hpp"
#include "Utils/StdVectorUtils.hpp"
#include "LfsEncodings.hpp"


namespace LfsInsimUtils {
typedef std::pair<std::string, std::string> StringPair;

std::string getPacketName (const unsigned char *pPacketPtr,
                           const bool pAppendDescription) {
   static struct S {
       std::vector<StringPair > v;
       std::vector<StringPair > vt;
       std::vector<StringPair > vs;

       S() :
           v (std::vector<StringPair >{
               {"ISP_NONE", "not used"},
               {"ISP_ISI", "insim initialise"},
               {"ISP_VER", "version info"},
               {"ISP_TINY", "multi purpose"},
               {"ISP_SMALL", "multi purpose"},
               {"ISP_STA", "state info"},
               {"ISP_SCH", "single character"},
               {"ISP_SFP", "state flags pack"},
               {"ISP_SCC", "set car camera"},
               {"ISP_CPP", "cam pos pack"},
               {"ISP_ISM", "start multiplayer"},
               {"ISP_MSO", "message out"},
               {"ISP_III", "hidden /i message"},
               {"ISP_MST", "type message or /command"},
               {"ISP_MTC", "message to a connection"},
               {"ISP_MOD", "set screen mode"},
               {"ISP_VTN", "vote notification"},
               {"ISP_RST", "race start"},
               {"ISP_NCN", "new connection"},
               {"ISP_CNL", "connection left"},
               {"ISP_CPR", "connection renamed"},
               {"ISP_NPL", "new player (joined race)"},
               {"ISP_PLP", "player pit (keeps slot in race)"},
               {"ISP_PLL", "player leave (spectate - loses slot)"},
               {"ISP_LAP", "lap time"},
               {"ISP_SPX", "split x time"},
               {"ISP_PIT", "pit stop start"},
               {"ISP_PSF", "pit stop finish"},
               {"ISP_PLA", "pit lane enter / leave"},
               {"ISP_CCH", "camera changed"},
               {"ISP_PEN", "penalty given or cleared"},
               {"ISP_TOC", "take over car"},
               {"ISP_FLG", "flag (yellow or blue)"},
               {"ISP_PFL", "player flags (help flags)"},
               {"ISP_FIN", "finished race"},
               {"ISP_RES", "result confirmed"},
               {"ISP_REO", "reorder (info or instruction)"},
               {"ISP_NLP", "node and lap packet"},
               {"ISP_MCI", "multi car info"},
               {"ISP_MSX", "type message"},
               {"ISP_MSL", "message to local computer"},
               {"ISP_CRS", "car reset"},
               {"ISP_BFN", "delete buttons / receive button requests"},
               {"ISP_AXI", "autocross layout information"},
               {"ISP_AXO", "hit an autocross object"},
               {"ISP_BTN", "show a button on local or remote screen"},
               {"ISP_BTC", "sent when a user clicks a button"},
               {"ISP_BTT", "sent after typing into a button"},
               {"ISP_RIP", "replay information packet"},
               {"ISP_SSH", "screenshot"},
               {"ISP_CON", "contact between cars (collision report)"},
               {"ISP_OBH", "contact car + object (collision report)"},
               {"ISP_HLV", "report incidents that would violate HLVC"},
               {"ISP_PLC", "player cars"},
               {"ISP_AXM", "autocross multiple objects"},
               {"ISP_ACR", "admin command report"},
               {"ISP_HCP", "car handicaps"},
               {"ISP_NCI", "new connection - extra info for host"}
           }),
           vt (std::vector<StringPair >{
               {"TINY_NONE", "keep alive (see \"maintaining the connection\")"},
               {"TINY_VER", "get version"},
               {"TINY_CLOSE", "close insim"},
               {"TINY_PING", "external progam requesting a reply"},
               {"TINY_REPLY", "reply to a ping request"},
               {"TINY_VTC", "game vote cancel (info or request)"},
               {"TINY_SCP", "send camera pos"},
               {"TINY_SST", "send state info"},
               {"TINY_GTH", "get time in hundredths (i.e. SMALL_RTP)"},
               {"TINY_MPE", "multi player end"},
               {"TINY_ISM", "get multiplayer info (i.e. ISP_ISM)"},
               {"TINY_REN", "race end (return to race setup screen)"},
               {"TINY_CLR", "all players cleared from race"},
               {"TINY_NCN", "get NCN for all connections"},
               {"TINY_NPL", "get all players"},
               {"TINY_RES", "get all results"},
               {"TINY_NLP", "send an IS_NLP"},
               {"TINY_MCI", "send an IS_MCI"},
               {"TINY_REO", "send an IS_REO"},
               {"TINY_RST", "send an IS_RST"},
               {"TINY_AXI", "send an IS_AXI - AutoX Info"},
               {"TINY_AXC", "autocross cleared"},
               {"TINY_RIP", "send an IS_RIP - Replay Information Packet"},
               {"TINY_NCI", "get NCI for all guests (on host only)"}
           }),
           vs (std::vector<StringPair >{
               {"SMALL_NONE", "not used"},
               {"SMALL_SSP", "start sending positions"},
               {"SMALL_SSG", "start sending gauges"},
               {"SMALL_VTA", "vote action"},
               {"SMALL_TMS", "time stop"},
               {"SMALL_STP", "time step"},
               {"SMALL_RTP", "race time packet (reply to GTH)"},
               {"SMALL_NLI", "set node lap interval"}
           })
       {
       }
    } stor;

   std::string description = "Unknown";
   std::string name = "Unknown";
   if (pPacketPtr[1] < stor.v.size()) {
       name = stor.v[pPacketPtr[1]].first;
       description =  stor.v[pPacketPtr[1]].second;
   }
   if (pPacketPtr[1] == ISP_TINY) {
       if (pPacketPtr[3] < stor.vt.size()) {
           name = stor.vt[pPacketPtr[3]].first;
           description = stor.vt[pPacketPtr[3]].second;
       }
   }
   else if (pPacketPtr[1] == ISP_SMALL) {
       if (pPacketPtr[3] < stor.vs.size()) {
           name = stor.vs[pPacketPtr[3]].first;
           description = stor.vs[pPacketPtr[3]].second;
       }
   }

   return name + (pAppendDescription ? ": " + description : "");
}

std::string stringStrippedColors (const std::string &pString) {
    size_t pos = 0;
    std::string ret_str = pString;
    while (pos != ret_str.npos) {
        pos = ret_str.find('^', pos);
        if (pos != std::string::npos) {
            if ((pos + 1) != ret_str.size()) {
                if (ret_str[pos + 1] >= '0' && ret_str[pos + 1] <= '8') {
                    ret_str.erase(pos, 2);
                }
                else {
                    pos += 2;
                }
            }
        }
    }
    return ret_str;
}

QString StringDecoder::decode (const char *pData,
                               const int pLen,
                               const char pLfsMagicByte) {
    return d.value(pLfsMagicByte)->toUnicode(pData, pLen);
}

QString StringDecoder::lfsStringToUnicode (const QByteArray &s) {
    QString ret_str;
    char current_charset = 'L';
    int start_pos = 0;
    while (start_pos < s.length()) {
        int piece_end_pos = s.indexOf('^', start_pos);
        if (piece_end_pos == -1) {
            piece_end_pos = s.size();
        }

        ret_str += decode(s.constData() + start_pos,
                          piece_end_pos - start_pos,
                          current_charset);

        if (piece_end_pos < s.size()) {
            if (std::isdigit(s[piece_end_pos + 1])) {
                // ignoring colors
            }
            else if (escapes.contains(s[piece_end_pos + 1])){
                ret_str += escapes[s[piece_end_pos + 1]];
            }
            else if (d.contains(s[piece_end_pos + 1])) {
                current_charset = s[piece_end_pos + 1];
            }
        }
        start_pos = piece_end_pos + 2;
    }
    return ret_str;
}

std::string stringNonEscapedNonColored(const std::string &pString) {
    size_t pos = 0;
    std::string ret_str = pString;
    while (pos != ret_str.npos) {
        pos = ret_str.find('^', pos);
        if (pos != std::string::npos) {
            if ((pos + 1) != ret_str.size()) {
                if (ret_str[pos + 1] >= '0' && ret_str[pos + 1] <= '8') {
                    ret_str.erase(pos, 2);
                }
                else if (StringDecoder::escapes.contains(ret_str[pos + 1])) {
                    ret_str.insert(pos,
                                   std::string(1, StringDecoder::escapes[ret_str[pos + 1]]));
                    ret_str.erase(pos + 1, 2);
                    ++pos;
                }
                else {
                    pos += 2;
                }
            }
        }
    }
    return ret_str;
}

QByteArray stringStrippedColors (const QByteArray &pString) {
    int pos = 0;
    QByteArray ret_str = pString;
    while (pos < ret_str.size()) {
        pos = ret_str.indexOf('^', pos);
        if (pos != -1) {
            if ((pos + 1) != ret_str.size()) {
                if (ret_str[pos + 1] >= '0' && ret_str[pos + 1] <= '8') {
                    ret_str.remove(pos, 2);
                }
                else {
                    pos += 2;
                }
            }
        }
    }
    return ret_str;
}

QByteArray stringNonEscapedNonColored (const QByteArray &pString) {
    int pos = 0;
    QByteArray ret_str = pString;
    while (pos < ret_str.size()) {
        pos = ret_str.indexOf('^', pos);
        if (pos == -1) {
            break;
        }
        if ((pos + 1) != ret_str.size()) {
            if (ret_str[pos + 1] >= '0' && ret_str[pos + 1] <= '8') {
                ret_str.remove(pos, 2);
            }
            else if (StringDecoder::escapes.contains(ret_str[pos + 1])) {
                ret_str.insert(pos,
                               StringDecoder::escapes[ret_str[pos + 1]]);
                ret_str.remove(pos + 1, 2);
                ++pos;
            }
            else {
                pos += 2;
            }
        }
    }
    return ret_str;
}

std::vector<QPair<bool, QByteArray> >
getCarCommandArguments (const QByteArray &command) {
    std::vector<QPair<bool, QByteArray>> ret;

    bool is_allowed = true;
    for (int pos = 0; pos < command.size();) {
        int next_specifier =
                StringUtils::QByteArrayFindFirstOfTwo(command, '+', '-', pos);

        ret.push_back({is_allowed, command.mid(pos, next_specifier - pos)});
        if (next_specifier == -1) {
            return ret;
        } else if (command[next_specifier] == '+') {
            is_allowed = true;
        } else {
            is_allowed = false;
        }
        pos = next_specifier + 1;
    }
    return ret;
}

IS_HCP createHandicapsPacket (const QMap<CarT, CarDefinition> &pCars) {
    return { 68, ISP_HCP,0 , 0,
        { pCars[CarT::XFG()].getAsLfsCarHcp(),
          pCars[CarT::XRG()].getAsLfsCarHcp(),
          pCars[CarT::XRT()].getAsLfsCarHcp(),
          pCars[CarT::RB4()].getAsLfsCarHcp(),
          pCars[CarT::FXO()].getAsLfsCarHcp(),
          pCars[CarT::LX4()].getAsLfsCarHcp(),
          pCars[CarT::LX6()].getAsLfsCarHcp(),
          pCars[CarT::MRT()].getAsLfsCarHcp(),
          pCars[CarT::UF1()].getAsLfsCarHcp(),
          pCars[CarT::RAC()].getAsLfsCarHcp(),
          pCars[CarT::FZ5()].getAsLfsCarHcp(),
          pCars[CarT::FOX()].getAsLfsCarHcp(),
          pCars[CarT::XFR()].getAsLfsCarHcp(),
          pCars[CarT::UFR()].getAsLfsCarHcp(),
          pCars[CarT::FO8()].getAsLfsCarHcp(),
          pCars[CarT::FXR()].getAsLfsCarHcp(),
          pCars[CarT::XRR()].getAsLfsCarHcp(),
          pCars[CarT::FZR()].getAsLfsCarHcp(),
          pCars[CarT::BF1()].getAsLfsCarHcp(),
          pCars[CarT::FBM()].getAsLfsCarHcp() } };
}

const QMap<char, QByteArray> StringDecoder::codec_names =
{ { 'L', QByteArray("Windows-1252") },
  { 'G', QByteArray("Windows-1253") },
  { 'C', QByteArray("Windows-1251") },
  { 'J', QByteArray("Shift-JIS") },
  { 'E', QByteArray("Windows-1250") },
  { 'T', QByteArray("Windows-1254") },
  { 'B', QByteArray("Windows-1257") },
  { 'H', QByteArray("Big5") },
  { 'S', QByteArray("GB18030") },
  { 'K', QByteArray("EUC-KR") } };

const QMap<char, char> StringDecoder::escapes =
    { { 'v', '|' },
      { 'a', '*' },
      { 'c', ':' },
      { 'd', '\\' },
      { 's', '/' },
      { 'q', '?' },
      { 't', '"' },
      { 'l', '<' },
      { 'r', '>' },
      { 'h', '#' },
      { '^', '^' } };


QByteArray StringDecoder::unicodeStringToLfs (const QString &s) {
    QByteArray ret_str;
    char current_charset = 'L';

    for (const QChar c : s) {
        // guess encoding
        const char charset = guessLfsEncoder(c, current_charset);
        if (charset != current_charset) {
           ret_str.append('^');
           ret_str.append(current_charset = charset);
        }

        ret_str.append(QTextCodec::codecForName(codec_names[charset])->fromUnicode(&c, 1));
    }
    return ret_str;
}


// Latin:
// In airio latin is encoded strangely, so that for two-byte { 194u, ???u }
// you need to just drop the first byte,
// while for other cases decoding from utf8 to windows-1252 works
//
// One byte windows-???? encodings:
// With those shitty_encoded encodings you can throw off the first byte
//
// Otherwise getting pure data works alright (seems like)
QByteArray StringDecoder::airioNicknameToLfs (const QByteArray &s) {
    QByteArray ret;
    char prev_char = 0;
    char current_charset = 'L';

    const std::vector<char> shitty_encoded{ 'G', 'C', 'E', 'T', 'B' };

    for (int i = 0; i < s.size(); ++i) {
        if (prev_char == '^' && vector_contains(shitty_encoded, s[i])) {
            current_charset = s[i];
        }

        if (((unsigned char) s[i] > 127u) && (i + 1) < s.size()) {
            if (vector_contains(shitty_encoded, current_charset)
                    || (unsigned char) s[i] == 194u) {
                ++i;
                ret.append(s[i]);
                prev_char = s[i];
                continue;
            } else {
                char arr[2];
                arr[0] = s[i];
                arr[1] = s[++i];
                const QString unistr =
                        QTextCodec::codecForName("utf-8")->makeDecoder()
                                                         ->toUnicode(arr, 2);
                ret.append(QTextCodec::codecForName(codec_names['L'])
                           ->fromUnicode(unistr));
                continue;
            }
        }

        ret.append(s[i]);
        prev_char = s[i];
    }
    return ret;
}


bool code_contains (const LfsEncodings &encodings,
                    const char encoding_name,
                    const uint16_t pChar) {
    if (pChar <= 0x7f) {
        return true;
    } else {
        const auto _begin = std::begin(encodings.ranges_data.at(encoding_name));
        const auto _end = std::end(encodings.ranges_data.at(encoding_name));
        auto found1 =
                std::upper_bound(_begin,
                                 _end,
                                 std::make_pair(pChar, pChar),
                                 [] (const std::pair<uint16_t, uint16_t> &p1,
                                 const std::pair<uint16_t, uint16_t> &p2) {
            return p1.first < p2.first;
        });

        if (found1 != _begin && (--found1)->second >= pChar) {
            return true;
        }

        return sorted_vector_contains(encodings.data.at(encoding_name), pChar);

    }

    return false;
}


char StringDecoder::guessLfsEncoder (const QChar pChar, const char prev) {
    static const LfsEncodings codes;

    if (code_contains(codes, prev, pChar.unicode())) {
        return prev;
    }

    for (const auto &el : codes.data) {
        if (el.first == prev) {
            continue;
        }
        if (code_contains(codes, el.first, pChar.unicode())) {
            return el.first;
        }
    }
    return 'L';
}

}



