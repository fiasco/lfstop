// Copyright 2015 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of LFSTop.
//
// LFSTop is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LFSTop is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LFSTop.  If not, see <http://www.gnu.org/licenses/>.


#include "UserSettings.hpp"


const std::vector<UserSettings::LaptimeUpdates::Types>
UserSettings::LaptimeUpdates::used_types =
    { UserSettings::LaptimeUpdates::Types::PB,
      UserSettings::LaptimeUpdates::Types::TBS,
      UserSettings::LaptimeUpdates::Types::TB,
      UserSettings::LaptimeUpdates::Types::AVG };


void UserSettings::load (const UsersDbStorage::UserSettings &pSettings) {
    if (pSettings.switches.size() >= 12) {
        int i = 0;

        offTrack.pb = pSettings.switches.testBit(i++);
        offTrack.tbs = pSettings.switches.testBit(i++);
        offTrack.tb = pSettings.switches.testBit(i++);
        offTrack.avg = pSettings.switches.testBit(i++);

        onTrackSelf.pb = pSettings.switches.testBit(i++);
        onTrackSelf.tbs = pSettings.switches.testBit(i++);
        onTrackSelf.tb = pSettings.switches.testBit(i++);
        onTrackSelf.avg = pSettings.switches.testBit(i++);

        onTrackOthers.pb = pSettings.switches.testBit(i++);
        onTrackOthers.tbs = pSettings.switches.testBit(i++);
        onTrackOthers.tb = pSettings.switches.testBit(i++);
        onTrackOthers.avg = pSettings.switches.testBit(i++);

        if (pSettings.switches.size() > 12) {
            hlvcReasons = pSettings.switches.testBit(i++);

            if (pSettings.switches.size() > 13) {
                showSplits = pSettings.switches.testBit(i++);
            }
        }
    }

    setLanguage(pSettings.language);
}

UsersDbStorage::UserSettings UserSettings::getUserSettings () const {
    UsersDbStorage::UserSettings us;
    us.switches.resize (14);
    int i = 0;

    us.switches.setBit(i++, offTrack.pb);
    us.switches.setBit(i++, offTrack.tbs);
    us.switches.setBit(i++, offTrack.tb);
    us.switches.setBit(i++, offTrack.avg);

    us.switches.setBit(i++, onTrackSelf.pb);
    us.switches.setBit(i++, onTrackSelf.tbs);
    us.switches.setBit(i++, onTrackSelf.tb);
    us.switches.setBit(i++, onTrackSelf.avg);

    us.switches.setBit(i++, onTrackOthers.pb);
    us.switches.setBit(i++, onTrackOthers.tbs);
    us.switches.setBit(i++, onTrackOthers.tb);
    us.switches.setBit(i++, onTrackOthers.avg);

    us.switches.setBit(i++, hlvcReasons);

    us.switches.setBit(i++, showSplits);

    us.language = language;

    return us;
}

QByteArray UserSettings::getLanguage () const {
    return language;
}

void UserSettings::setLanguage (const QByteArray &pLang) {
    language = pLang;
    mTr->resetLang(pLang);
}
