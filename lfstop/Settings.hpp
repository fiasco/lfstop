// Copyright 2015 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of LFSTop.
//
// LFSTop is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LFSTop is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LFSTop.  If not, see <http://www.gnu.org/licenses/>.


#ifndef LFSTOP_SETTINGS_HPP_INCLUDED
#define LFSTOP_SETTINGS_HPP_INCLUDED

#include <map>
#include <string>

#include "Utils/MKeyFile.hpp"
#include "Utils/StringUtils.hpp"


namespace Settings {
typedef std::map<std::string, std::string> SettingsMap;

SettingsMap get_settings_map (const MKeyFile &f);

template <typename T>
T getValue (const SettingsMap &settings,
            const std::string &option_name,
            const T &default_value) {
    if (settings.count(option_name)) {
        return StringUtils::fromString<T>(settings.at(option_name));
    }
    return default_value;
}

//template<>
//std::string getValue<std::string> (const SettingsMap &settings,
//                                   const std::string &option_name,
//                                   const std::string &default_value) {
//    if (settings.count(option_name)) {
//        return settings.at(option_name);
//    }
//    return default_value;
//}

} // namespace Settings

#endif // LFSTOP_SETTINGS_HPP_INCLUDED
