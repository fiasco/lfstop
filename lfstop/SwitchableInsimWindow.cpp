// Copyright 2015 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of LFSTop.
//
// LFSTop is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LFSTop is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LFSTop.  If not, see <http://www.gnu.org/licenses/>.


#include "SwitchableInsimWindow.hpp"


#include <QFile>


#include "Utils/StringUtils.hpp"
#include "LfsInsimUtils.hpp"


using StringUtils::toBase36;
using LfsInsimUtils::insertStr;
using Translation::StringIds;


SwitchableInsimWindow::SwitchableInsimWindow (LfsTcpSocket * const pSock,
                                              const unsigned char pUcid,
                                              const bool &pDataExportEnable,
                                              const std::string &pDataExportPath,
                                              const std::string &pDataExportUrlPrefix,
                                              const Translation::TranslationBinder * const pTr,
                                              Log *pLog)
    : mLog(pLog),
      mSock (pSock),
      mButton ("", 1, 0, 0, 0, 0, 0, 0),
      mUcid (pUcid),
      mDataExportEnable (pDataExportEnable),
      mDataExportPath (QByteArray::fromStdString(pDataExportPath)),
      mDataExportUrlPrefix (QByteArray::fromStdString(pDataExportUrlPrefix)),
      mTr (pTr)
{
    mCurrentDataPage.resize(20);
}

void SwitchableInsimWindow::setDataProvider (ISwitchableInsimWindowDataProvider *pDataProvider) {
    mDataProvider = pDataProvider;

    mNumColumns = mDataProvider->getNumColumns();
    mColumnsFormat = mDataProvider->getColumnsFormat();

    mSwitches = mDataProvider->getSwitches();
}

void SwitchableInsimWindow::showWindow () {
    mDataProvider->prepareData();
    if (mDataProvider->getNumRows() == 0) {
        mSock->write(InsimMessage(("No data: " + mHeader),
                                  mUcid));
        return;
    }


    mCurrentButtonId = 1;

    mButton.setUcid(mUcid);

    if (mStartFrom > mDataProvider->getNumRows()) {
        mSock->write(InsimMessage("No " + mHeader + " "
                                  + QByteArray::number(static_cast<qulonglong>(mStartFrom))
                                  + " data",
                                  mUcid));
        return;
    }

    size_t diff = mDataProvider->getNumRows() - mStartFrom;

    mNumRowsOnPage = (diff >= 20) ? 20 : (diff % 21);

    mWindowHeight =
            mRowHeight * mNumRowsOnPage
            + mHeaderHeight
            + mNavButtonHeight
            + 2;
    mWindowCoordTop = (200 - mWindowHeight) / 2 + 5;

    sendBackgroundButton();
    sendWindowHeaderButton();
    sendRowsButtons();

    mIsOpen = true;
}


void SwitchableInsimWindow::buttonClick (const IS_BTC &btn) {
    if (btn.ClickID == mNextClickId) {
        mNextClickId = 0;
        mStartFrom += 20; // assuming we never get close to
                          // numeric_limists<size_t>::max
        if (mStartFrom >= mDataProvider->getNumRows()) {
            const size_t substract = std::min<size_t>(20, mDataProvider->getNumRows());
            mStartFrom = mDataProvider->getNumRows() - substract;
        }
        clearWindow();
        showWindow();
    }
    else if (btn.ClickID == mPrevClickId) {
        mPrevClickId = 0;
        if (mStartFrom >= 20) {
            mStartFrom -= 20;
        } else {
            mStartFrom = 0;
        }
        clearWindow();
        showWindow();
    }
    else if (mDataExportEnable
             && mDataProvider->isExportable()
             && btn.ClickID == mExportClickId
             && (!mDataExportDefunct)) {
        if (QDateTime::currentMSecsSinceEpoch() - mLastDataExportTimestamp
            < (1000 * 5)) {
            return;
        }

        const size_t n = mDataProvider->getNumRows();
        QByteArray str;

        using LfsInsimUtils::format;
        using LfsInsimUtils::FormatPair;

        if (mHeader.contains("%total_records")) {
            str+= mDecoder.lfsStringToUnicode(
                format(mHeader,
                       FormatPair{ "%total_records",
                                    QByteArray::number(static_cast<qulonglong>(mDataProvider
                                    ->getNumRows()))})) + '\n';
        } else {
            str += mDecoder.lfsStringToUnicode(mHeader) + '\n';
        }

        for (size_t i = 0; i < n; ++i) {
            str += mDataProvider->getRowToExport(i, mDecoder, *mTr) + '\n';
        }

        const auto filename =
            toBase36(QDateTime::currentDateTime().toMSecsSinceEpoch()) + ".txt";

        QFile file(mDataExportPath + filename);
        if (file.open(QIODevice::WriteOnly)) {
            file.write(str);
            file.close();
            mSock->write(InsimMessage(mDataExportUrlPrefix + filename, mUcid, 0));
            mLastDataExportTimestamp = QDateTime::currentMSecsSinceEpoch();
        } else {
            if (mLog) {
                (*mLog)(Log::ERROR)
                        << "Export failed, could not open file for writing: "
                        << mDataExportPath + filename;
            }
            mDataExportDefunct = true;
        }
    }
    else if (mSwitchClickIds.size()
             && btn.ClickID <= mSwitchClickIds.lastKey()
             && btn.ClickID >= mSwitchClickIds.firstKey()) {
        auto columns_to_update =
                mDataProvider
                    ->switchClicked(mSwitchClickIds.value(btn.ClickID));

        if (columns_to_update.size()) {
            for (const auto c: columns_to_update) {
                resendColumnButtons(c);
            }
            const quint8 switch_id = mSwitchClickIds[btn.ClickID];
            resendSwitchButton((*mSwitches).value(switch_id),
                               switch_id,
                               btn.ClickID);
        } else {
            // highlighting is done only on initial data query, and after full
            // window update that is no longer accurate
            mHighlightRow = std::numeric_limits<size_t>::max();

            clearWindow();

            if (mStartFrom > mDataProvider->getNumRows()) {
                mStartFrom = mDataProvider->getNumRows()
                    - std::min(size_t(20), mDataProvider->getNumRows());

            }
            showWindow();
        }
    }
    else if (btn.ClickID <= mCurrentButtonId) {
        const size_t btn_num = static_cast<size_t>((btn.ClickID - 2) - 1);
        size_t row_num = btn_num / mNumColumns;

        if (row_num >= mNumRowsOnPage) {
            row_num = 0;
        }

        const size_t row_num_table = row_num + mStartFrom;

        if (mDataProvider->isInteractive() && btn.ClickID > 2) {
            mDataProvider->rowClicked(row_num, row_num_table, btn.CFlags);
            return;
        }

        if (btn.CFlags == ISB_RMB) {
            if (row_num < mNumRowsOnPage) {
                QByteArray str;
                str.reserve(120);
                for (const QByteArray &item : mCurrentDataPage[row_num]) {
                    str.append(item).append(' ');
                }

                mSock->write(InsimMessage(str, mUcid));
            }
        }
        else {
            clearWindow ();
        }
    }
}

void SwitchableInsimWindow::clearWindow () {
    InsimButtonFunction bc (BFN_DEL_BTN, mUcid, 0);
    for (int i = 1; i <= mCurrentButtonId; ++i) {
        bc.setClickId(i);
        mSock->write(bc);
    }

    mIsOpen = false;
}

void SwitchableInsimWindow::sendBackgroundButton () {
    mButton.setText("");
    mButton.setCoords(50, mWindowCoordTop, 100, mWindowHeight);
    mButton.setClickId(mCurrentButtonId++);
    mButton.setClick(true);
    mButton.setDark(true);

    mSock->write(mButton);
}

void SwitchableInsimWindow::sendWindowHeaderButton () {
    using LfsInsimUtils::format;
    using LfsInsimUtils::FormatPair;
    if (mHeader.contains("%total_records")) {
        mButton.setText(
            format(mHeader,
                FormatPair { "%total_records",
                             QByteArray::number((qulonglong)mDataProvider
                                                ->getNumRows()) }));
    } else {
        mButton.setText(mHeader);
    }

    mButton.setAlignment(InsimButton::center);
    mButton.setCoords(51, mWindowCoordTop + 1, 98, mHeaderHeight);
    mButton.setClickId(mCurrentButtonId++);

    mSock->write(mButton);
}

void SwitchableInsimWindow::sendRowsButtons () {
    const size_t total_rows =
            mDataProvider->getNumRows();

    size_t rows_printed = 0;
    const size_t first_row_to_print = mStartFrom;
    for (size_t i = first_row_to_print;
         i < 20 + mStartFrom && i < total_rows;
         ++i) {
        sendRowButtons (rows_printed++, i);
    }

    sendBottomFillerButton();

    mPrevClickId = 0;
    mNextClickId = 0;

    if (total_rows > first_row_to_print + rows_printed) {
        mNextClickId = mCurrentButtonId;
        sendNextPageButton();
    }
    if (total_rows > 0 && first_row_to_print != 0) {
        mPrevClickId = mCurrentButtonId;
        sendPrevPageButton();
    }

    mSwitchClickIds.clear();

    if (mSwitches) {
        QMapIterator<quint8, QByteArray> it(*mSwitches);
        while (it.hasNext()) {
            it.next();
            const quint8 click_id = sendSwitchButton(it.value(), it.key());
            mSwitchClickIds[click_id] = it.key();
        }
    }

    if (mDataExportEnable && mDataProvider->isExportable()) {
        sendExportButton();
    }
}

void SwitchableInsimWindow::sendRowButtons (const size_t pWindowRowNum,
                                            const size_t pRowNum) {
    mCurrentDataPage[pWindowRowNum] = mDataProvider->getRow(pRowNum, *mTr);
    if (pRowNum == mHighlightRow) {
        mButton.setLight(true);
        mButton.setDark(false);
    }
    unsigned char total_w = 0;
    for (size_t i = 0; i < mNumColumns; ++i) {
        const QByteArray &text = mCurrentDataPage[pWindowRowNum][i];
        mButton.setText(text);
        mButton.setCoords(51 + total_w ,
                          pWindowRowNum * mRowHeight
                          + mWindowCoordTop + 1 + mHeaderHeight,
                          mColumnsFormat[i].width, mRowHeight);
        mButton.setClickId(mCurrentButtonId++);
        mButton.setAlignment(mColumnsFormat[i].alignment);
        mSock->write(mButton);
        total_w += mColumnsFormat[i].width;
    }

    if (pRowNum == mHighlightRow) {
        mButton.setLight(false);
        mButton.setDark(true);
    }
}

void SwitchableInsimWindow::sendNextPageButton () {
    mButton.setText(insertStr("> %s >",
                              mTr->get(StringIds::WINDOW_NEXT_PAGE)));
    mButton.setCoords(129,
                      mNumRowsOnPage * mRowHeight + mWindowCoordTop + 1 + mHeaderHeight,
                      20,
                      mNavButtonHeight);
    mButton.setClickId(mCurrentButtonId++);
    mButton.setRight();
    mButton.setDark(false);

    mSock->write(mButton);
    mButton.setLeft();
}

void SwitchableInsimWindow::sendPrevPageButton () {
    mButton.setText(insertStr("< %s <",
                              mTr->get(StringIds::WINDOW_PREVIOUS_PAGE)));
    mButton.setCoords(51,
                      mNumRowsOnPage * mRowHeight + mWindowCoordTop + 1 + mHeaderHeight,
                      20,
                      mNavButtonHeight);
    mButton.setClickId(mCurrentButtonId++);
    mButton.setDark(false);

    mSock->write(mButton);
}

void SwitchableInsimWindow::sendBottomFillerButton () {
    mButton.setText(" ");
    mButton.setCoords(51,
                      mNumRowsOnPage * mRowHeight + mWindowCoordTop + 1 + mHeaderHeight,
                      98,
                      mNavButtonHeight);
    mButton.setClickId(mCurrentButtonId++);

    mSock->write(mButton);
}

quint8 SwitchableInsimWindow::sendSwitchButton (const QByteArray &pSwitchText,
                                                const quint8 pNum) {
    mButton.setText(pSwitchText);
    mButton.setCoords(51 + 20 + 20 * pNum,
                      mNumRowsOnPage * mRowHeight + mWindowCoordTop + 1 + mHeaderHeight,
                      20,
                      mNavButtonHeight);
    quint8 ret = mCurrentButtonId;
    mButton.setClickId(mCurrentButtonId++);
    mButton.setDark(false);

    mSock->write(mButton);

    return ret;
}

void SwitchableInsimWindow::resendColumnButtons (const size_t pColumnN) {
    const size_t total_rows = mDataProvider->getNumRows();
    const size_t first_row_to_print = 0 + mStartFrom;

    quint8 row_offset = 0;
    for (size_t i = 0; i < pColumnN; ++i) {
        row_offset += mColumnsFormat[i].width;
    }

    quint8 click_id = 3 + pColumnN;

    size_t data_page_i = 0;
    for (size_t i = first_row_to_print;
         i < 20 + mStartFrom
         && i < total_rows
         && data_page_i < mNumRowsOnPage;
         ++i) {

        const QByteArray row_text =
                mDataProvider->getRowColumn(i, pColumnN);

        mCurrentDataPage[data_page_i][pColumnN] = row_text;

        mButton.setAlignment(mColumnsFormat[pColumnN].alignment);
        mButton.setDark(true);
        if (i == mHighlightRow) {
            mButton.setLight(true);
            mButton.setDark(false);
        }

        mButton.setCoords(51 + row_offset ,
                          data_page_i * mRowHeight
                          + mWindowCoordTop + 1 + mHeaderHeight,
                          mColumnsFormat[pColumnN].width, mRowHeight);
        mButton.setClickId(click_id);

        mButton.setText(row_text);

        mSock->write(mButton);

        click_id += mNumColumns;
        ++data_page_i;
    }
}

void SwitchableInsimWindow::resendSwitchButton (const QByteArray &pSwitchText,
                                                const quint8 pNum,
                                                const quint8 pClickId) {
    mButton.setText(pSwitchText);
    mButton.setCoords(51 + 20 + 20 * pNum,
                      mNumRowsOnPage * mRowHeight + mWindowCoordTop + 1 + mHeaderHeight,
                      20,
                      mNavButtonHeight);
    mButton.setClickId(pClickId);
    mButton.setDark(false);
    mButton.setLight(false);

    mSock->write(mButton);
}

void SwitchableInsimWindow::sendExportButton () {
    mButton.setText(insertStr("[ %s ]",
                              mTr->get(StringIds::WINDOW_DATA_EXPORT)));

    int num_buttons_to_skip = 0;
    if (mSwitches) {
        num_buttons_to_skip = mSwitches->size();
    }
    mButton.setCoords(51 + 20 + 20 * num_buttons_to_skip,
                      mNumRowsOnPage * mRowHeight + mWindowCoordTop + 1 + mHeaderHeight,
                      20,
                      mNavButtonHeight);
    mExportClickId = mCurrentButtonId;
    mButton.setClickId(mCurrentButtonId++);
    mButton.setDark(false);

    mSock->write(mButton);
}
