// Copyright 2015 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of LFSTop.
//
// LFSTop is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LFSTop is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LFSTop.  If not, see <http://www.gnu.org/licenses/>.


#ifndef LFSTOP_SWITCHABLE_INSIM_WINDOW_HPP_INCLUDED
#define LFSTOP_SWITCHABLE_INSIM_WINDOW_HPP_INCLUDED

#include <QList>
#include <QStringList>


#include "InsimPackets/InsimButton.hpp"
#include "InsimPackets/InsimButtonFunction.hpp"
#include "InsimPackets/InsimMessage.hpp"
#include "LfsInsim.hpp"
#include "IInsimWindow.hpp"
#include "LfsTcpSocket.hpp"
#include "ISwitchableInsimWindowDataProvider.hpp"
#include "LfsInsimUtils.hpp"
#include "Translation.hpp"

class SwitchableInsimWindow final: public IInsimWindow {
public:
    SwitchableInsimWindow (LfsTcpSocket * const pSock,
                           const unsigned char pUcid,
                           const bool &pDataExportEnable,
                           const std::string &pDataExportPath,
                           const std::string &pDataExportUrlPrefix,
                           const Translation::TranslationBinder * const pTr,
                           Log *pLog);

    void setHeader (const QByteArray &pHeader) {
        mHeader = pHeader;
    }

    void setDataProvider (ISwitchableInsimWindowDataProvider *pDataProvider);

    const ISwitchableInsimWindowDataProvider *getDataProvider () const {
        return mDataProvider;
    }

    void setStartFrom (const size_t pStartFrom) {
        mStartFrom = pStartFrom;
    }

    void setHighlightRow (const size_t pHighlightRow) {
        mHighlightRow = pHighlightRow;
    }

    void updateRowColumnContent (const size_t pNumRow, const size_t pNumColumn) {
        // dirty hack method added for voting system
        const unsigned char click_id = 3 + mNumColumns * pNumRow + pNumColumn;
        InsimButton btn(mDataProvider->getRowColumn(pNumRow, pNumColumn),
                        1, 0, 0, 0, 0, mUcid, click_id);
        mSock->write(btn);
    }

    bool isOpen () const {
        return mIsOpen;
    }

    void clearWindow () override;

protected:
    void showWindow () override;
    void buttonClick (const IS_BTC &btn) override;
    void clearedByUser () override {
        mIsOpen = false;
    }

private:
    void sendBackgroundButton ();
    void sendWindowHeaderButton ();
    void sendRowsButtons ();
    void sendRowButtons (const size_t pWindowRowNum, const size_t pRowNum);
    void sendNextPageButton ();
    void sendPrevPageButton ();
    void sendBottomFillerButton ();
    quint8 sendSwitchButton (const QByteArray &pSwitchText,
                             const quint8 pNum); // returns click id

    void resendColumnButtons (const size_t pColumnN);
    void resendSwitchButton (const QByteArray &pSwitchText,
                             const quint8 pNum,
                             const quint8 pClickId);

    void sendExportButton ();

    Log *mLog = nullptr;

    LfsInsimUtils::StringDecoder mDecoder;

    LfsTcpSocket * const mSock;
    std::vector<InsimWindowColumnFormat> mColumnsFormat;
    QByteArray mHeader;
    InsimButton mButton;

    std::vector<std::vector<QByteArray>> mCurrentDataPage;

    ISwitchableInsimWindowDataProvider *mDataProvider = nullptr;

    const QMap<quint8, QByteArray> *mSwitches = nullptr;
    QMap<quint8, quint8> mSwitchClickIds; // click id : switch id

    size_t mStartFrom = 0;
    size_t mNumColumns = 0;
    size_t mHighlightRow = std::numeric_limits<size_t>::max();

    const quint8 mUcid;
    quint8 mCurrentButtonId = 0; // also amount of used click ids
    quint8 mPrevClickId = 0;
    quint8 mNextClickId = 0;
    quint8 mExportClickId = 0;

    unsigned char mNumRowsOnPage = 0;

    static constexpr quint8 mRowHeight = 5;
    static constexpr quint8 mHeaderHeight = 5;
    quint8 mWindowHeight = 0;
    quint8 mWindowCoordTop = 0;
    static constexpr quint8 mNavButtonHeight = 5;

    const bool &mDataExportEnable;
    bool mDataExportDefunct = false;
    const QByteArray mDataExportPath;
    const QByteArray mDataExportUrlPrefix;

    qint64 mLastDataExportTimestamp = QDateTime::currentMSecsSinceEpoch();
    const Translation::TranslationBinder * const mTr;

    bool mIsOpen = false;
};

#endif // LFSTOP_SWITCHABLE_INSIM_WINDOW_HPP_INCLUDED
