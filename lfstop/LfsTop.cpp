// Copyright 2015 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of LFSTop.
//
// LFSTop is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LFSTop is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LFSTop.  If not, see <http://www.gnu.org/licenses/>.


#include "LfsTop.hpp"


#include <QThread>
#include <QDir>
#include <QByteArray>


#include "LfsServer.hpp"
#include "Utils/StringUtils.hpp"
#include "Utils/MKeyFile.hpp"
#include "LfsServerConfigCreator.hpp"


LFSTop::LFSTop (Log &pLog, LogDestination &pServerAdditionalLogDestination)
    : mConfigFile ("config.ini"),
      mSettingsMap (Settings::get_settings_map(mConfigFile)),
      mLog (pLog),
      mServerLogDestination (pServerAdditionalLogDestination),
      mUsersDb (mLog),
      mDbManager (mLog, mConfigFile, mSettingsMap),
      mPerformanceLogFile ("logs/performance.log"),
      mPerformanceLog ("Performance",
                       mPerformanceLogFile,
                       "%datetime %message") {
}

qint64 LFSTop::getReceivedBytes () {
    qint64 ret = 0;
    for (const LfsServer * const srv : mServers) {
        ret += srv->getReceivedBytes();
    }
    return ret;
}

qint64 LFSTop::getSentBytes () {
    qint64 ret = 0;
    for (const LfsServer * const srv : mServers) {
        ret += srv->getSentBytes();
    }
    return ret;
}

void LFSTop::invokeServer (const MKeyFile &pConfigFile,
                           const QByteArray &pInstanceName) {
//    SettingsMap cfg = get_settings_map(pConfigFile);

    LfsServer* server = new LfsServer(this,
                                      LfsServerConfigLoader::loadLfsServerConfig(pConfigFile, mLog, mIsRestartPossible),
                                      mServerLogDestination,
                                      mPerformanceLog,
                                      pInstanceName,
                                      &mDbManager,
                                      &mUsersDb,
                                      mTr);

    mServers.insert(server);

    QThread* server_thread = new QThread(this);
    mServerThreadPtrs.insert(server_thread);
    server->moveToThread(server_thread);

    QObject::connect(server_thread, SIGNAL(started()), server, SLOT(run()));
    QObject::connect(server_thread, SIGNAL(finished()),
                     this, SLOT(serverThreadFinished()));
    QObject::connect(server_thread, SIGNAL(finished()),
                     server_thread, SLOT(deleteLater()));


    QObject::connect(server, SIGNAL(finished(const QByteArray &, const bool)),
                     server_thread, SLOT(quit()));

    QObject::connect(server, SIGNAL(finished(const QByteArray &, const bool)),
                     this, SLOT(instanceFinished(const QByteArray &, const bool)));

    QObject::connect(server, SIGNAL(finished(const QByteArray &, const bool)),
                     server, SLOT(deleteLater()));

    QObject::connect(this,
                     SIGNAL(shutdownAll()),
                     server,
                     SLOT(shutdown()),
                     Qt::UniqueConnection);

    QObject::connect(server, SIGNAL(shutdownRequested()),
                     this, SLOT(shutdownRequested()));

    QObject::connect(server, SIGNAL(restartRequested()),
                     this, SLOT(restartRequested()));

    server_thread->start();
    mInstanceNames += pInstanceName;
}


void LFSTop::run () {
    QDir dir ("servers/");
    QFileInfoList files = dir.entryInfoList(QStringList("*.ini"),
                                            QDir::Files | QDir::Readable);

    if (files.isEmpty()) {
        mLog(Log::ERROR) << "no servers to launch in servers directory";
        emit finished();
    }

    for (QFileInfo file: files) {
        std::string file_name = file.fileName().toUtf8().constData();
        MKeyFile f("servers/" + file_name);
        mLog() << "launching server: " << file_name;

        invokeServer(f, file.baseName().toUtf8());
    }
}


void LFSTop::instanceFinished (const QByteArray &pInstanceName, const bool pSuccessfully) {
    if (mAllFinishedSuccessfully && !pSuccessfully) {
        mAllFinishedSuccessfully = pSuccessfully;
    }
    mLog() << "instance finished: " << pInstanceName;
    mInstanceNames.remove(pInstanceName);
    if (mInstanceNames.size() == 0 && mServerThreadPtrs.size() == 0) {
        emit finished();
    }
}

void LFSTop::serverThreadFinished () {
    mServerThreadPtrs.remove((QThread*)sender());
    if (mInstanceNames.size() == 0 && mServerThreadPtrs.size() == 0) {
        emit finished();
    }
}

void LFSTop::restartRequested () {
    assert(mIsRestartPossible);
    mIsRestartRequested = true;
    emit shutdownAll();
}

void LFSTop::shutdownRequested ()
{
    emit shutdownAll();
}
