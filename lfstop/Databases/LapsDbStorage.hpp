// Copyright 2015 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of LFSTop.
//
// LFSTop is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LFSTop is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LFSTop.  If not, see <http://www.gnu.org/licenses/>.


#ifndef LFSTOP_LAPS_DB_STORAGE_HPP_INCLUDED
#define LFSTOP_LAPS_DB_STORAGE_HPP_INCLUDED


#include <vector>
#include <stdint.h>
#include <cassert>


#include <QMap>
#include <QList>
#include <QVector>
#include <QSet>
#include <QPair>
#include <QByteArray>
#include <QDataStream>
#include <QDateTime>


#include "LfsInsim.hpp"
#include "CompilerUtils.hpp"


typedef quint32 LaptimeT;


#include "Databases/Fundamentals/CarT.hpp"
#include "Databases/Fundamentals/ClassT.hpp"
#include "Databases/Fundamentals/TyreT.hpp"


struct LapsdbTypeInfo {
    static const QMap<QByteArray, QSet<CarT>> lfs_cars_aliases;
};

struct CarDefinition {
    CarDefinition () {
    }

    explicit CarDefinition (const CarT pC)
        : lfsCar (pC) {
    }

    CarDefinition (const CarT pC,
                   const quint8 pIntakeRestriction,
                   const quint8 pAddedMass)
        : lfsCar (pC),
          intakeRestriction (pIntakeRestriction),
          addedMass (pAddedMass) {
    }

    CarDefinition combine (const CarDefinition &pOther) const {
        assert (pOther.lfsCar == lfsCar);
        return CarDefinition (lfsCar,
                              std::min(intakeRestriction, pOther.intakeRestriction),
                              std::min(addedMass, pOther.addedMass));
    }

    bool overlaps (const CarDefinition &pOther) const {
        assert(lfsCar == pOther.lfsCar);
        if ((intakeRestriction > pOther.intakeRestriction
             && addedMass < pOther.addedMass)
            ||
            (intakeRestriction < pOther.intakeRestriction
             && addedMass > pOther.addedMass)) {
            return true;
        }
        return false;
    }

    bool operator< (const CarDefinition &pOther) const {
        assert(lfsCar == pOther.lfsCar);

        if (intakeRestriction <= pOther.intakeRestriction
                && addedMass < pOther.addedMass) {
            return true;
        } else if (intakeRestriction < pOther.intakeRestriction
                   && addedMass <= pOther.addedMass){
            return true;
        }
        return false;
    }

    bool operator<= (const CarDefinition &pOther) const {
        assert(lfsCar == pOther.lfsCar);

        if (intakeRestriction <= pOther.intakeRestriction
                && addedMass <= pOther.addedMass) {
            return true;
        }
        return false;
    }


    CarHCP getAsLfsCarHcp () const {
        return { addedMass, intakeRestriction };
    }

    QByteArray toString () const {
        return "(" + lfsCar.lfsCarToString()
                + " " + QByteArray::number(intakeRestriction) + "%"
                + " " + QByteArray::number(addedMass) + "kg)";
    }


    CarT lfsCar = CarT::unknownCar();
    quint8 intakeRestriction = 0;
    quint8 addedMass = 0;
};

bool operator== (const CarDefinition &c1, const CarDefinition &c2)
LFSTOP_FUNCTION_ATTRIBUTE_PURE;

struct LapsDbStorage {
    static constexpr int not_found = -1;

    struct Laptime {
        Laptime () noexcept
            : timeStamp (QDateTime::currentMSecsSinceEpoch() / 1000) {
        }

        explicit Laptime (const qint64 pTimeStamp)
            : timeStamp (pTimeStamp) {
        }

        Laptime (const std::vector<LaptimeT> &pSplits,
                 const LaptimeT pLaptime,
                 const QByteArray &pUsername)
            : username (pUsername),
              splits (QVector<LaptimeT>::fromStdVector(pSplits)),
              timeStamp (QDateTime::currentMSecsSinceEpoch() / 1000),
              laptime(pLaptime) {
        }

        Laptime (const std::vector<LaptimeT> &pSplits,
                 const LaptimeT pLaptime,
                 const QByteArray &pUsername,
                 const qint64 pTimeStamp)
            : username (pUsername),
              splits (QVector<LaptimeT>::fromStdVector(pSplits)),
              timeStamp (pTimeStamp),
              laptime(pLaptime) {
        }

        bool operator< (const Laptime &pOther) const {
            if (username[0] == '/'
                && timeStamp != pOther.timeStamp
                && username != pOther.username) {
                return true;
            }
            if (laptime == pOther.laptime) {
                return timeStamp < pOther.timeStamp;
            }
            return laptime < pOther.laptime;
        }

        bool operator> (const Laptime &pOther) const {
            if (username[0] == '/'
                && timeStamp != pOther.timeStamp
                && username != pOther.username) {
                return true;
            }
            if (laptime == pOther.laptime) {
                return timeStamp > pOther.timeStamp;
            }
            return laptime > pOther.laptime;
        }

        // warning! not for general use!
        bool operator== (const Laptime &pOther) const {
            return laptime == pOther.laptime;
        }



        QByteArray username;
        QVector<LaptimeT> splits;
        qint64 timeStamp;  // _seconds_ since epoch
        LaptimeT laptime;
        // consider adding also number of lap and timestamp
        static LapsDbStorage::Laptime
        createBySections (const std::vector<LaptimeT> &pSections,
                          const QByteArray &pUsername);
    };

    struct Bestlap {
        TyreT tyres;
        int num; // number in laps list (CarLaptimes.byTyre or .byTyreNonClean);
    };

    struct NonCleanLap {
        bool isClean; // true: lap is in CarLaptimes.byTyre, otherwise in .byTyreNonCleanStorage;
        int num; // number in the table selected
    };

    struct CarLaptimes {
        QMap<TyreT, QList<Laptime>> byTyre; // Tyre : laptimes
        QMap<TyreT, QList<Laptime>> byTyreNonCleanStorage;
        QMap<TyreT, QList<NonCleanLap>> byTyreNonClean;
        QList<Bestlap> best; // Tyre : id in byTyre[compound]
        QList<Bestlap> bestNonClean; // Tyre : id in byTyreNonClean[compound]
    };

    struct ClassLaptime {
        CarT car;
        int num;
    };

    struct ClassBestlap {
        TyreT tyres;
        int num;
    };

    struct NonCleanClassLaptime {
        CarT car;
        int num;
    };

    struct NonCleanClassBestlap {
        TyreT tyres;
        int num;
    };

	// PVS: V802 On 32-bit platform, structure size can be reduced from 16 to 12 bytes by rearranging the fields according to their sizes in decreasing order. lapsdbstorage.hpp 238
    struct ClassLaptimes {
        // this is similar to CarLaptimes but instead of storing times,
        // we storelinks to CarLaptimes' data
        QMap<TyreT, QList<ClassLaptime>> byTyre;
        // and here we store links to ClassLaptimes::byTyre
        QList<ClassBestlap> best;

        QMap<TyreT, QList<NonCleanClassLaptime> > byTyreNonClean;
        QList<NonCleanClassBestlap> bestNonClean;

    };

    QMap<CarT, CarLaptimes> laps; // Car : Laptimes
    QMap<ClassT, ClassLaptimes> classLaps;

    static void load_db_v1 (QDataStream &in, QMap<QByteArray, LapsDbStorage> &db);
    static void load_db_v2 (QDataStream &in, QMap<QByteArray, LapsDbStorage> &db);
};

struct LapsDbStorageMetadata {
    QMap<ClassT, QSet<CarT> > classes;
    QMap<QByteArray, ClassT> classNames;

    QMap<CarT, CarDefinition> customCars; // custom cars
    QMap<QByteArray, CarT> customCarNames;
};


QDataStream &operator<< (QDataStream &out, const LapsDbStorage::Bestlap &lt);
QDataStream &operator>> (QDataStream &in, LapsDbStorage::Bestlap &lt);

QDataStream &operator<< (QDataStream &out, const LapsDbStorage::NonCleanLap &lt);
QDataStream &operator>> (QDataStream &in, LapsDbStorage::NonCleanLap &lt);

QDataStream &operator<< (QDataStream &out, const LapsDbStorage::ClassLaptime &lt);
QDataStream &operator>> (QDataStream &in, LapsDbStorage::ClassLaptime &lt);

QDataStream &operator<< (QDataStream &out, const LapsDbStorage::ClassBestlap &lt);
QDataStream &operator>> (QDataStream &in, LapsDbStorage::ClassBestlap &lt);


QDataStream &operator<< (QDataStream &out, const LapsDbStorage::Laptime &lt);
QDataStream &operator>> (QDataStream &in, LapsDbStorage::Laptime &lt);


QDataStream &operator<< (QDataStream &out,
                         const LapsDbStorage::CarLaptimes &clt);
QDataStream &operator>> (QDataStream &in,
                         LapsDbStorage::CarLaptimes &clt);


QDataStream &operator<< (QDataStream &out,
                         const LapsDbStorage::ClassLaptimes &clt);
QDataStream &operator>> (QDataStream &in,
                         LapsDbStorage::ClassLaptimes &clt);

QDataStream &operator<< (QDataStream &out,
                         const LapsDbStorage::NonCleanClassLaptime &clt);
QDataStream &operator>> (QDataStream &in,
                         LapsDbStorage::NonCleanClassLaptime &clt);

QDataStream &operator<< (QDataStream &out,
                         const LapsDbStorage::NonCleanClassBestlap &clt);
QDataStream &operator>> (QDataStream &in,
                         LapsDbStorage::NonCleanClassBestlap &clt);



QDataStream &operator<< (QDataStream &out,
                         const CarDefinition &cd);
QDataStream &operator>> (QDataStream &in,
                         CarDefinition &cd);


QDataStream &operator<< (QDataStream &out, const LapsDbStorage &db);
QDataStream &operator>> (QDataStream &in, LapsDbStorage &db);


QDataStream &operator<< (QDataStream &out, const LapsDbStorageMetadata &db);
QDataStream &operator>> (QDataStream &in, LapsDbStorageMetadata &db);


#endif // LFSTOP_LAPS_DB_STORAGE_HPP_INCLUDED
