// Copyright 2015 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of LFSTop.
//
// LFSTop is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LFSTop is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LFSTop.  If not, see <http://www.gnu.org/licenses/>.


#ifndef LFSTOP_LAPS_DB_HPP_INCLUDED
#define LFSTOP_LAPS_DB_HPP_INCLUDED


#include <memory>

#include <QReadWriteLock>


#include "LapsDbStorage.hpp"
#include "Utils/StdVectorUtils.hpp"
#include "Logging/Log.hpp"
#include "LapsDbQuery.hpp"
#include "Translation.hpp"

struct LapsDbLaptimeAnswer : public LapsDbStorage::Laptime {
    LapsDbLaptimeAnswer (const LapsDbStorage::Laptime &pLaptime,
                         const TyreT pTyres,
                         const bool pClean,
                         const CarT pCar,
                         const int pIndex)
        : LapsDbStorage::Laptime (pLaptime),
          tyres (pTyres),
          car (pCar),
          clean (pClean),
          index (pIndex) {
    }

    LapsDbLaptimeAnswer (const LapsDbStorage::Laptime &pLaptime,
                         const TyreT pTyres,
                         const bool pClean,
                         const int pIndex)
        : LapsDbStorage::Laptime (pLaptime),
          tyres (pTyres),
          clean (pClean),
          index (pIndex) {
    }

    TyreT tyres;
    CarT car;
    bool clean;
    int index;
};


struct LapsDbAddAnswer {
    LaptimeT difference; // 0 means not enhanced, >0 means enhancment
    bool isBestLap; // was enhancement only tyre wise or (if true) best of all
    bool isAdded;
};

class QReadWriteLock;

class LapsDb {
public:
    class LapNotFoundException {};

    // These methods should be used only by DatabasesManager
    // and are not thread-safe or re-entrant {
    LapsDb (Log &pLogger);
    ~LapsDb ();

    void loadFile (const QString &pTrackname);
    void writeFile ();
    void writeFileIfNeed();

    // database manipulation classes:
    void addClass (const QByteArray &pClassName,
                   const QSet<QByteArray> &pCars);
    void addCar (const QByteArray &pCarName,
                 const CarDefinition &pCar);

    // } (end of DatabasesManager-allowed methods)

    QMapIterator<QByteArray, QSet<QByteArray>> getClassesNamesIterator () {
        return QMapIterator<QByteArray, QSet<QByteArray>>(mClassesStrings);
    }

    QMapIterator<QByteArray, CarDefinition > getCarsIterator () {
        return QMapIterator<QByteArray, CarDefinition >(mCars);
    }

    QByteArray queryToString (const QByteArray &pDbName,
                              const LapsdbQuery &pQuery,
                              const Translation::TranslationBinder &pTr);

    ClassT getClassByName (const QByteArray &pClassName) const {
        if (mDbMetadata.classNames.contains(pClassName)) {
            return mDbMetadata.classNames[pClassName];
        }
        return ClassT::notFound();
    }

    QMap<ClassT, QSet<CarT>> getClasses () {
        return mDbMetadata.classes;
    }

    QSet<CarT> getClassCars (const ClassT pClass) {
        return mDbMetadata.classes[pClass];
    }

    CarT getCarByName (const QByteArray &pCarName) const {
        const CarT car = CarT::lfsCarFromString(pCarName);
        if (car == CarT::unknownCar()) {
            return mDbMetadata.customCarNames[pCarName];
        }
        return car;
    }

    QByteArray getCarName (const CarT pCar) const {
        if (pCar.isStockLfsCar()) {
            return pCar.lfsCarToString();
        } else {
            return mDbMetadata.customCarNames.key(pCar, "ERROR!");
        }
    }

    CarT getCarByCarDefinition (const CarDefinition &pCar);

    CarDefinition getCarDefinition (const CarT pCar) const {
        const QByteArray car_name = getCarName(pCar);
        if (mDbMetadata.customCars.contains(pCar)) {
            return mDbMetadata.customCars.value(pCar);
        }
        return CarDefinition(pCar, 0, 0);
    }

    QByteArray getClassName (const ClassT pClass) const {
        return mDbMetadata.classNames.key(pClass, "ERROR!");
    }

    QSet<CarT> getCustomCarsByLfsCar (const CarT pCar) {
        QSet<CarT> ret;
        QMapIterator<CarT, CarDefinition> cars_it = mDbMetadata.customCars;
        while (cars_it.hasNext()) {
            cars_it.next();
            if (cars_it.value().lfsCar == pCar) {
                ret << cars_it.key();
            }
        }
        return ret;
    }

    // database manipulating and querying classes
    LapsDbAddAnswer addLaptime (const QByteArray &pDbName,
                                const CarT pCar,
                                const TyreT pTyres,
                                const LapsDbStorage::Laptime &pLaptime,
                                const bool pClean = true);

    LapsDbLaptimeAnswer getClassLaptime (const QByteArray &pDbName,
                                                 const int pNum,
                                                 const ClassT pClass,
                                                 const TyreT pTyresRequested,
                                                 const bool pClean) const;
    int getClassLaptimesCount (const QByteArray &pDbName,
                               const ClassT pClass,
                               const TyreT pTyresRequested,
                               const bool pClean = true) const;

    LapsDbLaptimeAnswer getLaptime (const QByteArray &pDbName,
                                    const int pNum,
                                    const CarT pCar,
                                    const TyreT pTyres,
                                    const bool pClean = true) const;

    // non-const because calling const methods on our map will create copies...
    int getLaptimesCount (const QByteArray &pDbName,
                          const CarT pCar,
                          const TyreT pTyresRequested,
                          const bool pClean = true) const;

    LapsDbLaptimeAnswer getLaptimeByQuery (const QByteArray &pDbName,
                                           const int pNum,
                                           const LapsdbQuery &pQuery,
                                           const bool pClean) const;

    int getLaptimesCountByQuery (const QByteArray &pDbName,
                                 const LapsdbQuery &pQuery,
                                 const bool pClean = true) const;

    LapsDbLaptimeAnswer
    findUsernameLaptime(const QByteArray &pDbName,
                        const QByteArray &pUsername,
                        const CarT pCar,
                        const TyreT pTyres,
                        const bool pClean) const;

    LapsDbLaptimeAnswer
    findUsernameClassLaptime (const QByteArray &pDbName,
                                 const QByteArray &pUsername,
                                 const ClassT pClass,
                                 const TyreT pTyres,
                                 const bool pClean) const;

    int findUserLaptimeNumByQuery (const QByteArray &pDbName,
                                   const QByteArray &pUsername,
                                   const LapsdbQuery &pQuery,
                                   const bool pClean) const;


    void wipe (const QByteArray &pDbName);

    int findUserLaptimeNum (const QByteArray &pDbName,
                            const QByteArray &pUsername,
                            const CarT pCar,
                            const TyreT pTyresRequested,
                            const bool pClean) const;

private:
    bool checkDatabaseValidity ();

    const LapsDbLaptimeAnswer
    findUsernameInClassBestlapsList (const QByteArray &pDbName,
                                     const ClassT pClass,
                                     const QByteArray &pUsername,
                                     const bool pClean,
                                     int &bestlapActualIndex) const;

    const LapsDbLaptimeAnswer
    findUsernameInClassLaptimesList (const QByteArray &pDbName,
                                     const QByteArray &pUsername,
                                     const ClassT pClass,
                                     const TyreT pTyres,
                                     const bool pClean,
                                     int &lapActualIndex) const;

    const LapsDbLaptimeAnswer
    findUsernameInLaptimesList (const QByteArray &pDbName,
                                const QByteArray &pUsername,
                                const CarT pCar,
                                const TyreT pTyres,
                                const bool pClean,
                                int &lapActualIndex) const;

    const LapsDbLaptimeAnswer
    findUsernameInBestlapsList(const QByteArray &pDbName,
                               const CarT pCar,
                               const QByteArray &pUsername,
                               const bool pClean,
                               int &bestlapActualLapIndex) const;

    void removeClassesFromDbStorage (const QSet<QByteArray> &pClasses);
    void addClassToDbStorage (const QByteArray &pName,
                              const QSet<CarT> &pCars);
    void dealWithClasses ();


    void removeCarsFromDbStorage (const QSet<QByteArray> &pCars);
    void addCarToDbStorage (const QByteArray &pName,
                            const CarDefinition &pCar);
    void dealWithCars ();


    void changeIndexesInBestlapsList (const QByteArray &pDbName,
                                      const CarT pCar,
                                      const TyreT pTyres,
                                      const int pStartFrom,
                                      const int increment);

    void changeIndexesInClassList (const QByteArray &pDbName,
                                   const ClassT pClass,
                                   const CarT pCar,
                                   const TyreT pTyres,
                                   const int pStartFrom,
                                   const int pIncrement,
                                   const bool pClean = true);

    void changeIndexesInClassBestlapsList (const QByteArray &pDbName,
                                           const ClassT pClass,
                                           const TyreT pTyres,
                                           const int pStartFrom,
                                           const int pIncrement,
                                           const bool pClean = true);


    void changeIndexesInNonCleanLapsList (const QByteArray &pDbName,
                                          const CarT pCar,
                                          const TyreT pTyres,
                                          const bool pChangeClean,
                                          const int pStartFrom,
                                          const int pIncrement);

    void changeIndexesInNonCleanBestlapsList (const QByteArray &pDbName,
                                              const CarT pCar,
                                              const TyreT pTyres,
                                              const int pStartFrom,
                                              const int pIncrement);

    void removeLinkFromClassList (const QByteArray &pDbName,
                                  const ClassT pClass,
                                  const CarT pCar,
                                  const TyreT pTyres,
                                  const int pLink,
                                  const bool pClean = true);

    int insertNoncleanLinkToCleanLaptime (const QByteArray &pDbName,
                                          const CarT pCar,
                                          const TyreT pTyres,
                                          const int pIndex);

    int insertLaptime (const QByteArray &pDbName,
                       const CarT pCar,
                       const TyreT pTyres,
                       const LapsDbStorage::Laptime &pLap,
                       const bool pClean);

    void removeLaptime (const QByteArray &pDbName,
                        const CarT pCar,
                        const TyreT pTyres,
                        const int pLaptimeIndex,
                        const bool pClean);

    void insertBestlapLink (const QByteArray &pDbName,
                            const CarT pCar,
                            const TyreT pTyres,
                            const int pIndex);

    void insertNonCleanBestlapLink (const QByteArray &pDbName,
                                    const CarT pCar,
                                    const TyreT pTyres,
                                    const int pIndex);

    void insertClassBestlapLink (const QByteArray &pDbName,
                                 const ClassT pClass,
                                 const TyreT pTyre,
                                 const int pIndex,
                                 const bool pClean = true);

    void removeClassBestlapLink (const QByteArray &pDbName,
                                 const ClassT pClass,
                                 const CarT pCar,
                                 const QByteArray &pUsername,
                                 const bool pClean = true);

    int findUsernameNumInLaptimesList (const QByteArray &pDbName,
                                       const QByteArray &pUsername,
                                       const CarT pCar, const TyreT pTyre,
                                       const bool pClean) const;

    int findUsernameNumInBestlapsList (const QByteArray &pDbName,
                                       const CarT pCar,
                                       const QByteArray &pUsername,
                                       const bool pClean = true) const;

    void doAddLaptime (const QByteArray &pDbName,
                       const CarT pCar,
                       const TyreT pTyres,
                       const LapsDbStorage::Laptime &pLap,
                       const bool pClean,
                       const bool pIsCleanAndFasterThanNonClean,
                       const bool pInsertBestlapLink,
                       const bool pInsertNonCleanBestlapLink);

    int insertClassLink (const QByteArray &pDbName,
                         const ClassT pClass,
                         const CarT pCar,
                         const TyreT pTyres,
                         const int pIndex,
                         const bool pClean = true);

    std::vector<ClassT> getCarClasses (const CarT pCar) const;

    const LapsDbStorage::Laptime &
    classLaptimeToLaptime (const QByteArray &pDbName,
                           const LapsDbStorage::ClassLaptime &f1,
                           const TyreT pTyres) const;

    const LapsDbStorage::Laptime &
    classLaptimeToLaptime (const QByteArray &pDbName,
                           const LapsDbStorage::NonCleanClassLaptime &f1,
                           const TyreT pTyres) const;

    const LapsDbStorage::Laptime &
    bestlapToLaptime (const QByteArray &pDbName,
                      const LapsDbStorage::Bestlap &f1,
                      const CarT pCar) const;

    const LapsDbStorage::Laptime &
    bestlapNonCleanToLaptime (const QByteArray &pDbName,
                              const LapsDbStorage::Bestlap &pLap,
                              const CarT pCar) const;

    const LapsDbStorage::Laptime &
    classBestlapToLaptime (const QByteArray &pDbName,
                           const LapsDbStorage::ClassBestlap &pLap,
                           const ClassT pClass) const;

    const LapsDbStorage::Laptime &
    classBestlapToLaptime (const QByteArray &pDbName,
                           const LapsDbStorage::NonCleanClassBestlap &pLap,
                           const ClassT pClass) const;

    const LapsDbStorage::Laptime &
    nonCleanLapToLaptime (const QByteArray &pDbName,
                          const LapsDbStorage::NonCleanLap &pLap,
                          const CarT pCar,
                          const TyreT pTyres) const;

    void removeBestlap (const QByteArray &pDbName,
                        const CarT pCar,
                        const int bl_index,
                        const QByteArray &pUsername,
                        bool pClean);

    Log &mLog;

    QString mFileName;

    QMap<QByteArray, LapsDbStorage> mDbs;
    LapsDbStorageMetadata mDbMetadata;

    QMap<QByteArray, QSet<QByteArray>> mClassesStrings;
    QMap<QByteArray, CarDefinition > mCars;

    mutable QMap<QByteArray, std::shared_ptr<QReadWriteLock>> mLocks;
    mutable QReadWriteLock mFileSaveLock;

    static constexpr qint32 mDbFileFormatVersion = 2;

    bool mFileUnsaved = false;
    bool mDbLoaded = false;
};

#endif // LFSTOP_LAPS_DB_HPP_INCLUDED
