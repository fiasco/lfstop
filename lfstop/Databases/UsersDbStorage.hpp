// Copyright 2015 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of LFSTop.
//
// LFSTop is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LFSTop is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LFSTop.  If not, see <http://www.gnu.org/licenses/>.


#ifndef LFSTOP_USERS_DB_STORAGE_HPP_INCLUDED
#define LFSTOP_USERS_DB_STORAGE_HPP_INCLUDED


#include <QMap>
#include <QByteArray>
#include <QDataStream>
#include <QBitArray>


struct UsersDbStorage {
    struct Username {
        Username () {
        }
        Username (const QByteArray &pUsername)
            : data(pUsername) {
        }

        QByteArray data;
    };


    struct UserSettings {
        QBitArray switches;
        QByteArray language;
    };

    struct UserData {
        QByteArray nickname;
        UserSettings settings;
    };

    QMap<Username, UserData> users; // username : nickname
};

inline int qstrcmp_case_insensitive (const QByteArray &pArr1,
                                     const QByteArray &pArr2) {
    const int l1 = pArr1.length();
    const int l2 = pArr2.length();
    const int ret = qstrnicmp(pArr1.constData(),
                              pArr2.constData(),
                              (unsigned) qMin(l1, l2));
    if (ret != 0)
        return ret;

    // they matched qMin(l1, l2) bytes
    // so the longer one is lexically after the shorter one
    return l1 - l2;
}

inline bool operator< (const UsersDbStorage::Username& pUsername1,
                       const UsersDbStorage::Username& pUsername2) {
    return qstrcmp_case_insensitive(pUsername1.data, pUsername2.data) < 0;
}



#endif // LFSTOP_USERS_DB_STORAGE_HPP_INCLUDED
