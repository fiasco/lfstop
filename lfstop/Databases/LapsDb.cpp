// Copyright 2015 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of LFSTop.
//
// LFSTop is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LFSTop is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LFSTop.  If not, see <http://www.gnu.org/licenses/>.


#include "LapsDb.hpp"


#include <QFile>
#include <QFileInfo>
#include <QSet>
#include <QReadWriteLock>


#include "Utils/StringUtils.hpp"
#include "Common.hpp"


LapsDb::LapsDb (Log &pLogger)
    : mLog (pLogger) {
}

LapsDb::~LapsDb() {
    writeFile();
}

void LapsDb::removeClassesFromDbStorage (const QSet<QByteArray> &pClasses) {
    QMutableMapIterator<QByteArray, LapsDbStorage> dbit(mDbs);
    while (dbit.hasNext()) {
        dbit.next();

        for (const QByteArray &class_name : pClasses) {
            dbit.value().classLaps.remove(mDbMetadata.classNames[class_name]);
        }
    }
    for (const QByteArray &class_name : pClasses) {
        mDbMetadata.classes.remove(mDbMetadata.classNames[class_name]);
        mDbMetadata.classNames.remove(class_name);
    }
    mFileUnsaved = true;
}

void LapsDb::addClassToDbStorage (const QByteArray &pName,
                                  const QSet<CarT> &pCars) {
    const ClassT class_id (mDbMetadata.classes.size());
    mDbMetadata.classNames[pName] = class_id;
    mDbMetadata.classes[class_id] = pCars;


    for (const CarT car: pCars) {
        QMapIterator<QByteArray, LapsDbStorage> dbit(mDbs);
        while (dbit.hasNext()) {
            dbit.next();
			// PVS: V807 Decreased performance.Consider creating a reference to avoid using the 'dbit.value().laps[car]' expression repeatedly.lapsdb.cpp 70
            // clean
            {
                QMapIterator<TyreT, QList<LapsDbStorage::Laptime> >
                        tit(dbit.value().laps[car].byTyre);
                while (tit.hasNext()) {
                    tit.next();
                    for (int i = 0; i < tit.value().size(); ++i) {
                        const LapsDbStorage::Laptime &lt = tit.value()[i];
                        const int bl_index =
                                findUsernameNumInBestlapsList(dbit.key(),
                                                           car,
                                                           lt.username);
                        const TyreT bl_tyre =
                                dbit.value().laps[car].best[bl_index].tyres;
                        const bool is_bestlap = (bl_tyre == tit.key());

                        const int newlap_index =
                                insertClassLink(dbit.key(),
                                                class_id,
                                                car,
                                                tit.key(),
                                                i);
                        if (is_bestlap) {
                            insertClassBestlapLink(dbit.key(),
                                                   class_id,
                                                   tit.key(),
                                                   newlap_index);
                        }
                    }
                }
            }
            // nonclean
            {
                QMapIterator<TyreT, QList<LapsDbStorage::NonCleanLap> >
                        tit(dbit.value().laps[car].byTyreNonClean);
                while (tit.hasNext()) {
                    tit.next();
                    for (int i = 0; i < tit.value().size(); ++i) {
                        const LapsDbStorage::Laptime &lt =
                            nonCleanLapToLaptime(dbit.key(), tit.value()[i], car, tit.key());

                        const int bl_index =
                            findUsernameNumInBestlapsList(dbit.key(),
                                                       car,
                                                       lt.username,
                                                       false);
                        const TyreT bl_tyre =
                            dbit.value().laps[car].bestNonClean[bl_index].tyres;
                        const bool is_bestlap = (bl_tyre == tit.key());

                        const int newlap_index =
                                insertClassLink(dbit.key(),
                                                class_id,
                                                car,
                                                tit.key(),
                                                i,
                                                false);
                        if (is_bestlap) {
                            insertClassBestlapLink(dbit.key(),
                                                   class_id,
                                                   tit.key(),
                                                   newlap_index,
                                                   false);
                        }
                    }
                }
            }
        }
    }
    mFileUnsaved = true;
}

void LapsDb::dealWithClasses () {
    // now that we have cars DB synced properly, we can finally
    // fill actual classes sets
    QMap<QByteArray, QSet<CarT> > classes;
    QMapIterator<QByteArray, QSet<QByteArray> > sit (mClassesStrings);
    while (sit.hasNext()) {
        sit.next();
        for (const QByteArray &car_name : sit.value()) {
            classes[sit.key()].insert(getCarByName(car_name.data()));
        }
    }

    QSet<QByteArray> classes_to_remove;
    QMapIterator<QByteArray, ClassT> dbit (mDbMetadata.classNames);
    while (dbit.hasNext()) {
        dbit.next();

        if (classes.contains(dbit.key())) {
            if (mDbMetadata.classes[dbit.value()] == classes[dbit.key()]) {
                continue;
            } else {
                classes_to_remove.insert(dbit.key());
            }
        } else {
            classes_to_remove.insert(dbit.key());
        }
    }
    removeClassesFromDbStorage(classes_to_remove);

    QMapIterator<QByteArray, QSet<CarT> > it(classes);
    while (it.hasNext()) {
        it.next();

        if (mDbMetadata.classNames.contains(it.key())) {
            continue;
        } else {
            addClassToDbStorage(it.key(), it.value());
        }
    }
}

void LapsDb::removeCarsFromDbStorage (const QSet<QByteArray> &pCars) {
    QMutableMapIterator<QByteArray, LapsDbStorage> dbit(mDbs);
    while (dbit.hasNext()) {
        dbit.next();
        for (const QByteArray &car_name : pCars) {
            dbit.value().laps.remove(mDbMetadata.customCarNames[car_name]);
        }
    }
    for (const QByteArray &car_name : pCars) {
        mDbMetadata.customCars.remove(mDbMetadata.customCarNames[car_name]);
        mDbMetadata.customCarNames.remove(car_name);
    }
    mFileUnsaved = true;
}

void LapsDb::addCarToDbStorage (const QByteArray &pName,
                                const CarDefinition &pCar) {
    auto get_free_custom_car_id = [this] () {
        for (uint16_t i = 0; ; ++i) {
            const CarT id = CarT(i + CarT::customCarsOffset);
            if (!mDbMetadata.customCars.keys().contains(id)) {
                return id;
            }
        }
    };

    const CarT car_id = get_free_custom_car_id();


    mDbMetadata.customCarNames[pName] = car_id;
    mDbMetadata.customCars[car_id] = pCar;

    mFileUnsaved = true;
}

void LapsDb::dealWithCars () {
    QWriteLocker file_save_locker (&mFileSaveLock);
    QSet<QByteArray> cars_to_remove;
    QMapIterator<QByteArray, CarT> dbit (mDbMetadata.customCarNames);
    while (dbit.hasNext()) {
        dbit.next();

        if (mCars.contains(dbit.key())) {
            if (mDbMetadata.customCars[dbit.value()] == mCars[dbit.key()]) {
                continue;
            }
            else {
                cars_to_remove.insert(dbit.key());
            }
        }
        else {
            cars_to_remove.insert(dbit.key());
        }
    }
    removeCarsFromDbStorage(cars_to_remove);

    QMapIterator<QByteArray, CarDefinition> it(mCars);
    while (it.hasNext()) {
        it.next();

        if (mDbMetadata.customCarNames.contains(it.key())) {
            continue;
        }
        else {
            addCarToDbStorage(it.key(), it.value());
        }
    }

}

void LapsDb::loadFile (const QString &pTrackname) {
    if (mFileUnsaved) {
        writeFile();
    }

    mFileName = pTrackname + ".dat";

    mLocks = QMap<QByteArray, std::shared_ptr<QReadWriteLock>>();
    for (const char *dbname : {"top", "tbs", "tb", "avg"}) {
        mLocks[dbname] = std::make_shared<QReadWriteLock>();
    }


    QFile file(mFileName);
    const QString abs_path = QFileInfo(mFileName).absoluteFilePath();

    if (file.open(QIODevice::ReadOnly)) {


        QDataStream in(&file);

        QByteArray file_magic_string;
        qint32 actual_file_format_version;
        in >> file_magic_string;
        in >> actual_file_format_version;

        if (file_magic_string != "LfsTopLapsDb") {
            mLog(Log::WARNING) << "wrong file format: " << abs_path;
            mLog(Log::WARNING) << "moving file to: " << abs_path << ".invalid";
            if (!file.rename(mFileName + ".invalid")) {
                mLog(Log::ERROR) << "failed to rename file: " << mFileName;
                throw std::runtime_error("failed to rename file");
            }
            file.rename(mFileName + ".invalid");
        } else if (actual_file_format_version != mDbFileFormatVersion) {
            if (actual_file_format_version < mDbFileFormatVersion) {
                mLog(Log::INFO) << "will probably import laps from previous "
                                   "version database file: " << abs_path;
                mLog(Log::INFO) << "making backup file: " << abs_path
                                   << ".old" << actual_file_format_version;
                QFile::copy(mFileName,
                            mFileName + ".old" +
                            QByteArray::number(actual_file_format_version));

                if (actual_file_format_version == 1) {
                    mLog(Log::INFO) << "importing laps from previous "
                                       "version database file: " << abs_path;
                    LapsDbStorage::load_db_v1(in, mDbs);
                    in >> mDbMetadata;
                }
            } else {
                mLog(Log::WARNING) << "moving future file format version to: "
                                   << abs_path
                                   << ".new" << actual_file_format_version;
                file.rename(mFileName + ".new" +
                            QByteArray::number(actual_file_format_version));
            }
        } else {
            LapsDbStorage::load_db_v2(in, mDbs);
            in >> mDbMetadata;
            mLog() << "Laps database loaded";
        }
    }

    dealWithCars();
    dealWithClasses();


    if (!checkDatabaseValidity()) {
        mLog(Log::WARNING) << "moving file to: " << abs_path << ".invalid";
        QFile::copy(mFileName, mFileName + ".invalid");

        mDbs.clear();
        dealWithCars();
        dealWithClasses();
        // return to the state we had before loading
    }
    else {
        mLog() << "database is valid";
    }
    mDbLoaded = true;
}


void LapsDb::writeFile () {
    QFile file(mFileName);
    const QString abs_path = QFileInfo(mFileName).absoluteFilePath();
    if (file.open(QIODevice::WriteOnly)) {
        QDataStream out(&file);
        out << QByteArray("LfsTopLapsDb") << mDbFileFormatVersion
            << mDbs << mDbMetadata;
        mLog() << "database saved: " << abs_path;
        mFileUnsaved = false;
    } else {
        mLog(Log::ERROR) << "can not write file: " << abs_path;
    }
}

void LapsDb::writeFileIfNeed () {
    // we call write lock here, and read lock in addLaptime,
    // therefore:
    // * we can add laptimes any time when we are not saving file,
    // * we can not write file as long as we add laptime
    QWriteLocker global_lock(&mFileSaveLock);
    if (mFileUnsaved) {
        writeFile();
    }
}


void LapsDb::changeIndexesInBestlapsList (const QByteArray &pDbName,
                                          const CarT pCar,
                                          const TyreT pTyres,
                                          const int pStartFrom,
                                          const int pIncrement) {
    for (LapsDbStorage::Bestlap &bestlap : mDbs[pDbName].laps[pCar].best) {
        if (bestlap.tyres == pTyres
                && bestlap.num >= pStartFrom) {
            bestlap.num += pIncrement;
        }
    }
}

void LapsDb::changeIndexesInClassList (const QByteArray &pDbName,
                                       const ClassT pClass,
                                       const CarT pCar,
                                       const TyreT pTyres,
                                       const int pStartFrom,
                                       const int pIncrement,
                                       const bool pClean) {
    if (pClean) {
        for (LapsDbStorage::ClassLaptime &classlaptime
             : mDbs[pDbName].classLaps[pClass].byTyre[pTyres]) {
            if (classlaptime.car == pCar && classlaptime.num >= pStartFrom) {
                classlaptime.num += pIncrement;
            }
        }
    } else {
        for (LapsDbStorage::NonCleanClassLaptime &classlaptime
             : mDbs[pDbName].classLaps[pClass].byTyreNonClean[pTyres]) {
            if (classlaptime.car == pCar && classlaptime.num >= pStartFrom) {
                classlaptime.num += pIncrement;
            }
        }
    }
}

void LapsDb::changeIndexesInClassBestlapsList (const QByteArray &pDbName,
                                               const ClassT pClass,
                                               const TyreT pTyres,
                                               const int pStartFrom,
                                               const int pIncrement,
                                               const bool pClean) {
    if (pClean) {
        for (LapsDbStorage::ClassBestlap &classbestlap
                 : mDbs[pDbName].classLaps[pClass].best) {
            if (classbestlap.tyres == pTyres
                && classbestlap.num >= pStartFrom) {
                classbestlap.num += pIncrement;
            }
        }
    } else {
        for (LapsDbStorage::NonCleanClassBestlap &classbestlap
                 : mDbs[pDbName].classLaps[pClass].bestNonClean) {
            if (classbestlap.tyres == pTyres
                && classbestlap.num>= pStartFrom) {
                classbestlap.num += pIncrement;
            }
        }
    }
}

void LapsDb::changeIndexesInNonCleanLapsList (const QByteArray &pDbName,
                                              const CarT pCar,
                                              const TyreT pTyres,
                                              const bool pChangeClean,
                                              const int pStartFrom,
                                              const int pIncrement) {
    for (LapsDbStorage::NonCleanLap &nonclean: mDbs[pDbName].laps[pCar].byTyreNonClean[pTyres]) {
        if (nonclean.isClean == pChangeClean
            && nonclean.num >= pStartFrom) {
            nonclean.num += pIncrement;
        }
    }
}

void LapsDb::changeIndexesInNonCleanBestlapsList (const QByteArray &pDbName,
                                                  const CarT pCar,
                                                  const TyreT pTyres,
                                                  const int pStartFrom,
                                                  const int pIncrement) {
    for (auto &nonclean_bl : mDbs[pDbName].laps[pCar].bestNonClean) {
        if (nonclean_bl.tyres == pTyres
            && nonclean_bl.num >= pStartFrom) {
                nonclean_bl.num += pIncrement;
        }
    }
}

void LapsDb::removeLinkFromClassList (const QByteArray &pDbName,
                                      const ClassT pClass,
                                      const CarT pCar,
                                      const TyreT pTyres,
                                      const int pLink,
                                      const bool pClean) {
    if (pClean) {
        QList<LapsDbStorage::ClassLaptime> &list =
                mDbs[pDbName].classLaps[pClass].byTyre[pTyres];
        for (int i = 0; i < list.size(); ++i) {
            if (list[i].car == pCar
                && list[i].num == pLink) {
                list.removeAt(i);
                changeIndexesInClassBestlapsList(pDbName, pClass, pTyres, i, -1);
                return;
            }
        }
    } else {
        QList<LapsDbStorage::NonCleanClassLaptime> &list =
                mDbs[pDbName].classLaps[pClass].byTyreNonClean[pTyres];
        for (int i = 0; i < list.size(); ++i) {
            if (list[i].car == pCar
                && list[i].num == pLink) {
                list.removeAt(i);
                changeIndexesInClassBestlapsList(pDbName, pClass, pTyres, i, -1, false);
                return;
            }
        }
    }
}


int LapsDb::insertNoncleanLinkToCleanLaptime (const QByteArray &pDbName,
                                              const CarT pCar,
                                              const TyreT pTyres,
                                              const int pIndex) {


    QList<LapsDbStorage::NonCleanLap> &list =
            mDbs[pDbName].laps[pCar].byTyreNonClean[pTyres];

    auto comp = [&] (const LapsDbStorage::NonCleanLap &ncl1,
                     const LapsDbStorage::NonCleanLap &ncl2) {
        return nonCleanLapToLaptime(pDbName, ncl1, pCar, pTyres)
                < nonCleanLapToLaptime(pDbName, ncl2, pCar, pTyres);
    };

    LapsDbStorage::NonCleanLap newlap { true, pIndex };

    QList<LapsDbStorage::NonCleanLap>::iterator it
            = std::upper_bound(list.begin(), list.end(), newlap, comp);

    if (it != list.end()) {
        const int i = it - list.begin();
        list.insert(it, newlap);
        return i;
    } else {
        list.append(newlap);
        return list.size() - 1;
    }
}

int LapsDb::insertLaptime (const QByteArray &pDbName,
                           const CarT pCar,
                           const TyreT pTyres,
                           const LapsDbStorage::Laptime &pLap,
                           const bool pClean) {
    if (pClean) {
        QList<LapsDbStorage::Laptime> &list =
                mDbs[pDbName].laps[pCar].byTyre[pTyres];

        QList<LapsDbStorage::Laptime>::iterator it
                = std::upper_bound(list.begin(), list.end(), pLap);

        if (it != list.end()) {
            const int i = it - list.begin();
            list.insert(it, pLap);
            changeIndexesInNonCleanLapsList(pDbName, pCar, pTyres, true, i, 1);
            changeIndexesInBestlapsList(pDbName, pCar, pTyres, i, 1);
            for (const ClassT c : getCarClasses(pCar)) {
                changeIndexesInClassList(pDbName, c, pCar, pTyres, i, 1);
            }
            return i;
        } else {
            list.append(pLap);
            return list.size() - 1;
        }
    } else {
        int stor_i;
        {

            QList<LapsDbStorage::Laptime> &list_ =
                    mDbs[pDbName].laps[pCar].byTyreNonCleanStorage[pTyres];


            QList<LapsDbStorage::Laptime>::iterator it_
                    = std::upper_bound(list_.begin(), list_.end(), pLap);

            if (it_ != list_.end()) {
                stor_i = it_ - list_.begin();
                list_.insert(it_, pLap);
                changeIndexesInNonCleanLapsList(pDbName, pCar, pTyres, false, stor_i, 1);
            } else {
                stor_i = list_.size();
                list_.append(pLap);
            }

        }


        QList<LapsDbStorage::NonCleanLap> &list =
                mDbs[pDbName].laps[pCar].byTyreNonClean[pTyres];

        auto comp = [&] (const LapsDbStorage::NonCleanLap &ncl1,
                         const LapsDbStorage::NonCleanLap &ncl2) {
            return nonCleanLapToLaptime(pDbName, ncl1, pCar, pTyres)
                    < nonCleanLapToLaptime(pDbName, ncl2, pCar, pTyres);
        };

        LapsDbStorage::NonCleanLap newlap { false, stor_i };

        QList<LapsDbStorage::NonCleanLap>::iterator it
                = std::upper_bound(list.begin(), list.end(), newlap, comp);

        if (it != list.end()) {
            const int i = it - list.begin();
            list.insert(it, newlap);
            for (const ClassT c : getCarClasses(pCar)) {
                changeIndexesInClassList(pDbName, c, pCar, pTyres, i, 1, false);
            }
            return i;
        } else {
            list.append(newlap);
            return list.size() - 1;
        }
    }
    return -1;
}

void LapsDb::removeLaptime (const QByteArray &pDbName,
                            const CarT pCar,
                            const TyreT pTyres,
                            const int pLaptimeIndex,
                            const bool pClean) {
    if (pClean) {
        QList<LapsDbStorage::Laptime> &list =
                mDbs[pDbName].laps[pCar].byTyre[pTyres];
        list.removeAt(pLaptimeIndex);
        changeIndexesInNonCleanLapsList(pDbName, pCar, pTyres, true, pLaptimeIndex, -1);
        changeIndexesInBestlapsList(pDbName, pCar, pTyres, pLaptimeIndex, -1);
        for (const ClassT c : getCarClasses(pCar)) {
            removeLinkFromClassList(pDbName, c, pCar, pTyres, pLaptimeIndex);
            changeIndexesInClassList(pDbName, c, pCar, pTyres, pLaptimeIndex, -1);
        }
    } else {
        QList<LapsDbStorage::NonCleanLap> &list =
                mDbs[pDbName].laps[pCar].byTyreNonClean[pTyres];

        const LapsDbStorage::NonCleanLap non_clean_lap = list[pLaptimeIndex];
        if (non_clean_lap.isClean) {
            list.removeAt(pLaptimeIndex);
            changeIndexesInNonCleanBestlapsList(pDbName, pCar, pTyres, pLaptimeIndex, -1);
        } else {
            mDbs[pDbName].laps[pCar].byTyreNonCleanStorage[pTyres].removeAt(non_clean_lap.num);
            changeIndexesInNonCleanBestlapsList(pDbName, pCar, pTyres, pLaptimeIndex, -1);
            changeIndexesInNonCleanLapsList(pDbName, pCar, pTyres, non_clean_lap.isClean, non_clean_lap.num, -1);
            list.removeAt(pLaptimeIndex);
        }
        for (const ClassT c : getCarClasses(pCar)) {
            removeLinkFromClassList(pDbName, c, pCar, pTyres, pLaptimeIndex, false);
            changeIndexesInClassList(pDbName, c, pCar, pTyres, pLaptimeIndex, -1, false);
        }
    }
}

void LapsDb::insertBestlapLink (const QByteArray &pDbName,
                                const CarT pCar,
                                const TyreT pTyres,
                                const int pIndex) {
    assert(pIndex >= 0);
    QList<LapsDbStorage::Bestlap> &list = mDbs[pDbName].laps[pCar].best;
    const LapsDbStorage::Bestlap lap_to_insert = { pTyres, pIndex };

    auto comp = [&] (const LapsDbStorage::Bestlap &bl1,
                     const LapsDbStorage::Bestlap &bl2) {
        return bestlapToLaptime(pDbName, bl1, pCar)
                < bestlapToLaptime(pDbName, bl2, pCar);
    };

    QList<LapsDbStorage::Bestlap>::iterator it
            = std::upper_bound(list.begin(), list.end(), lap_to_insert, comp);

    list.insert(it, lap_to_insert);
}

void LapsDb::insertNonCleanBestlapLink (const QByteArray &pDbName,
                                        const CarT pCar,
                                        const TyreT pTyres,
                                        const int pIndex) {
    assert(pIndex >= 0);
    QList<LapsDbStorage::Bestlap> &list = mDbs[pDbName].laps[pCar].bestNonClean;
    const LapsDbStorage::Bestlap lap_to_insert { pTyres, pIndex };

    auto comp = [&] (const LapsDbStorage::Bestlap &bl1,
                     const LapsDbStorage::Bestlap &bl2) {
        return bestlapNonCleanToLaptime(pDbName, bl1, pCar)
                < bestlapNonCleanToLaptime(pDbName, bl2, pCar);
    };

    QList<LapsDbStorage::Bestlap>::iterator it
            = std::upper_bound(list.begin(), list.end(), lap_to_insert, comp);

    list.insert(it, lap_to_insert);
}

void LapsDb::insertClassBestlapLink (const QByteArray &pDbName,
                                     const ClassT pClass,
                                     const TyreT pTyre,
                                     const int pIndex,
                                     const bool pClean) {
    if (pClean) {
        QList<LapsDbStorage::ClassBestlap> &list =
            mDbs[pDbName].classLaps[pClass].best;
        const LapsDbStorage::ClassBestlap clt { pTyre, pIndex };

        auto comp = [&] (const LapsDbStorage::ClassBestlap& cbl1,
                         const LapsDbStorage::ClassBestlap& cbl2) {
            return classBestlapToLaptime(pDbName, cbl1, pClass).laptime
            < classBestlapToLaptime(pDbName, cbl2, pClass).laptime;
        };

        QList<LapsDbStorage::ClassBestlap>::iterator it
            = std::upper_bound(list.begin(), list.end(), clt, comp);
        list.insert(it, clt);
    } else {
        QList<LapsDbStorage::NonCleanClassBestlap> &list =
            mDbs[pDbName].classLaps[pClass].bestNonClean;
        const LapsDbStorage::NonCleanClassBestlap clt { pTyre, pIndex };

        auto comp = [&] (const LapsDbStorage::NonCleanClassBestlap& cbl1,
                         const LapsDbStorage::NonCleanClassBestlap& cbl2) {
            return classBestlapToLaptime(pDbName, cbl1, pClass).laptime
                < classBestlapToLaptime(pDbName, cbl2, pClass).laptime;
        };

        list.insert(std::upper_bound(list.begin(), list.end(), clt, comp), clt);
    }
}


int LapsDb::findUsernameNumInLaptimesList (const QByteArray &pDbName,
                                           const QByteArray &pUsername,
                                           const CarT pCar,
                                           const TyreT pTyre,
                                           const bool pClean) const {
    assert(pTyre != TyreT::unknownTyres());
    int index = -1;

    findUsernameInLaptimesList(pDbName,
                               pUsername,
                               pCar,
                               pTyre,
                               pClean,
                               index);

    return index;
}

const LapsDbLaptimeAnswer
LapsDb::findUsernameInLaptimesList (const QByteArray &pDbName,
                                    const QByteArray &pUsername,
                                    const CarT pCar,
                                    const TyreT pTyres,
                                    const bool pClean,
                                    int &lapActualIndex) const {
    assert(pTyres != TyreT::unknownTyres());
    if (pClean) {
        auto &list = mDbs[pDbName].laps[pCar].byTyre[pTyres];

        for (int i = 0; i < list.size(); ++i) {
            if ((pUsername.toLower() == list[i].username.toLower())
                || (pUsername[0] == '/' && list[i].username[0] == '/')) {
                lapActualIndex = i;
                return { list[i], pTyres, true, pCar, i };
            }
        }
    } else {
        auto &list = mDbs[pDbName].laps[pCar].byTyreNonClean[pTyres];

        for (int i = 0; i < list.size(); ++i) {
            const LapsDbStorage::Laptime &lt =
                    nonCleanLapToLaptime(pDbName, list[i], pCar, pTyres);

            if ((pUsername.toLower() == lt.username.toLower())
                    || (pUsername[0] == '/' && lt.username[0] == '/')) {
                lapActualIndex = i;
                return { lt, pTyres, list[i].isClean, pCar, i };
            }
        }
    }

    throw LapNotFoundException();
}

const LapsDbLaptimeAnswer
LapsDb::findUsernameInClassBestlapsList (const QByteArray &pDbName,
                                         const ClassT pClass,
                                         const QByteArray &pUsername,
                                         const bool pClean,
                                         int &bestlapActualIndex) const {
    Q_ASSERT(pUsername.size());
    const auto &laps_list = mDbs[pDbName].classLaps[pClass];

    if (pClean) {
        for (int i = 0; i < laps_list.best.size(); ++i) {
            const LapsDbStorage::Laptime &laptime_i =
                classBestlapToLaptime(pDbName,laps_list.best[i], pClass);

            // username starting with '/' is a special case:
            // it means it is a wr time.
            // here WR time represents 1 user named '/'. While further part
            // of username is actual name of the guy who set WR
            if ((pUsername.toLower() == laptime_i.username.toLower())
                    || (pUsername[0] == '/' && laptime_i.username[0] == '/')) {
                bestlapActualIndex = i;
                return { laptime_i, laps_list.best[i].tyres, true, i };
            }
        }
    } else {
        for (int i = 0; i < laps_list.bestNonClean.size(); ++i) {
            const LapsDbStorage::Laptime &laptime_i =
                classBestlapToLaptime(pDbName, laps_list.bestNonClean[i], pClass);

            // username starting with '/' is a special case:
            // it means it is a wr time.
            // here WR time represents 1 user named '/'. While further part
            // of username is actual name of the guy who set WR
            if ((pUsername.toLower() == laptime_i.username.toLower())
                    || (pUsername[0] == '/' && laptime_i.username[0] == '/')) {
                bestlapActualIndex = i;
                const auto &bnc = laps_list.bestNonClean[i];
                const auto &nc = laps_list.byTyreNonClean[bnc.tyres][bnc.num];
                const LapsDbStorage::NonCleanLap &ncl =
                    mDbs[pDbName].laps[nc.car].byTyreNonClean[bnc.tyres][nc.num];

                return { laptime_i, bnc.tyres, ncl.isClean, i };
            }
        }
    }

    throw LapNotFoundException();
}

const LapsDbLaptimeAnswer
LapsDb::findUsernameInClassLaptimesList (const QByteArray &pDbName,
                                         const QByteArray &pUsername,
                                         const ClassT pClass,
                                         const TyreT pTyres,
                                         const bool pClean,
                                         int &lapActualIndex) const {
    assert(pTyres != TyreT::unknownTyres());
    if (pClean) {
        auto &list = mDbs[pDbName].classLaps[pClass].byTyre[pTyres];

        for (int i = 0; i < list.size(); ++i) {
            const auto &lap = classLaptimeToLaptime(pDbName, list[i], pTyres);
            if ((pUsername.toLower() == lap.username.toLower())
                || (pUsername[0] == '/' && lap.username[0] == '/')) {
                lapActualIndex = i;
                return { lap, pTyres, true, list[i].car, i };
            }
        }
    } else {
        auto &list = mDbs[pDbName].classLaps[pClass].byTyreNonClean[pTyres];

        for (int i = 0; i < list.size(); ++i) {
            const auto cnc = list[i];
            const auto nc = mDbs[pDbName].laps[cnc.car].byTyreNonClean[pTyres][cnc.num];

            const auto &lt =
                    classLaptimeToLaptime(pDbName, list[i], pTyres);


            if ((pUsername.toLower() == lt.username.toLower())
                || (pUsername[0] == '/' && lt.username[0] == '/')) {
                lapActualIndex = i;
                return { lt, pTyres, nc.isClean, cnc.car, i };
            }
        }
    }

    throw LapNotFoundException();
}

const LapsDbLaptimeAnswer
LapsDb::findUsernameInBestlapsList (const QByteArray &pDbName,
                                    const CarT pCar,
                                    const QByteArray &pUsername,
                                    const bool pClean,
                                    int &bestlapActualIndex) const {
    Q_ASSERT(pUsername.size());

    const LapsDbStorage::CarLaptimes &laps_list = mDbs[pDbName].laps[pCar];

    if (pClean) {
        for (int i = 0; i < laps_list.best.size(); ++i) {
            const LapsDbStorage::Laptime &laptime_i =
                    bestlapToLaptime (pDbName, laps_list.best[i], pCar);

            // username starting with '/' is a special case:
            // it means it is a wr time.
            // here WR time represents 1 user named '/'. While further part
            // of username is actual name of the guy who set WR
            if ((pUsername.toLower() == laptime_i.username.toLower())
                    || (pUsername[0] == '/' && laptime_i.username[0] == '/')) {
                bestlapActualIndex = i;
                return { laptime_i, laps_list.best[i].tyres, true, i };
            }
        }
    } else {
        for (int i = 0; i < laps_list.bestNonClean.size(); ++i) {
            const LapsDbStorage::Laptime &laptime_i =
                    bestlapNonCleanToLaptime (pDbName, laps_list.bestNonClean[i], pCar);

            // username starting with '/' is a special case:
            // it means it is a wr time.
            // here WR time represents 1 user named '/'. While further part
            // of username is actual name of the guy who set WR
            if ((pUsername.toLower() == laptime_i.username.toLower())
                    || (pUsername[0] == '/' && laptime_i.username[0] == '/')) {
                bestlapActualIndex = i;
                const auto &bnc = laps_list.bestNonClean[i];
                const auto &nc = laps_list.byTyreNonClean[bnc.tyres][bnc.num];
                return { laptime_i, bnc.tyres, nc.isClean, i };
            }
        }
    }

    throw LapNotFoundException();
}


int
LapsDb::findUsernameNumInBestlapsList (const QByteArray &pDbName,
                                       const CarT pCar,
                                       const QByteArray &pUsername,
                                       const bool pClean) const {
    Q_ASSERT(pUsername.size());

    const LapsDbStorage::CarLaptimes &laps_list = mDbs[pDbName].laps[pCar];

    if (pClean) {
        for (int i = 0; i < laps_list.best.size(); ++i) {
            const TyreT tyres_i = laps_list.best[i].tyres;
            const int laplink_i = laps_list.best[i].num;
            const LapsDbStorage::Laptime &laptime_i =
                    laps_list.byTyre[tyres_i][laplink_i];

            // username starting with '/' is a special case:
            // it means it is a wr time.
            // here WR time represents 1 user named '/'. While further part
            // of username is actual name of the guy who set WR
            if (pUsername.toLower() == laptime_i.username.toLower()) {
                return i;
            } else if (pUsername[0] == '/') {
                if (laptime_i.username[0] == '/') {
                    return i;
                }
            }
        }
    } else {
        for (int i = 0; i < laps_list.bestNonClean.size(); ++i) {
            const TyreT tyres_i = laps_list.bestNonClean[i].tyres;
            const int laplink_i = laps_list.bestNonClean[i].num;
            const LapsDbStorage::Laptime &laptime_i =
                nonCleanLapToLaptime(pDbName,
                                     laps_list.byTyreNonClean[tyres_i][laplink_i],
                                     pCar,
                                     tyres_i);

            // username starting with '/' is a special case:
            // it means it is a wr time.
            // here WR time represents 1 user named '/'. While further part
            // of username is actual name of the guy who set WR
            if (pUsername.toLower() == laptime_i.username.toLower()) {
                return i;
            } else if (pUsername[0] == '/') {
                if (laptime_i.username[0] == '/') {
                    return i;
                }
            }
        }
    }



    return LapsDbStorage::not_found;
}

std::vector<ClassT> LapsDb::getCarClasses (const CarT pCar) const {
    std::vector<ClassT> ret_vector;

    QMapIterator<ClassT, QSet<CarT> > i(mDbMetadata.classes);
    while (i.hasNext()) {
        i.next();
        if (i.value().contains(pCar)) {
            ret_vector.push_back(i.key());
        }
    }
    return ret_vector;
}

void LapsDb::addClass (const QByteArray &pClassName,
                       const QSet<QByteArray> &pCars) {
    Q_ASSERT(!mDbLoaded);

    mClassesStrings[pClassName] = pCars;
}


void LapsDb::addCar (const QByteArray &pCarName,
                     const CarDefinition &pCar) {
    Q_ASSERT(!mDbLoaded);

    mCars[pCarName] = pCar;
}

QByteArray LapsDb::queryToString (const QByteArray &pDbName,
                                  const LapsdbQuery &pQuery,
                                  const Translation::TranslationBinder &pTr) {
    using Translation::StringIds;
    using LfsInsimUtils::insertStr;

    QByteArray ret("^7");
    if (pQuery.carsClass != ClassT::notFound()) {
        ret += getClassName(pQuery.carsClass);
    } else {
        ret += getCarName(pQuery.car);
    }
    ret += "^8 ";

    ret += pTr.get(Translation::StringIds::LAPS);

    if (pQuery.tyres != TyreT::unknownTyres()) {
        ret += ' ';
        ret += insertStr(pTr.get(StringIds::WITH_SOME_TYRES),
                         pQuery.tyres.toString());;
    }

    const int num =
            pQuery.carsClass != ClassT::notFound()
            ? getClassLaptimesCount(pDbName, pQuery.carsClass, pQuery.tyres)
            : getLaptimesCount(pDbName, pQuery.car, pQuery.tyres);
    if (num > 0) {
        ret += " (" + QByteArray::number(num)+ ")";
    }
    return ret;
}

CarT LapsDb::getCarByCarDefinition (const CarDefinition &pCar) {
    if (pCar.addedMass != 0 || pCar.intakeRestriction != 0) {
        const CarDefinition *match = nullptr;
        CarT matched_car;
        QMapIterator<CarT, CarDefinition> it(mDbMetadata.customCars);
        while (it.hasNext()) {
            it.next();
            if (it.value().lfsCar == pCar.lfsCar) {
                if (it.value() == pCar) {
                    return it.key();
                } else if (it.value() <= pCar) {
                    if (match == nullptr || *match < it.value()) {
                        match = &it.value();
                        matched_car = it.key();
                    }
                }
            }
        }
        if (matched_car != CarT::unknownCar()) {
            return matched_car;
        }
    }
    return pCar.lfsCar;
}


int LapsDb::insertClassLink (const QByteArray &pDbName,
                             const ClassT pClass,
                             const CarT pCar,
                             const TyreT pTyres,
                             const int pIndex,
                             const bool pClean) {
    if (pClean) {
        QList<LapsDbStorage::ClassLaptime>& list =
                mDbs[pDbName].classLaps[pClass].byTyre[pTyres];
        const LapsDbStorage::ClassLaptime class_lap { pCar, pIndex };

        auto comp = [&] (const LapsDbStorage::ClassLaptime &cl1,
                const LapsDbStorage::ClassLaptime &cl2) {
            return classLaptimeToLaptime(pDbName, cl1, pTyres)
                    < classLaptimeToLaptime(pDbName, cl2, pTyres);
        };
        QList<LapsDbStorage::ClassLaptime>::iterator it
                = std::upper_bound(list.begin(), list.end(), class_lap, comp);

        if (it != list.end()) {
            const int i = it - list.begin();
            list.insert(it, class_lap);
            changeIndexesInClassBestlapsList(pDbName, pClass, pTyres, i, 1);
            return i;
        }

        list.append(class_lap);
        return list.size() - 1;
    } else {
        QList<LapsDbStorage::NonCleanClassLaptime>& list =
                mDbs[pDbName].classLaps[pClass].byTyreNonClean[pTyres];
        const LapsDbStorage::NonCleanClassLaptime class_lap { pCar, pIndex };

        auto comp = [&] (const LapsDbStorage::NonCleanClassLaptime &cl1,
                         const LapsDbStorage::NonCleanClassLaptime &cl2) {
            return classLaptimeToLaptime(pDbName, cl1, pTyres)
                    < classLaptimeToLaptime(pDbName, cl2, pTyres);
        };
        QList<LapsDbStorage::NonCleanClassLaptime>::iterator it
                = std::upper_bound(list.begin(), list.end(), class_lap, comp);

        if (it != list.end()) {
            const int i = it - list.begin();
            list.insert(it, class_lap);
            changeIndexesInClassBestlapsList(pDbName, pClass, pTyres, i, 1, false);
            return i;
        }

        list.append(class_lap);
        return list.size() - 1;
    }
}

void LapsDb::doAddLaptime (const QByteArray &pDbName,
                           const CarT pCar,
                           const TyreT pTyres,
                           const LapsDbStorage::Laptime &pLap,
                           const bool pClean,
                           const bool pIsCleanAndFasterThanNonClean,
                           const bool pInsertBestlapLink,
                           const bool pInsertNonCleanBestlapLink) {
    if (pClean) {
        const int newlap_index = insertLaptime(pDbName, pCar, pTyres, pLap, true);
        if (pIsCleanAndFasterThanNonClean) {
            const int nonclean_index =
                    insertNoncleanLinkToCleanLaptime(pDbName, pCar, pTyres, newlap_index);


            changeIndexesInNonCleanBestlapsList(pDbName, pCar, pTyres, nonclean_index, 1);
            for (const ClassT c : getCarClasses(pCar)) {
                changeIndexesInClassList(pDbName, c, pCar, pTyres, nonclean_index, 1, false);
                const int class_index =
                        insertClassLink(pDbName, c, pCar, pTyres, nonclean_index, false);
                if (pInsertNonCleanBestlapLink) {
                    insertClassBestlapLink(pDbName, c, pTyres, class_index, false);
                }
            }

            if (pInsertNonCleanBestlapLink) {
                insertNonCleanBestlapLink(pDbName, pCar, pTyres, nonclean_index);
            }
        }
        for (const ClassT c : getCarClasses(pCar)) {
            const int class_index =
                    insertClassLink(pDbName, c, pCar, pTyres, newlap_index);
            if (pInsertBestlapLink) {
                insertClassBestlapLink(pDbName, c, pTyres, class_index);
            }
        }
        if (pInsertBestlapLink) {
            insertBestlapLink(pDbName, pCar, pTyres, newlap_index);
        }
    } else {
        const int newlap_index = insertLaptime(pDbName, pCar, pTyres, pLap, false);
        changeIndexesInNonCleanBestlapsList(pDbName, pCar, pTyres, newlap_index, 1);

        if (pInsertNonCleanBestlapLink) {
            insertNonCleanBestlapLink(pDbName, pCar, pTyres, newlap_index);
        }

        for (const ClassT c : getCarClasses(pCar)) {
            const int class_index =
                    insertClassLink(pDbName, c, pCar, pTyres, newlap_index, false);
            if (pInsertNonCleanBestlapLink) {
                insertClassBestlapLink(pDbName, c, pTyres, class_index, false);
            }
        }
    }
    mFileUnsaved = true;
}

void LapsDb::removeClassBestlapLink (const QByteArray &pDbName,
                                     const ClassT pClass,
                                     const CarT pCar,
                                     const QByteArray &pUsername,
                                     const bool pClean) {
    if (pClean) {
        QList<LapsDbStorage::ClassBestlap> &list_best =
            mDbs[pDbName].classLaps[pClass].best;
        for (int i = 0; i < list_best.size(); ++i) {
            const TyreT tyres_i = list_best[i].tyres;
            const int index_i = list_best[i].num;
            const LapsDbStorage::ClassLaptime &class_laptime_i =
                mDbs[pDbName].classLaps[pClass].byTyre[tyres_i][index_i];
            if (class_laptime_i.car == pCar) {
                const LapsDbStorage::Laptime &laptime_i =
                    mDbs[pDbName].laps[pCar].byTyre[tyres_i][class_laptime_i.num];
                if (laptime_i.username.toLower() == pUsername.toLower()) {
                    list_best.removeAt(i);
                    return;
                } else if (pUsername[0] == '/') {
                    if (laptime_i.username[0] == '/') {
                        list_best.removeAt(i);
                        return;
                    }
                }
            }
        }
    } else {
        QList<LapsDbStorage::NonCleanClassBestlap> &list_best =
            mDbs[pDbName].classLaps[pClass].bestNonClean;
        for (int i = 0; i < list_best.size(); ++i) {
            const TyreT tyres_i = list_best[i].tyres;
            const int index_i = list_best[i].num;
            const LapsDbStorage::NonCleanClassLaptime &class_laptime_i =
                mDbs[pDbName].classLaps[pClass].byTyreNonClean[tyres_i][index_i];
            if (class_laptime_i.car == pCar) {
                const LapsDbStorage::Laptime &laptime_i =
                    nonCleanLapToLaptime(pDbName,
                                         mDbs[pDbName].laps[pCar].byTyreNonClean[tyres_i][class_laptime_i.num],
                                         pCar,
                                         tyres_i);
                if (laptime_i.username.toLower() == pUsername.toLower()) {
                    list_best.removeAt(i);
                    return;
                } else if (pUsername[0] == '/') {
                    if (laptime_i.username[0] == '/') {
                        list_best.removeAt(i);
                        return;
                    }
                }
            }
        }
    }
}



void LapsDb::removeBestlap (const QByteArray &pDbName,
                            const CarT pCar,
                            const int bl_index,
                            const QByteArray &pUsername,
                            bool pClean) {
    LapsDbStorage::CarLaptimes &car_laptimes = mDbs[pDbName].laps[pCar];

    if (pClean) {
        car_laptimes.best.removeAt(bl_index);
        for (const ClassT c : getCarClasses(pCar)) {
            removeClassBestlapLink(pDbName, c, pCar, pUsername);
        }
    } else {
        car_laptimes.bestNonClean.removeAt(bl_index);
        for (const ClassT c : getCarClasses(pCar)) {
            removeClassBestlapLink(pDbName, c, pCar, pUsername, false);
        }
    }
}

LapsDbAddAnswer LapsDb::addLaptime (const QByteArray &pDbName,
                                    const CarT pCar,
                                    const TyreT pTyres,
                                    const LapsDbStorage::Laptime &pLaptime,
                                    const bool pClean) {
    Q_ASSERT(pLaptime.username.size() > 0);
    QReadLocker file_save_lock(&mFileSaveLock);
    QWriteLocker locker(mLocks[pDbName].get());
    LapsDbAddAnswer answer { 0, false, false };

    bool is_non_clean_bestlap = false;

    if (pClean) {
        try {
            int bl_index;
            const auto old_bestlap =
                findUsernameInBestlapsList(pDbName, pCar, pLaptime.username, true, bl_index);

            if (old_bestlap > pLaptime) {
                answer.difference = old_bestlap.laptime - pLaptime.laptime;
                answer.isBestLap = true;
                removeBestlap(pDbName, pCar, bl_index, pLaptime.username, true);
            }
        } catch (LapNotFoundException) {
            answer.isBestLap = true;
        }
    }

    try {
        int bl_index;
        const auto old_bestlap =
                findUsernameInBestlapsList(pDbName, pCar, pLaptime.username, false, bl_index);

        if (old_bestlap > pLaptime
            || (old_bestlap == pLaptime
                && old_bestlap.clean == false
                && pClean)) {
            removeBestlap(pDbName, pCar, bl_index, pLaptime.username, false);
            is_non_clean_bestlap = true;
        }
    } catch (LapNotFoundException) {
        is_non_clean_bestlap = true;
    }


    if (pClean) {
        bool is_faster_than_nonclean = false;
        try {
            int nonclean_oldlap_index;
            const auto &nonclean_oldlap =
                findUsernameInLaptimesList(pDbName, pLaptime.username, pCar, pTyres, false, nonclean_oldlap_index);
            if (nonclean_oldlap > pLaptime
                || (nonclean_oldlap == pLaptime
                    && nonclean_oldlap.clean == false)) {
                removeLaptime(pDbName, pCar, pTyres, nonclean_oldlap_index, false);
                is_faster_than_nonclean = true;
            }
        } catch (LapNotFoundException) {
            is_faster_than_nonclean = true;
        }

        try {
            int oldlap_index;
            const auto old_laptime =
                    findUsernameInLaptimesList(pDbName, pLaptime.username, pCar, pTyres, pClean, oldlap_index);
            if (old_laptime > pLaptime) {
                if (answer.difference == 0) {
                    answer.difference = old_laptime.laptime - pLaptime.laptime;
                }
                answer.isAdded = true;

                removeLaptime(pDbName, pCar, pTyres, oldlap_index, pClean);
                doAddLaptime(pDbName, pCar, pTyres, pLaptime, true, is_faster_than_nonclean, answer.isBestLap, is_non_clean_bestlap);
            }
        } catch (LapNotFoundException) {
            answer.isAdded = true;
            doAddLaptime(pDbName, pCar, pTyres, pLaptime, true, is_faster_than_nonclean, answer.isBestLap, is_non_clean_bestlap);
        }
    } else {
        try {
            int nonclean_oldlap_index;
            const LapsDbStorage::Laptime &nonclean_oldlap =
                    findUsernameInLaptimesList(pDbName, pLaptime.username, pCar, pTyres, pClean, nonclean_oldlap_index);
            if (nonclean_oldlap > pLaptime) {
                removeLaptime(pDbName, pCar, pTyres, nonclean_oldlap_index, pClean);
                doAddLaptime(pDbName, pCar, pTyres, pLaptime, pClean, false, false, is_non_clean_bestlap);
            }
        } catch (LapNotFoundException) {
            doAddLaptime(pDbName, pCar, pTyres, pLaptime, pClean, false, false, is_non_clean_bestlap);
        }
    }

    return answer;
}


LapsDbLaptimeAnswer LapsDb::getLaptime (const QByteArray &pDbName,
                                        const int pNum,
                                        const CarT pCar,
                                        const TyreT pTyres,
                                        const bool pClean) const {
    QReadLocker locker(mLocks[pDbName].get());

    if (pClean) {
        if (pTyres == TyreT::unknownTyres()) {
            const auto &bestlap = mDbs[pDbName].laps[pCar].best[pNum];
            return { bestlapToLaptime(pDbName, bestlap, pCar),
                     bestlap.tyres,
                     true,
                     pNum };
        } else {
            return { mDbs[pDbName].laps[pCar].byTyre[pTyres][pNum],
                     TyreT::unknownTyres(),
                     true,
                     pNum };
        }
    } else {
        if (pTyres == TyreT::unknownTyres()) {
            const auto &best_nonclean = mDbs[pDbName].laps[pCar].bestNonClean[pNum];
            const auto &nonclean =
                mDbs[pDbName].laps[pCar].byTyreNonClean[best_nonclean.tyres][best_nonclean.num];
            return { bestlapNonCleanToLaptime(pDbName, best_nonclean, pCar),
                     best_nonclean.tyres,
                     nonclean.isClean,
                     pNum };

        } else {
            const auto &nonclean =
                    mDbs[pDbName].laps[pCar].byTyreNonClean[pTyres][pNum];
            return { nonCleanLapToLaptime(pDbName, nonclean, pCar, pTyres),
                     TyreT::unknownTyres(),
                     nonclean.isClean,
                     pNum };
        }
    }
}


int LapsDb::getLaptimesCount (const QByteArray &pDbName,
                              const CarT pCar,
                              const TyreT pTyresRequested,
                              const bool pClean) const {
    QReadLocker locker(mLocks[pDbName].get());

    if (pClean) {
        if (pTyresRequested == TyreT::unknownTyres()) {
            return mDbs[pDbName].laps[pCar].best.size();
        } else {
            return mDbs[pDbName].laps[pCar].byTyre[pTyresRequested].size();
        }
    } else {
        if (pTyresRequested == TyreT::unknownTyres()) {
            return mDbs[pDbName].laps[pCar].bestNonClean.size();
        } else {
            return mDbs[pDbName].laps[pCar].byTyreNonClean[pTyresRequested].size();
        }
    }
}

int LapsDb::getLaptimesCountByQuery (const QByteArray &pDbName,
                                     const LapsdbQuery &pQuery,
                                     const bool pClean) const {
    QReadLocker locker(mLocks[pDbName].get());
    if (pQuery.carsClass != ClassT::notFound()) {
        return getClassLaptimesCount(pDbName, pQuery.carsClass, pQuery.tyres, pClean);
    } else {
        return getLaptimesCount(pDbName, pQuery.car, pQuery.tyres, pClean);
    }
}

LapsDbLaptimeAnswer
LapsDb::findUsernameLaptime (const QByteArray &pDbName,
                             const QByteArray &pUsername,
                             const CarT pCar,
                             const TyreT pTyres,
                             const bool pClean) const {
    QReadLocker locker(mLocks[pDbName].get());
    int stub;
    if (pTyres == TyreT::unknownTyres()) {
        return findUsernameInBestlapsList(pDbName,
                                          pCar,
                                          pUsername,
                                          pClean,
                                          stub);
    } else {
        return findUsernameInLaptimesList(pDbName,
                                          pUsername,
                                          pCar,
                                          pTyres,
                                          pClean,
                                          stub);
    }
}

LapsDbLaptimeAnswer
LapsDb::findUsernameClassLaptime (const QByteArray &pDbName,
                                  const QByteArray &pUsername,
                                  const ClassT pClass,
                                  const TyreT pTyres,
                                  const bool pClean) const {
    QReadLocker locker(mLocks[pDbName].get());
    int stub;
    if (pTyres == TyreT::unknownTyres()) {
        return findUsernameInClassBestlapsList(pDbName,
                                               pClass,
                                               pUsername,
                                               pClean,
                                               stub);
    } else {
        return findUsernameInClassLaptimesList(pDbName,
                                               pUsername,
                                               pClass,
                                               pTyres,
                                               pClean,
                                               stub);
    }
}

int LapsDb::findUserLaptimeNumByQuery (const QByteArray &pDbName,
                                       const QByteArray &pUsername,
                                       const LapsdbQuery &pQuery,
                                       const bool pClean) const {
    try {
        if (pQuery.carsClass != ClassT::notFound()) {
            return findUsernameClassLaptime(pDbName,
                                            pUsername,
                                            pQuery.carsClass,
                                            pQuery.tyres,
                                            pClean)
                    .index;
        } else {
            return findUsernameLaptime(pDbName,
                                       pUsername,
                                       pQuery.car,
                                       pQuery.tyres,
                                       pClean)
                    .index;
        }
    } catch (const LapNotFoundException &) {
    }

    return -1;
}

void LapsDb::wipe (const QByteArray &pDbName) {
    QReadLocker global_lock(&mFileSaveLock);
    QWriteLocker lock (mLocks[pDbName].get());
    mDbs[pDbName] = LapsDbStorage();
    mDbMetadata = LapsDbStorageMetadata();
    mClassesStrings.clear();
    mCars.clear();
}

int LapsDb::findUserLaptimeNum (const QByteArray &pDbName,
                                const QByteArray &pUsername,
                                const CarT pCar,
                                const TyreT pTyresRequested,
                                const bool pClean) const {
    QReadLocker locker(mLocks[pDbName].get());
    if (pTyresRequested == TyreT::unknownTyres()) {
        return findUsernameNumInBestlapsList(pDbName, pCar, pUsername, pClean);
    }

    return findUsernameNumInLaptimesList(pDbName,
                                         pUsername,
                                         pCar,
                                         pTyresRequested,
                                         pClean);
}



bool LapsDb::checkDatabaseValidity () {
    auto check_laptimes =
        [] (const QList<LapsDbStorage::Laptime> &l) -> bool {
        LaptimeT previous_laptime = 0;
        for (auto &lap : l) {
            if (lap.laptime < previous_laptime) {
                return false;
            }
            previous_laptime = lap.laptime;
        }

        return true;
    };

    auto check_nonclean_laptimes =
        [] (const QList<LapsDbStorage::NonCleanLap> &l) -> bool {
        struct Prev { int clean; int nonclean; };
        Prev previous { -1, -1 };
        for (auto &lap : l) {
            int &previous_num = lap.isClean ? previous.clean : previous.nonclean;
            if (lap.num <= previous_num) {
                return false;
            }
            previous_num = lap.num;
        }

        return true;
    };

    auto check_class_laptimes =
        [] (const QList<LapsDbStorage::ClassLaptime> &l) -> bool {
        std::map<CarT, int> previous_nums;
        for (auto &lap : l) {
            if (!previous_nums.count(lap.car)) {
                previous_nums[lap.car] = -1;
            }

            if (lap.num <= previous_nums[lap.car]) {
                return false;
            }
        }

        return true;
    };

    auto check_nonclean_class_laptimes =
        [] (const QList<LapsDbStorage::NonCleanClassLaptime> &l) -> bool {
        std::map<CarT, int> previous_nums;
        for (auto &lap : l) {
            if (!previous_nums.count(lap.car)) {
                previous_nums[lap.car] = -1;
            }

            if (lap.num <= previous_nums[lap.car]) {
                return false;
            }
        }

        return true;
    };

    auto check_bestlaps =
        [] (const QList<LapsDbStorage::Bestlap> &l) -> bool {
        std::map<TyreT, int> previous_nums;
        for (auto &lap : l) {
            if (!previous_nums.count(lap.tyres)) {
                previous_nums[lap.tyres] = -1;
            }

            if (lap.num <= previous_nums[lap.tyres]) {
                return false;
            }
        }

        return true;
    };

    auto check_class_bestlaps =
        [] (const QList<LapsDbStorage::ClassBestlap> &l) -> bool {
        std::map<TyreT, int> previous_nums;
        for (auto &lap : l) {
            if (!previous_nums.count(lap.tyres)) {
                previous_nums[lap.tyres] =-1;
            }

            if (lap.num <= previous_nums[lap.tyres]) {
                return false;
            }
        }

        return true;
    };

    auto check_nonclean_class_bestlaps =
        [] (const QList<LapsDbStorage::NonCleanClassBestlap> &l) -> bool {
        std::map<TyreT, int> previous_nums;
        for (auto &lap : l) {
            if (!previous_nums.count(lap.tyres)) {
                previous_nums[lap.tyres] = -1;
            }

            if (lap.num <= previous_nums[lap.tyres]) {
                return false;
            }
        }

        return true;
    };

    auto seppuku = [this] (const QByteArray &pStr) -> bool {
        mLog(Log::ERROR) << "database validation failed while checking "
                         << pStr;
        return false;
    };

    for (const auto &db_name : mDbs.keys()) {
        const auto &db = mDbs.value(db_name);
        for (const auto &car : db.laps.keys()) {
            const auto &laps = db.laps.value(car);
            for (const auto &tyre_t: laps.byTyre.keys()) {
                const auto &laptimes_list = laps.byTyre.value(tyre_t);
                if (!check_laptimes(laptimes_list)) {
                    return seppuku("laps list");
                }
            }

            for (const auto &tyre_t : laps.byTyreNonCleanStorage.keys()) {
                const auto &laptimes_list = laps.byTyreNonCleanStorage.value(tyre_t);
                if (!check_laptimes(laptimes_list)) {
                    return seppuku("nonclean storage laps list");
                }
            }

            for (const auto &tyre_t : laps.byTyreNonClean.keys()) {
                const auto &nonclean_list = laps.byTyreNonClean.value(tyre_t);
                if (!check_nonclean_laptimes(nonclean_list)) {
                    return seppuku("nonclean laps list");
                }
            }

            if (!check_bestlaps(laps.best)) {
                return seppuku("bestlaps list");
            }

            if (!check_bestlaps(laps.bestNonClean)) {
                return seppuku("nonclean bestlaps list");
            }
        }

        for (const auto &class_ : db.classLaps.keys()) {
            const auto &laps = db.classLaps.value(class_);

            for (const auto &tyre_t: laps.byTyre.keys()) {
                const auto &laptimes_list = laps.byTyre.value(tyre_t);
                if (!check_class_laptimes(laptimes_list)) {
                   return seppuku("class laps list");
                }
            }


            if (!check_class_bestlaps(laps.best)) {
                return seppuku("class bestlaps list");
            }


            for (const auto &tyre_t: laps.byTyreNonClean.keys()) {
                const auto &laptimes_list = laps.byTyreNonClean.value(tyre_t);
                if (!check_nonclean_class_laptimes(laptimes_list)) {
                    return seppuku("nonclean class laps list");
                }
            }

            if (!check_nonclean_class_bestlaps(laps.bestNonClean)) {
                return seppuku("nonclean class bestlaps list");
            }
        }
    }

    return true;
}

LapsDbLaptimeAnswer
LapsDb::getClassLaptime (const QByteArray &pDbName,
                         const int pNum,
                         const ClassT pClass,
                         const TyreT pTyres,
                         const bool pClean) const {
    QReadLocker locker(mLocks[pDbName].get());

    const LapsDbStorage::ClassLaptimes &laps =
            mDbs[pDbName].classLaps[pClass];

    if (pClean) {
        if (pTyres == TyreT::unknownTyres()) {
            const auto &bestlap = laps.best[pNum];
            const auto &lap = laps.byTyre[bestlap.tyres][bestlap.num];

            return { classLaptimeToLaptime(pDbName, lap, bestlap.tyres),
                     bestlap.tyres,
                     true,
                     lap.car,
                     pNum };
        } else {
            const auto &lap = laps.byTyre[pTyres][pNum];

            return { classLaptimeToLaptime(pDbName, lap, pTyres),
                     TyreT::unknownTyres(),
                     true,
                     lap.car,
                     pNum };
        }
    } else {
        if (pTyres == TyreT::unknownTyres()) {
            const auto &class_bestlap = laps.bestNonClean[pNum];
            //class_bestlap.f
            const auto &class_lap =
                laps.byTyreNonClean[class_bestlap.tyres][class_bestlap.num];
            const auto &nonclean_lap =
                mDbs[pDbName].laps[class_lap.car].byTyreNonClean[class_bestlap.tyres][class_lap.num];

//            class_lap;

            return { classLaptimeToLaptime(pDbName, class_lap, class_bestlap.tyres),
                     class_bestlap.tyres,
                     nonclean_lap.isClean,
                     class_lap.car,
                     pNum };

        } else {
            const auto &class_lap =
                laps.byTyreNonClean[pTyres][pNum];
            const auto &nonclean_lap =
                mDbs[pDbName].laps[class_lap.car].byTyreNonClean[pTyres][class_lap.num];

            return { nonCleanLapToLaptime(pDbName, nonclean_lap, class_lap.car, pTyres),
                     TyreT::unknownTyres(),
                     nonclean_lap.isClean,
                     class_lap.car,
                     pNum };
        }
    }
}


int LapsDb::getClassLaptimesCount (const QByteArray &pDbName,
                                   const ClassT pClass,
                                   const TyreT pTyresRequested,
                                   const bool pClean) const {
    QReadLocker locker(mLocks[pDbName].get());

    if (pClean) {
        if (pTyresRequested == TyreT::unknownTyres()) {
            return mDbs[pDbName].classLaps[pClass].best.size();
        } else {
            return mDbs[pDbName].classLaps[pClass].byTyre[pTyresRequested].size();
        }
    } else {
        if (pTyresRequested == TyreT::unknownTyres()) {
            return mDbs[pDbName].classLaps[pClass].bestNonClean.size();
        } else {
            return mDbs[pDbName].classLaps[pClass].byTyreNonClean[pTyresRequested].size();
        }
    }
}


const LapsDbStorage::Laptime &
LapsDb::classLaptimeToLaptime (const QByteArray &pDbName,
                               const LapsDbStorage::ClassLaptime& pClassLaptime,
                               const TyreT pTyres) const {
    return mDbs[pDbName].laps[pClassLaptime.car].byTyre[pTyres][pClassLaptime.num];
}

const LapsDbStorage::Laptime &
LapsDb::classLaptimeToLaptime (const QByteArray &pDbName,
                                       const LapsDbStorage::NonCleanClassLaptime &f1,
                                       const TyreT pTyres) const{
    const LapsDbStorage::NonCleanLap &ncl =
        mDbs[pDbName].laps[f1.car].byTyreNonClean[pTyres][f1.num];
    return nonCleanLapToLaptime(pDbName, ncl, f1.car, pTyres);
}

const LapsDbStorage::Laptime &
LapsDb::bestlapToLaptime (const QByteArray &pDbName,
                          const LapsDbStorage::Bestlap &pBestlap,
                          const CarT pCar) const {
    return mDbs[pDbName].laps[pCar].byTyre[pBestlap.tyres][pBestlap.num];
}

const LapsDbStorage::Laptime &
LapsDb::bestlapNonCleanToLaptime (const QByteArray &pDbName,
                                  const LapsDbStorage::Bestlap &pLap,
                                  const CarT pCar) const {
    return nonCleanLapToLaptime(
        pDbName,
        mDbs[pDbName].laps[pCar].byTyreNonClean[pLap.tyres][pLap.num],
        pCar,
        pLap.tyres);
}

const LapsDbStorage::Laptime &
LapsDb::classBestlapToLaptime (const QByteArray &pDbName,
                               const LapsDbStorage::ClassBestlap &pClassBestlap,
                               const ClassT pClass) const{
    const LapsDbStorage::ClassLaptime &cl =
        mDbs[pDbName].classLaps[pClass]
        .byTyre[pClassBestlap.tyres][pClassBestlap.num];
    return classLaptimeToLaptime(pDbName, cl, pClassBestlap.tyres);
}

const LapsDbStorage::Laptime &
LapsDb::classBestlapToLaptime (const QByteArray &pDbName,
                               const LapsDbStorage::NonCleanClassBestlap &pClassBestlap,
                               const ClassT pClass) const {
    const LapsDbStorage::NonCleanClassLaptime &cl =
        mDbs[pDbName].classLaps[pClass]
        .byTyreNonClean[pClassBestlap.tyres][pClassBestlap.num];
    return classLaptimeToLaptime(pDbName, cl, pClassBestlap.tyres);
}

const LapsDbStorage::Laptime &
LapsDb::nonCleanLapToLaptime (const QByteArray &pDbName,
                              const LapsDbStorage::NonCleanLap &pLap,
                              const CarT pCar,
                              const TyreT pTyres) const {
    if (pLap.isClean) {
        return mDbs[pDbName].laps[pCar].byTyre[pTyres][pLap.num];
    } else {
        return mDbs[pDbName].laps[pCar].byTyreNonCleanStorage[pTyres][pLap.num];
    }
}


LapsDbLaptimeAnswer
LapsDb::getLaptimeByQuery (const QByteArray &pDbName,
                           const int pNum,
                           const LapsdbQuery &pQuery,
                           const bool pClean = true) const {
    if (pQuery.carsClass != ClassT::notFound()) {
        return getClassLaptime(pDbName,
                               pNum,
                               pQuery.carsClass,
                               pQuery.tyres,
                               pClean);
    } else {
        return getLaptime(pDbName, pNum, pQuery.car, pQuery.tyres, pClean);
    }
}
