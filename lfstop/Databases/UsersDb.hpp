// Copyright 2015 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of LFSTop.
//
// LFSTop is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LFSTop is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LFSTop.  If not, see <http://www.gnu.org/licenses/>.


#ifndef LFSTOP_USERS_DB_HPP_INCLUDED
#define LFSTOP_USERS_DB_HPP_INCLUDED


#include <QReadWriteLock>


#include "UsersDbStorage.hpp"
#include "Logging/LogFile.hpp"
#include "Logging/Log.hpp"


class UsersDb {
public:
    UsersDb (Log &pLog);
    ~UsersDb ();

    void userSetNickname (const QByteArray &pUser,
                          const QByteArray &pNickname);

    QByteArray userGetNickname (const QByteArray &user) const;

    QByteArray userGetFullName (const QByteArray &pUsername) const;

    const QByteArray findUsernameByString (const QByteArray &pString) const;

    void userSetSettings (const QByteArray &pUser,
                          const UsersDbStorage::UserSettings &pUserSettings);
    UsersDbStorage::UserSettings userGetSettings (const QByteArray &pUser);

private:
    bool userResetCase (const QByteArray &pUser);

    Log &mLog;

    UsersDbStorage mDb;
    mutable QReadWriteLock mLock;
    static constexpr qint32 mDbFileFormatVersion = 2;
    bool mNeedToSave = false;
};

#endif // LFSTOP_USERS_DB_HPP_INCLUDED
