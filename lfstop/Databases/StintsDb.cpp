#include "StintsDb.hpp"


StintsDb::StintsDb (QByteArray pName, Log &pLog)
    : mFileName (pName),
      mLog (pLog) {
    QFile file(pName);
    const QString abs_path = QFileInfo(pName).absoluteFilePath();

    if (file.open(QIODevice::ReadOnly)) {
        QDataStream in(&file);
        QByteArray file_magic_string;
        qint32 actual_file_format_version;

        in >> file_magic_string;
        in >> actual_file_format_version;

        if (file_magic_string != "LfsTopStintsDb"
            || actual_file_format_version != mFileFormatVersion) {
            mLog(Log::WARNING) << "wrong file format: " << abs_path;
            mLog(Log::WARNING) << "moving file to: " << abs_path << ".invalid";
            if (!file.rename(pName + ".invalid")) {
                mLog(Log::ERROR) << "failed to rename file: " << mFileName;
                throw std::runtime_error("failed to rename file");
            }
        }

        in >> stintsList;
    }

}

StintsDb::~StintsDb() {

    if (!QDir("stints").exists()) {
        QDir().mkdir("stints");
    }

    QFile file(mFileName);
    const QString abs_path = QFileInfo(mFileName).absoluteFilePath();
    if (file.open(QIODevice::WriteOnly)) {
        QDataStream out(&file);
        out << QByteArray("LfsTopStintsDb") << mFileFormatVersion
            << stintsList;
        mLog() << "stints database saved: " << abs_path;
    } else {
        mLog(Log::ERROR) << "can not write file: " << abs_path;
    }
}

QDataStream &operator<<(QDataStream &out, const StintInfo::Lap &lap) {
    out << lap.sections
        << lap.laptime
        << quint8(lap.exclude);
    return out;
}

QDataStream &operator>>(QDataStream &in, StintInfo::Lap &lap) {
    in >> lap.sections
       >> lap.laptime
       >> reinterpret_cast<quint8&>(lap.exclude);
    return in;
}

QDataStream &operator<<(QDataStream &out, const StintInfo &inf) {
    out << inf.laps
        << inf.sc
        << inf.trackName
        << quint32(inf.cleanBest)
        << quint32(inf.cleanLapsNum)
        << quint32(inf.noncleanBest)
        << quint32(inf.noncleanLapsNum);
    return out;
}

QDataStream &operator>>(QDataStream &in, StintInfo &inf) {
    in >> inf.laps
            >> inf.sc
            >> inf.trackName
            >> reinterpret_cast<quint32&>(inf.cleanBest)
            >> reinterpret_cast<quint32&>(inf.cleanLapsNum)
            >> reinterpret_cast<quint32&>(inf.noncleanBest)
            >> reinterpret_cast<quint32&>(inf.noncleanLapsNum);
    return in;
}

QDataStream &operator<<(QDataStream &out, const StintInfoListRow &row) {
    out << row.username
        << row.car
        << row.tyres
        << row.stintInfo
        << row.timeStamp;
    return out;
}

QDataStream &operator>>(QDataStream &in, StintInfoListRow &row) {
    in >> row.username
            >> row.car
            >> row.tyres
            >> row.stintInfo
            >> row.timeStamp;
    return in;
}
