// Copyright 2015 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of LFSTop.
//
// LFSTop is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LFSTop is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LFSTop.  If not, see <http://www.gnu.org/licenses/>.


#include "LapsDbStorage.hpp"


#include <limits>

const std::vector<QByteArray> CarT::mLfsCarsStrings =
    { "UF1", "XFG", "XRG", "LX4", "LX6",
      "RB4", "FXO", "XRT", "RAC", "FZ5",
      "UFR", "XFR", "FXR", "XRR", "FZR",
      "MRT", "FBM", "FOX", "FO8", "BF1" };


const QMap<QByteArray, QSet<CarT>> LapsdbTypeInfo::lfs_cars_aliases =
    { { "ALL", { CarT::UF1(), CarT::XFG(), CarT::XRG(), CarT::LX4(), CarT::LX6(),
                 CarT::RB4(), CarT::FXO(), CarT::XRT(), CarT::RAC(), CarT::FZ5(),
                 CarT::UFR(), CarT::XFR(), CarT::FXR(), CarT::XRR(), CarT::FZR(),
                 CarT::MRT(), CarT::FBM(), CarT::FOX(), CarT::FO8(), CarT::BF1() } },
      { "ROAD", { CarT::UF1(), CarT::XFG(), CarT::XRG(), CarT::LX4(), CarT::LX6(),
                  CarT::RB4(), CarT::FXO(), CarT::XRT(), CarT::RAC(), CarT::FZ5() } },
      { "RACE", { CarT::UFR(), CarT::XFR(), CarT::FXR(), CarT::XRR(), CarT::FZR(),
                  CarT::FBM(), CarT::FOX(), CarT::FO8(), CarT::BF1() } },
      { "TBO", { CarT::RB4(), CarT::FXO(), CarT::XRT() } },
      { "LRF", { CarT::LX6(), CarT::RAC(), CarT::FZ5() } },
      { "GTR", { CarT::FXR(), CarT::XRR(), CarT::FZR() } } };



LapsDbStorage::Laptime
LapsDbStorage::Laptime::createBySections (const std::vector<LaptimeT> &pSections,
                                          const QByteArray &pUsername) {
    Laptime lt;
    lt.laptime = 0;
    for (size_t i = 0; i < (pSections.size() - 1); ++i) {
        lt.splits.append(pSections[i] + lt.laptime);
        lt.laptime += pSections[i];
    }
    lt.laptime += pSections.back();
    lt.username = pUsername;
    return lt;
}

bool operator== (const CarDefinition &c1, const CarDefinition &c2) {
    return (c1.lfsCar == c2.lfsCar
            && c1.intakeRestriction == c2.intakeRestriction
            && c1.addedMass == c2.addedMass);
}


QDataStream &operator<< (QDataStream &out, const LapsDbStorage::Laptime &lt) {
    out << lt.splits << lt.laptime << lt.username << lt.timeStamp;
    return out;
}

QDataStream &operator>> (QDataStream &in, LapsDbStorage::Laptime &lt) {
    in >> lt.splits >> lt.laptime >> lt.username >> lt.timeStamp;
    return in;
}



QDataStream &operator<< (QDataStream &out, const LapsDbStorage::CarLaptimes &clt) {
    out << clt.byTyre
        << clt.best
        << clt.byTyreNonClean
        << clt.byTyreNonCleanStorage
        << clt.bestNonClean;
    return out;
}

QDataStream &operator<< (QDataStream &out, const LapsDbStorage::ClassLaptimes &clt) {
    out << clt.byTyre
        << clt.best
        << clt.byTyreNonClean
        << clt.bestNonClean;
    return out;
}



QDataStream &operator<< (QDataStream &out, const CarDefinition &cd) {
    out << cd.lfsCar << cd.intakeRestriction << cd.addedMass;
    return out;
}

QDataStream &operator>> (QDataStream &in, CarDefinition &cd) {
    in >> cd.lfsCar >> cd.intakeRestriction >> cd.addedMass;
    return in;
}



QDataStream &operator<< (QDataStream &out, const LapsDbStorage &db) {
    out << db.laps
        << db.classLaps;

    return out;
}



QDataStream &operator<< (QDataStream &out, const LapsDbStorageMetadata &db) {
    out << db.classes
        << db.classNames
        << db.customCars
        << db.customCarNames;
    return out;
}

QDataStream &operator>> (QDataStream &in, LapsDbStorageMetadata &db) {
    in >> db.classes
       >> db.classNames
       >> db.customCars
       >> db.customCarNames;
    return in;
}



void load_db_v1_laps (QDataStream &pIn,
                      QMap<CarT, LapsDbStorage::CarLaptimes> &pCarLaps) {
    QDataStream::Status oldStatus = pIn.status();
    pIn.resetStatus();

    pCarLaps.clear();

    quint32 n;
    pIn >> n;

    pCarLaps.detach();

    for (quint32 i = 0; i < n; ++i) {
        if (pIn.status() != QDataStream::Ok) {
            break;
        }

        CarT car_id;
        LapsDbStorage::CarLaptimes car_laptimes;

        pIn >> car_id
            >> car_laptimes.byTyre
            >> car_laptimes.best;

        for (auto tyres : car_laptimes.byTyre.keys()) {
            QList<LapsDbStorage::NonCleanLap> non_clean;
            for (int ii = 0; ii < car_laptimes.byTyre[tyres].size(); ++ii) {
                non_clean.push_back(LapsDbStorage::NonCleanLap { true, ii });
            }
            car_laptimes.byTyreNonClean.insertMulti(tyres, non_clean);
        }

        car_laptimes.bestNonClean = car_laptimes.best;

        pCarLaps.insertMulti(car_id, car_laptimes);
    }

    if (pIn.status() != QDataStream::Ok) {
        pCarLaps.clear();
    }
    if (oldStatus != QDataStream::Ok) {
        pIn.setStatus(oldStatus);
    }
}
void load_db_v1_class_laps (QDataStream &pIn,
                            QMap<ClassT, LapsDbStorage::ClassLaptimes> &pClassLaps) {
    QDataStream::Status oldStatus = pIn.status();
    pIn.resetStatus();

    pClassLaps.clear();

    quint32 n;
    pIn >> n;

    pClassLaps.detach();

    for (quint32 i = 0; i < n; ++i) {
        if (pIn.status() != QDataStream::Ok) {
            break;
        }

        ClassT class_id;
        LapsDbStorage::ClassLaptimes class_laptimes;

        pIn >> class_id
            >> class_laptimes.byTyre
            >> class_laptimes.best;

        for (const auto &tyre : class_laptimes.byTyre.keys()) {
            for (const auto &cl: class_laptimes.byTyre[tyre]) {
                class_laptimes.byTyreNonClean[tyre].append({ cl.car, cl.num });
            }
        }

        for (const auto &lap : class_laptimes.best) {
            class_laptimes.bestNonClean.append ({ lap.tyres, lap.num });
        }

        pClassLaps.insertMulti(class_id, class_laptimes);
    }

    if (pIn.status() != QDataStream::Ok) {
        pClassLaps.clear();
    }
    if (oldStatus != QDataStream::Ok) {
        pIn.setStatus(oldStatus);
    }
}

void LapsDbStorage::load_db_v1 (QDataStream &pIn,
                                QMap<QByteArray, LapsDbStorage> &pDb) {
    QDataStream::Status oldStatus = pIn.status();
    pIn.resetStatus();

    pDb.clear();

    quint32 n;
    pIn >> n;

    pDb.detach();

    for (quint32 i = 0; i < n; ++i) {
        if (pIn.status() != QDataStream::Ok) {
            break;
        }

        QByteArray db_name;
        LapsDbStorage db;


        pIn >> db_name;
        load_db_v1_laps(pIn, db.laps);
        load_db_v1_class_laps(pIn, db.classLaps);

        pDb.insertMulti(db_name, db);
    }

    if (pIn.status() != QDataStream::Ok) {
        pDb.clear();
    }
    if (oldStatus != QDataStream::Ok) {
        pIn.setStatus(oldStatus);
    }
}



void load_db_v2_laps (QDataStream &pIn,
                      QMap<CarT, LapsDbStorage::CarLaptimes> &pCarLaps) {
    QDataStream::Status oldStatus = pIn.status();
    pIn.resetStatus();

    pCarLaps.clear();

    quint32 n;
    pIn >> n;

    pCarLaps.detach();

    for (quint32 i = 0; i < n; ++i) {
        if (pIn.status() != QDataStream::Ok) {
            break;
        }

        CarT car_id;
        LapsDbStorage::CarLaptimes car_laptimes;

        pIn >> car_id
            >> car_laptimes.byTyre
            >> car_laptimes.best
            >> car_laptimes.byTyreNonClean
            >> car_laptimes.byTyreNonCleanStorage
            >> car_laptimes.bestNonClean;

        pCarLaps.insertMulti(car_id, car_laptimes);
    }

    if (pIn.status() != QDataStream::Ok) {
        pCarLaps.clear();
    }
    if (oldStatus != QDataStream::Ok) {
        pIn.setStatus(oldStatus);
    }
}
void load_db_v2_class_laps (QDataStream &pIn,
                            QMap<ClassT, LapsDbStorage::ClassLaptimes> &pClassLaps) {
    QDataStream::Status oldStatus = pIn.status();
    pIn.resetStatus();

    pClassLaps.clear();

    quint32 n;
    pIn >> n;

    pClassLaps.detach();

    for (quint32 i = 0; i < n; ++i) {
        if (pIn.status() != QDataStream::Ok) {
            break;
        }

        ClassT class_id;
        LapsDbStorage::ClassLaptimes class_laptimes;

        pIn >> class_id
            >> class_laptimes.byTyre
            >> class_laptimes.best
            >> class_laptimes.byTyreNonClean
            >> class_laptimes.bestNonClean;

        pClassLaps.insertMulti(class_id, class_laptimes);
    }

    if (pIn.status() != QDataStream::Ok) {
        pClassLaps.clear();
    }
    if (oldStatus != QDataStream::Ok) {
        pIn.setStatus(oldStatus);
    }
}

void LapsDbStorage::load_db_v2 (QDataStream &pIn,
                                QMap<QByteArray, LapsDbStorage> &pDb) {
    QDataStream::Status oldStatus = pIn.status();
    pIn.resetStatus();

    pDb.clear();

    quint32 n;
    pIn >> n;

    pDb.detach();

    for (quint32 i = 0; i < n; ++i) {
        if (pIn.status() != QDataStream::Ok) {
            break;
        }

        QByteArray db_name;
        LapsDbStorage db;


        pIn >> db_name;
        load_db_v2_laps(pIn, db.laps);
        load_db_v2_class_laps(pIn, db.classLaps);

        pDb.insertMulti(db_name, db);
    }

    if (pIn.status() != QDataStream::Ok) {
        pDb.clear();
    }
    if (oldStatus != QDataStream::Ok) {
        pIn.setStatus(oldStatus);
    }
}

QDataStream &operator<< (QDataStream &out, const LapsDbStorage::Bestlap &lt) {
    out << lt.tyres << lt.num;
    return out;
}

QDataStream &operator>> (QDataStream &in, LapsDbStorage::Bestlap &lt) {
    in >> lt.tyres >> lt.num;
    return in;
}

QDataStream &operator<< (QDataStream &out, const LapsDbStorage::NonCleanLap &lt) {
    out << lt.isClean << lt.num;
    return out;
}

QDataStream &operator>> (QDataStream &in, LapsDbStorage::NonCleanLap &lt) {
    in >> lt.isClean >> lt.num;
    return in;
}

QDataStream &operator<< (QDataStream &out, const LapsDbStorage::ClassLaptime &lt) {
    out << lt.car << lt.num;
    return out;
}

QDataStream &operator>> (QDataStream &in, LapsDbStorage::ClassLaptime &lt) {
    in >> lt.car >> lt.num;
    return in;
}

QDataStream &operator<< (QDataStream &out, const LapsDbStorage::ClassBestlap &lt) {
    out << lt.tyres << lt.num;
    return out;
}

QDataStream &operator>> (QDataStream &in, LapsDbStorage::ClassBestlap &lt) {
    in >> lt.tyres >> lt.num;
    return in;
}

QDataStream &operator<< (QDataStream &out, const LapsDbStorage::NonCleanClassLaptime &clt) {
    out << clt.car << clt.num;
    return out;
}

QDataStream &operator>> (QDataStream &in, LapsDbStorage::NonCleanClassLaptime &clt) {
    in >> clt.car >> clt.num;
    return in;
}

QDataStream &operator<< (QDataStream &out, const LapsDbStorage::NonCleanClassBestlap &clt) {
    out << clt.tyres << clt.num;
    return out;
}

QDataStream &operator>> (QDataStream &in, LapsDbStorage::NonCleanClassBestlap &clt) {
    in >> clt.tyres >> clt.num;
    return in;
}
