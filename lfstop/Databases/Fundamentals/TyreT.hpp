// Copyright 2015 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of LFSTop.
//
// LFSTop is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LFSTop is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LFSTop.  If not, see <http://www.gnu.org/licenses/>.


#ifndef LFSTOP_FUNDAMENTALS_TYRE_T_HPP_INCLUDED
#define LFSTOP_FUNDAMENTALS_TYRE_T_HPP_INCLUDED


#include <cstdint>
#include <vector>


#include <QDataStream>
#include <QHash>


class TyreT final  {
public:
    constexpr TyreT ()
        : tyre1 (255),
          tyre2 (255),
          tyre3 (255),
          tyre4 (255) {
    }

    inline constexpr TyreT (const uint8_t pTyre1,
                            const uint8_t pTyre2,
                            const uint8_t pTyre3,
                            const uint8_t pTyre4)
        : tyre1 (pTyre1),
          tyre2 (pTyre2),
          tyre3 (pTyre3),
          tyre4 (pTyre4) {
    }

    inline static TyreT fromArr (const uint8_t pTyres[]) {
        return TyreT(pTyres[3], pTyres[2], pTyres[1], pTyres[0]);
    }

    inline static constexpr TyreT fromOne (const uint8_t pTyre) {
        return TyreT(pTyre, pTyre, pTyre, pTyre);
    }

    inline static constexpr TyreT unknownTyres () {
        return TyreT();
    }

    inline static constexpr TyreT wrTyres () {
        return fromOne(254);
    }

    inline static constexpr TyreT airioTyres () {
        return fromOne(253);
    }

    inline uint32_t getAsUint32 () const {
        union tyre_union {
            uint8_t arr[4];
            uint32_t whole;
        };

        tyre_union un;
        un.arr[0] = tyre1;
        un.arr[1] = tyre2;
        un.arr[2] = tyre3;
        un.arr[3] = tyre4;
        return un.whole;
    }

    static QByteArray printTyre (const uint8_t pTyre) {
        auto iter = std::lower_bound(tyreNames.data.cbegin(),
                                     tyreNames.data.cend(),
                                     std::pair<uint8_t, QByteArray> { pTyre, ""},
                                     [&pTyre] (const std::pair<uint8_t, QByteArray> &p1,
                                               const std::pair<uint8_t, QByteArray> &p2) {
                                         return p1.first < p2.first;
                                     });
        if (iter != tyreNames.data.cend() && iter->first == pTyre) {
            return iter->second;
        }
        return ".";
    }

    static uint8_t getTyreByName (const QByteArray &pName) {
        auto iter = std::find_if(tyreNames.data.cbegin(),
                                 tyreNames.data.cend(),
                                 [&pName] (const std::pair<uint8_t, QByteArray> &p) {
                                      return p.second == pName;
                                 });
        if (iter != tyreNames.data.cend()) {
            return iter->first;
        }

        return 255;
    }

    QByteArray toString () const {
        if (tyre1 == tyre2
            && tyre3 == tyre4) {
            if (tyre1 == tyre3) {
                return printTyre(tyre1);
            }
            else {
                return printTyre(tyre1)
                        + '-' + printTyre(tyre3);
            }
        }
        else {
            return printTyre(tyre1)
                    + '-' + printTyre(tyre2)
                    + '-' + printTyre(tyre3)
                    + '-' + printTyre(tyre4);
        }
    }

    bool operator< (const TyreT pOther) const {
        // required to put TyreT into QMap
        return getAsUint32() < pOther.getAsUint32();
    }

    bool operator== (const TyreT pOther) const {
        return tyre1 == pOther.tyre1
                && tyre2 == pOther.tyre2
                && tyre3 == pOther.tyre3
                && tyre4 == pOther.tyre4;
    }

    bool operator!= (const TyreT pOther) const {
        return tyre1 != pOther.tyre1
                || tyre2 != pOther.tyre2
                || tyre3 != pOther.tyre3
                || tyre4 != pOther.tyre4;
    }

    uint8_t tyre1;
    uint8_t tyre2;
    uint8_t tyre3;
    uint8_t tyre4;
private:
    struct TyreNames {
        TyreNames ()
            : data (std::vector<std::pair<uint8_t, QByteArray>>{
                  { 0, "R1" },
                  { 1, "R2" },
                  { 2, "R3" },
                  { 3, "R4" },
                  { 4, "RS" },
                  { 5, "RN" },
                  { 6, "HB" },
                  { 7, "KN" },
                  { 253, "Airio"},
                  { 254, " "} }) {
        }

        const std::vector<std::pair<uint8_t, QByteArray>> data;
    };

    static const TyreNames tyreNames;
};

inline uint qHash (const TyreT &pTyre, uint seed) {
    return qHash(pTyre.getAsUint32(), seed);
}

inline QDataStream &operator<< (QDataStream &out, const TyreT &c) {
    // at the beginning we used uint32_t internally,
    // therefore we need to read and write in the opposite way
    out << c.tyre4 << c.tyre3 << c.tyre2 << c.tyre1;
    return out;
}

inline QDataStream &operator>> (QDataStream &in, TyreT &c) {
    in >> c.tyre4 >> c.tyre3 >> c.tyre2 >> c.tyre1;

    if ((c.tyre1 != c.tyre2)
        || (c.tyre3 != c.tyre4)) {
        return in;
    }

    return in;
}


#endif // LFSTOP_FUNDAMENTALS_TYRE_T_HPP_INCLUDED
