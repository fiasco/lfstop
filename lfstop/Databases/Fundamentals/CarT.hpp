// Copyright 2015 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of LFSTop.
//
// LFSTop is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LFSTop is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LFSTop.  If not, see <http://www.gnu.org/licenses/>.


#ifndef LFSTOP_FUNDAMENTALS_CAR_T_HPP_INCLUDED
#define LFSTOP_FUNDAMENTALS_CAR_T_HPP_INCLUDED

#include <cassert>
#include <cstdint>
#include <vector>


#include <QDataStream>
#include <QHash>


#include "Databases/Fundamentals/TypeSafetyWrapper.hpp"
#include "Utils/StdVectorUtils.hpp"


class CarT final : public TypeSafetyWrapper<uint16_t, CarT> {
public:
    using base = TypeSafetyWrapper<uint16_t, CarT>;
    using base::base;

    inline constexpr CarT ()
        : base (unknownCar()) {
    }

    inline static constexpr CarT unknownCar () {
        return CarT(std::numeric_limits<CarT::InnerType>::max());
    }

    inline static constexpr CarT UF1 () { return CarT(0); }
    inline static constexpr CarT XFG () { return CarT(1); }
    inline static constexpr CarT XRG () { return CarT(2); }
    inline static constexpr CarT LX4 () { return CarT(3); }
    inline static constexpr CarT LX6 () { return CarT(4); }

    inline static constexpr CarT RB4 () { return CarT(5); }
    inline static constexpr CarT FXO () { return CarT(6); }
    inline static constexpr CarT XRT () { return CarT(7); }
    inline static constexpr CarT RAC () { return CarT(8); }
    inline static constexpr CarT FZ5 () { return CarT(9); }

    inline static constexpr CarT UFR () { return CarT(10); }
    inline static constexpr CarT XFR () { return CarT(11); }
    inline static constexpr CarT FXR () { return CarT(12); }
    inline static constexpr CarT XRR () { return CarT(13); }
    inline static constexpr CarT FZR () { return CarT(14); }

    inline static constexpr CarT MRT () { return CarT(15); }
    inline static constexpr CarT FBM () { return CarT(16); }
    inline static constexpr CarT FOX () { return CarT(17); }
    inline static constexpr CarT FO8 () { return CarT(18); }
    inline static constexpr CarT BF1 () { return CarT(19); }

    inline static CarT lfsCarFromString (const QByteArray &pStr) {
        if (vector_contains(mLfsCarsStrings, pStr)) {
            return CarT(vector_find(mLfsCarsStrings, pStr));
        } else {
            return unknownCar();
        }
    }

    bool isStockLfsCar () const {
        return value < mLfsCarsStrings.size();
    }

    const QByteArray lfsCarToString () const {
        assert(base::value < customCarsOffset);
        return mLfsCarsStrings[value];
    }

//    static constexpr int numberOfLfsCars = 20;
    static constexpr CarT::InnerType customCarsOffset = CarT::InnerType (1000);
private:
    static const std::vector<QByteArray> mLfsCarsStrings; // overhead? patches welcome
};

inline uint qHash (const CarT &pCar, uint seed) {
    return qHash(pCar.value, seed);
}

inline QDataStream &operator<< (QDataStream &out, const CarT &c) {
    out << c.value;
    return out;
}

inline QDataStream &operator>> (QDataStream &in, CarT &c) {
    in >> c.value;
    return in;
}


#endif // LFSTOP_FUNDAMENTALS_CAR_T_HPP_INCLUDED
