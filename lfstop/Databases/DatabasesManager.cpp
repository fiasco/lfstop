// Copyright 2015 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of LFSTop.
//
// LFSTop is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LFSTop is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LFSTop.  If not, see <http://www.gnu.org/licenses/>.


#include "DatabasesManager.hpp"


#include <QDir>
#include <QMutexLocker>


#include "Utils/MKeyFile.hpp"
#include "Utils/StringUtils.hpp"

#include "CompilerUtils.hpp"

#include "ip2location/IP2Location.h"



bool car_is_zero (const CarDefinition &car)
    LFSTOP_FUNCTION_ATTRIBUTE_PURE;

bool clarify_car_setting_engine (CarDefinition &rCarDef,
                                 const QByteArray &pParameter) {
    quint8 p = pParameter.toUInt();
    if (p <= 50) {
        rCarDef.intakeRestriction = p;
        return true;
    }
    return false;
}

bool clarify_car_setting_mass (CarDefinition &rCarDef,
                               const QByteArray &pParameter) {
    quint8 p = pParameter.toUInt();
    if (p <= 190) {
        rCarDef.addedMass = p;
        return true;
    }
    return false;
}

bool clarify_car_setting_parameter (CarDefinition &rCarDef,
                                    const QByteArray &pParameter,
                                    quint8 position) {
    switch (position) {
    case 0:
        return clarify_car_setting_engine(rCarDef, pParameter);
        break;
    case 1:
        return clarify_car_setting_mass(rCarDef, pParameter);
        break;
    }
    return false;
}

bool clarify_car_setting (Log &log,
                          CarDefinition &rCarDef,
                          const std::vector<QByteArray> &pCarParams) {
    if (pCarParams.size() != 0) {
        const CarT car = CarT::lfsCarFromString(pCarParams[0]);
        if (car.isStockLfsCar()) {
            rCarDef.lfsCar = car;
        }
        else {
            log(Log::WARNING) << "no such car in LFS: "
                              << pCarParams[0]
                              << ", ignoring the car completely";
            return false;
        }
        if (pCarParams.size() > 1) {
            if (!clarify_car_setting_parameter(rCarDef,
                                               pCarParams[1],
                                               0)) {
                log(Log::WARNING) << "parsing car parameter failed: "
                                  << pCarParams[1]
                                  << ", ignoring the car completely";
                return false;
            }

            if (pCarParams.size() > 2) {
                if (!clarify_car_setting_parameter(rCarDef,
                                                   pCarParams[2],
                                                   1)) {
                    log(Log::WARNING) << "parsing car parameter failed: "
                                      << pCarParams[2]
                                      << ", ignoring the car completely";
                    return false;
                }
                if (pCarParams.size() > 3) {
                    log(Log::WARNING) << "extra car parameters ignored: "
                                      << pCarParams[3]
                                      << " ...";
                }
            }
        }
    }
    else {
        return false;
    }
    return true;
}

bool car_is_zero (const CarDefinition &car) {
    return car.addedMass == 0 && car.intakeRestriction == 0;
}

LapsDb * LfsDatabasesManager::loadDatabase (const std::string &pTrackName) {
    LapsDb *db = new LapsDb(mLog);

    QMapIterator<QByteArray, CarDefinition> car_it(mCustomCars);
    while (car_it.hasNext()) {
        car_it.next();
        db->addCar(car_it.key(), car_it.value());
    }

    QMapIterator<QByteArray, QSet<QByteArray>> class_it(mClasses);
    while (class_it.hasNext()) {
        class_it.next();
        db->addClass(class_it.key(), class_it.value());
    }

    db->loadFile(QString::fromUtf8(("databases/" + pTrackName).c_str()));

    return db;
}

LfsDatabasesManager::LfsDatabasesManager (Log &pLog,
                                          const MKeyFile &pConfigFile,
                                          const Settings::SettingsMap &pSettings,
                                          const bool pDisableWr)

    : mConfigFile (pConfigFile),
      mSettingsMap (pSettings),
      mLog(pLog),
      mWrDb (mLog,
             Settings::getValue<std::string>(mSettingsMap,
                                             "settings.pubstat_key",
                                             "")),
      mDatabasesSaveTimer (this) {

    if (!pDisableWr) {
        QObject::connect(&mWrDb, SIGNAL(updated()),
                         this, SLOT(fillWrs()));

        mWrDb.setUpdateIntervalMins(
                    Settings::getValue(mSettingsMap,
                                       "settings.wr_update_interval",
                                       60));
        mWrDb.init();
    }

    if (!QDir("databases").exists()) {
        QDir().mkdir("databases");
    }

    loadCarsAndClasses();

    connect(&mDatabasesSaveTimer, SIGNAL(timeout()),
            this, SLOT(saveDatabases()));

    mDatabasesSaveTimer.setInterval(mSaveIntervalMins * 1000 * 60);
    mDatabasesSaveTimer.start();


    mIp2Location = IP2Location_open("data/IP2LOCATION-LITE-DB1.BIN");
}

LfsDatabasesManager::~LfsDatabasesManager () {
    IP2Location_close(mIp2Location);
}

void LfsDatabasesManager::loadCarsAndClasses () {
    const MKeyFile &f = mConfigFile;
    QList<QByteArray> custom_car_names;
    if (f.sectionCount("cars") > 0) {
        MSettingsIterator set_iter = f.getLastSectionSettings("cars");
        while (set_iter.isElement()) {
            const std::vector<QByteArray> vec =
                    StringUtils::stringToQByteArrayVector(set_iter.getValue());
            CarDefinition car = CarDefinition();

            if (!clarify_car_setting(mLog, car, vec) || car_is_zero(car)) {
                mLog(Log::WARNING) << "custom car ignored: "
                                   << set_iter.getName()
                                   << " " << set_iter.getValue();
            }
            else {
                mCustomCars[set_iter.getName().c_str()] = car;
                custom_car_names.append(set_iter.getName().c_str());
            }

            set_iter.peekNext();
        }
    }
    if (f.sectionCount("classes") > 0) {
        MSettingsIterator set_iter = f.getLastSectionSettings("classes");
        while (set_iter.isElement()) {
            const std::vector<QByteArray> vec =
                    StringUtils::stringToQByteArrayVector(set_iter.getValue());
            QSet<QByteArray> cars;
            for (const QByteArray &car_str : vec) {
                CarT car = CarT::lfsCarFromString(car_str);
                bool is_lfs_car = car.isStockLfsCar();
                if (custom_car_names.contains(car_str.data())
                        || is_lfs_car ) {
                    cars.insert(car_str);
                }
                else {
                    mLog(Log::WARNING) << "no such car:"
                                       << car_str
                                       << ", not adding it to class "
                                       << set_iter.getName();
                }
            }
            if (cars.size() > 1) {
                mClasses[set_iter.getName().c_str()] = cars;
            }
            else {
                mLog(Log::WARNING) << "not adding empty class: "
                                   << set_iter.getName();
            }
            set_iter.peekNext();
        }
    }
}

void LfsDatabasesManager::fillTrackWrs (const std::string &pTrackName) {
    QMapIterator<CarT, LapsDbStorage::Laptime>
            it(mWrDb.getDb()[QByteArray::fromStdString(pTrackName)]);
    while (it.hasNext()) {
        it.next();
        mTrackDatabases[pTrackName]
                ->addLaptime("top", it.key(),
                             TyreT::wrTyres(),
                             it.value());
    }
}

void LfsDatabasesManager::fillWrs () {
    for (const std::string &track: mTrackDatabases.keys()) {
        fillTrackWrs(track);
    }
}


std::shared_ptr<LapsDb> LfsDatabasesManager::getDatabase(const std::string &pTrackName) {
    QMutexLocker locker(&mMutex);
    if (mTrackDatabases.contains(pTrackName)) {
            mTrackUsageCounters[pTrackName] += 1;
            return mTrackDatabases[pTrackName];
    }
    mTrackDatabases[pTrackName].reset(loadDatabase(pTrackName));
    fillTrackWrs(pTrackName);
    mTrackUsageCounters[pTrackName] = 1;
    return mTrackDatabases[pTrackName];
}

void LfsDatabasesManager::freeDatabase (const std::string &pTrackName) {
    QMutexLocker locker(&mMutex);
    if (mTrackUsageCounters[pTrackName] == 1) {
        mTrackUsageCounters.remove(pTrackName);
        mTrackDatabases.remove(pTrackName);
    }
    else {
        mTrackUsageCounters[pTrackName] -= 1;
    }
}

QByteArray LfsDatabasesManager::getIpLocation (const unsigned pIp) {
    QMutexLocker locker(&mIpLocationMutex);

    union wtf {
        unsigned all;
        unsigned char by_one[4];
    };

    wtf orig;
    orig.all = pIp;
    wtf reversed;
    reversed.by_one[0] = orig.by_one[3];
    reversed.by_one[1] = orig.by_one[2];
    reversed.by_one[2] = orig.by_one[1];
    reversed.by_one[3] = orig.by_one[0];

    IP2LocationRecord *record =
        IP2Location_ipv4_get_country_long_binary (mIp2Location, reversed.all);

    QByteArray ret (record->country_long);

    IP2Location_free_record(record);

    return ret;
}

void LfsDatabasesManager::saveDatabases () {
    QMutexLocker locker(&mMutex);
    mDatabasesSaveTimer.setInterval(mSaveIntervalMins * 1000 * 60);
    mDatabasesSaveTimer.start();

    for (auto& e : mTrackDatabases) {
        e->writeFileIfNeed();
    }
}
