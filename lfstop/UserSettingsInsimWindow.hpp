// Copyright 2015 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of LFSTop.
//
// LFSTop is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LFSTop is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LFSTop.  If not, see <http://www.gnu.org/licenses/>.


#ifndef LFSTOP_SETTINGS_WINDOW_HPP_INCLUDED
#define LFSTOP_SETTINGS_WINDOW_HPP_INCLUDED


#include <QAbstractSocket>
#include <QBitArray>


#include "IInsimWindow.hpp"
#include "InsimPackets/InsimButton.hpp"
#include "InsimPackets/InsimButtonFunction.hpp"
#include "UserSettings.hpp"
#include "Translation.hpp"
#include "LfsTcpSocket.hpp"


class UserSettingsInsimWindow final: public IInsimWindow {
public:
    UserSettingsInsimWindow (LfsTcpSocket * const pSock,
                             UserSettings * const pSettings,
                             const uint8_t pUcid,
                             const Translation::Translation *pTranslations,
                             const Translation::TranslationBinder * const pTr)
        : mButton ("", 1, 0, 0, 0, 0, 0, 0),
          mSock (pSock),
          mUserSettings (pSettings),
          mTranslations (pTranslations),
          mUcid (pUcid),
          mTr (pTr)
    {
    }

    void clearWindow () override;

    // IInsimWindow interface
protected:
    void showWindow () override;

    void buttonClick (const IS_BTC &pButtonClick) override;
    void clearedByUser () override {
    }

private:
    void sendHeaderButton ();
    void sendOkButton ();
    void sendOnTrackHeader ();
    void sendOnTrackSelfHeader ();
    void sendOnTrackOthersHeader ();
    void sendOffTrackHeader ();
    void sendUpdateTypesHeaders ();
    void sendUpdateTypesOptions (const int pNum,
                                 const UserSettings::LaptimeUpdates &pUpd);
    void sendLaptimeUpdatesSettings ();
    void sendSettingsButtons ();
    void sendHlvcPrintReasonsButton ();
    void sendHlvcPrintReasonsHeaderButtons ();
    void sendLanguageButtons ();

    void sendBackgroundButton ();

    QByteArray getPrintableLanguageName (const QByteArray &pLang);

    InsimButton mButton;
    LfsTcpSocket * const mSock;
    UserSettings * const mUserSettings;
    const Translation::Translation *mTranslations;

    unsigned char mUcid;
    unsigned char mCurrentButtonId = 0; // also amount of used click ids
    unsigned char mCurrentSettingsButtonId = 0;
    unsigned char mCurrentLanguageSettingsButtonId = 0;

    unsigned char mNumRowsOnPage = 0;
    static constexpr unsigned char mRowHeight = 6;
    static constexpr unsigned char mHeaderHeight = 6;
    unsigned char mWindowHeight = 0;
    unsigned char mWindowCoordTop = 0;
    static constexpr unsigned char mNavButtonHeight = 8;

    static constexpr unsigned char mButtonWidth = 12;
    static constexpr unsigned char mHeaderButtonWidth = 24;
    static constexpr unsigned char mSettingsNum = 13;
    static constexpr unsigned char mLanguageButtonWidth = 12;
    const Translation::TranslationBinder * const mTr;

    unsigned char mCurrentLanguageButtonId = 255;
};

#endif // LFSTOP_SETTINGS_WINDOW_HPP_INCLUDED
