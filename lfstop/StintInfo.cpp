// Copyright 2015 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of LFSTop.
//
// LFSTop is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LFSTop is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LFSTop.  If not, see <http://www.gnu.org/licenses/>.


#include "StintInfo.hpp"


StintInfo::StintInfo (QByteArray pTrackName, const size_t pSectionsNum)
    : trackName (pTrackName) {
    sc.reserve(2);
    sc.push_back(std::vector<uint32_t>());
    sc.push_back(std::vector<uint32_t>());
    sc[0].reserve(pSectionsNum);
    sc[1].reserve(pSectionsNum);

    for (size_t i = 0; i < pSectionsNum; ++i) {
        sc[0].push_back(std::numeric_limits<uint32_t>::max());
        sc[1].push_back(std::numeric_limits<uint32_t>::max());
    }
}
