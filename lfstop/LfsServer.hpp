// Copyright 2015 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of LFSTop.
//
// LFSTop is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LFSTop is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LFSTop.  If not, see <http://www.gnu.org/licenses/>.


#ifndef LFSTOP_LFS_SERVER_HPP_INCLUDED
#define LFSTOP_LFS_SERVER_HPP_INCLUDED


#include <unordered_map>


#include <QObject>
#include <QTcpSocket>
#include <QString>
#include <QUdpSocket>
#include <QTimer>


#include <fstream>
#include <string>


#include "Databases/LapsDb.hpp"
#include "Databases/UsersDb.hpp"
#include "Databases/DatabasesManager.hpp"
#include "SwitchableInsimWindow.hpp"
#include "DataProviders/HelpDataProvider.hpp"
#include "ConnectionInfo.hpp"
#include "DataProviders/CarsDataProvider.hpp"
#include "LfsTcpSocket.hpp"
#include "LfsInsimUtils.hpp"
#include "DataProviders/RecentDriversDataProvider.hpp"
#include "DataProviders/VersionDataProvider.hpp"
#include "DataProviders/LogDataProvider.hpp"
#include "Translation.hpp"
#include "TrackVoteDispatcher.hpp"
#include "DataProviders/CustomInfoDataProvider.hpp"
#include "LfsServerConfig.hpp"
#include "Databases/StintsDb.hpp"
#include "Utils/RecentPacketsContainer.hpp"



class LFSTop;

class LfsServer : public QObject {
    Q_OBJECT
public:
    LfsServer (LFSTop* pLfsTop,
               const LfsServerConfig pCfg,
               LogDestination &pLogDestination,
               Log &pPerformanceLog,
               const QByteArray &pInstanceName,
               LfsDatabasesManager *pDbManager,
               UsersDb *pUsersDb,
               const Translation::Translation &pTr);
    ~LfsServer ();


    qint64 getReceivedBytes () const {
        return mReceivedBytes;
    }
    qint64 getSentBytes () const {
        return mSock->getSentBytes();
    }

    qint64 getConnectionUptime () {
        assert(mIsStarted);
        return QDateTime::currentMSecsSinceEpoch() - mConnectedTimeStamp;
    }

public slots:
    void dataReceived ();
    void udpDataReceived ();

    void stateChanged (QAbstractSocket::SocketState pState);

    void errorOccured (QAbstractSocket::SocketError pError);

    void run ();
    void disconnected ();
    void shutdown ();

    void trackVoteTimerTimeOut ();
    void trackVoteTimerSecondsTimerTimeOut ();
    void udpReceivalCheckTimeOut ();

signals:
    void finished (const QByteArray &pInstanceName, const bool pSuccessfuly);
    void shutdownRequested ();
    void restartRequested ();

private:
    friend class RecentStintsDataProvider;

    void loadDatabase ();

    void sendNewlapMessage (const ConnectionInfo &pConn,
                            const LapsDbAddAnswer &pAnswer,
                            const UserSettings::LaptimeUpdates::Types pType,
                            const LapsDbStorage::Laptime &pLap,
                            const ConnectionInfo &pDriverConn);

    void sendNewlapMessages (const LapsDbAddAnswer &pAnswer,
                             const LapsDbStorage::Laptime &pLap,
                             const ConnectionInfo &pConn,
                             const UserSettings::LaptimeUpdates::Types pType);

    bool newLap (const QByteArray &pDbName,
                 const LapsDbStorage::Laptime &pLap,
                 const bool pClean,
                 const ConnectionInfo &pConn,
                 const bool pSendMessage = true);

    bool clarifyTopQueryCar (LapsdbQuery &pQuery, const QByteArray &pString);
    bool clarifyTopQueryClass (LapsdbQuery &pQuery, const QByteArray &pString);
    bool clarifyTopQuery (LapsdbQuery &pQuery, const QByteArray &pString);

    void processAdminCommand (ConnectionInfo &pConn,
                              const std::vector<QByteArray> &pCommand);
    void processUserCommand (ConnectionInfo &pConn,
                             const std::vector<QByteArray> &pCommand);

    /// a wrapper to be called from a DataProvider for example
    void processUserCommand (const uint8_t pUcid,
                             const std::vector<QByteArray> &pCommand);

    void processDbQuery (ConnectionInfo &pConn,
                         std::vector<QByteArray>::const_iterator pParamsIt,
                         const std::vector<QByteArray>::const_iterator &pParamsEnd,
                         const QByteArray &pDbName,
                         const QByteArray &pHeaderFormat);
    void processStintRequested (ConnectionInfo &pConn,
                                const std::vector<QByteArray> pCommand);
    void processStintsRequested (ConnectionInfo &pConn);
    void processPbRequested (ConnectionInfo &pConn,
                             std::vector<QByteArray>::const_iterator pParamsIt,
                             const std::vector<QByteArray>::const_iterator &pParamsEnd);
    void processHelpRequested (ConnectionInfo &pConn);
    void processLogRequested (ConnectionInfo &pConn);
    void processPlayersRequested (ConnectionInfo &pConn);
    void processRecentRequested (ConnectionInfo &pConn);

    quint8 findUcidByString (const QByteArray &pString) const;

    void initializeDefaultQuery (LapsdbQuery &pQuery,
                                 const ConnectionInfo &pConn,
                                 const QByteArray &pDbName);

    void initializeDefaultCarQuery (LapsdbQuery &pQuery,
                                    const ConnectionInfo &pConn,
                                    const QByteArray &pDbName);

    void gotPacket (const unsigned char* pPacket);

    void invalidatePlayerLap (const quint8 pPlid,
                              const StintInfo::Lap::ExcludeReason pReason,
                              bool pNotify = true);

    bool isAdminCommand (const std::vector<QByteArray> &pCommand);

    void finalizeLap (const IS_LAP* const pLap);
    void processAvgLap (const ConnectionInfo &pConn);
    void processTheoreticalBestLap (ConnectionInfo &pConn,
                                    const LapsDbStorage::Laptime &pLaptime,
                                    const bool pIsPbPrinted);
    void processTheoreticalBestlapStintwise(ConnectionInfo &pConn,
                                            bool &pIsPbPrinted);
    void processStintInfoBestlaps (ConnectionInfo &pConn);

    bool checkPlayerFlags (std::vector<QByteArray> &pFixSetupMessages,
                           const word pFlags,
                           const byte pUcid);

    // Packets processing functions:
    void gotNewConnectionPacket (const IS_NCN * const cn);
    void gotNewConnectionInfoPacket(const IS_NCI * const cn);
    void gotConnectionLeavePacket (const IS_CNL * const cn);
    void gotNewPlayerPacket (const IS_NPL * const pl);
    void gotPlayerFlagsPacket (const IS_PFL * const pfl);
    void gotPlayerLeavePacket (const IS_PLL * const pl);
    void gotPlayerPitPacket (const IS_PLP * const pl);
    void gotStatePackPacket (const IS_STA * const state);
    void gotPlayerRenamePacket (const IS_CPR * const cpr);
    void gotInsimVersionPacket (const IS_VER * const ver);
    void gotLapPacket (const IS_LAP * const lap);
    void gotSplitPacket (const IS_SPX * const spl);
    void gotHotlapValidationPacket (const IS_HLV * const hlv);
    void gotCarsContactPacket (const IS_CON * const con);
    void gotObjectHitPacket (const IS_OBH * const obh);
    void gotRaceStatePacket (const IS_RST * const rst);
    void gotMessageOutPacket (const IS_MSO * const mso);
    void gotButtonClickPacket (const IS_BTC * const btc);
    void gotPlayerPitlanePacket (const IS_PLA * const pit);
    void gotCarResetPacket (const IS_CRS * const crs);
    void gotAdminCommandReportPacket (const IS_ACR * const acr);
    void gotAutoXInfoPacket (const IS_AXI * const pPacket);
    void gotTakeOverPacket (const IS_TOC * const pPacket);
    void gotButtonFunctionPacket (const IS_BFN * const pPacket);
    void gotAutoXClearedPacket ();
    void gotRaceEndPacket ();
    void gotVoteNotifyPacket (const IS_VTN * const pPacket);


    void notifyAdmin (const QByteArray &pMsg);

    QByteArray getAllowedCarsMsg() const;
    void gatherLfsCarsSetting(const IS_SMALL * const pPacket);
    void gatherLfsCarsSetting (const QByteArray &pCommand);
    void refillCarsFromLfsCars ();

    CarT getMostUsedFromAllowedCars (const QByteArray &pDbName,
                                     const TyreT pTyres);
    ClassT getMostUsedFromAllowedClasses (const QByteArray &pDbName,
                                          const TyreT pTyres);

    QSet<CarT> parseCarsCommand (const QByteArray &pCommand);

    IS_MST createCarsCommand (const QSet<CarT> &cars);
    void executeCarsCommand (const QSet<CarT> &pCars);

    void spectatePlayer (const ConnectionInfo &pConn);

    std::vector<LaptimeT> getTb(const ConnectionInfo &pConn,
                             const bool pClean = true);

    void checkDraft (const CompCar &pCar1, const CompCar &pCar2);
    void checkDraft ();

    void resetMciInterval ();

    bool checkPlayerClutchSetup (const word pFlags);
    bool checkPlayerGearinghSetup (const IS_NPL * const pl);

    void collectConnectionQualityData (const unsigned char pPlid,
                                       const bool pIsLagging);
    bool checkPlayerCarAndSetup(const IS_NPL * const pl,
                                 const bool pIsProcessingJoinRequest);

    void saveStintToStintsDb (ConnectionInfo & pConn);

    void saveData ();
    void fillAllowedClasses ();

    const QByteArray mInstanceName;

    const LfsServerConfig mCfg;
    std::string mCarsConfig;

    // resources
    LogFile mLogFile;
    Log mLog;
    LogFile mChatLogFile;
    Log mChatLog;
    LogFile mInsimLogFile;
    Log mInsimLog;
    Log &mPerformanceLog;

    const Translation::Translation &mTr;
    LfsTcpSocket *mSock = nullptr;
    QUdpSocket *mUdpSock = nullptr;
    LfsInsimUtils::StringDecoder mDecoder;
    std::shared_ptr<LapsDb> mDb;
    LfsDatabasesManager * const mDbManager;
    UsersDb * const mUsersDb;
    QTimer mUdpReceivalCheckTimer;

    // TODO: make OverwritableLimitedContainer compile-time limited
    RecentPacketsContainer mRecentPackets;

    // server state
    QByteArray mTrack;
    QByteArray mTrackConfig;
    std::unordered_map<quint8, quint8> mDrivers; // PLID: UCID
    std::unordered_map<quint8, ConnectionInfo> mConns; // UCID : ConnectionInfo
    std::vector<quint8> mAdminUcids;
    QByteArray mLoadedTrackName;
    size_t mNumSplits = 0;
    bool mIsStarted = false;
    bool mConnected = false;
    bool mOpenConfig = false;
    qint64 mReceivedBytes = 0;
    qint64 mConnectedTimeStamp = 0;
    bool mTrackLoadingIsInProcess = false;
    bool mIsInRacingMode = false;
    uint8_t mRaceLaps = 0;

    // server current config
    const QSet<StintInfo::Lap::ExcludeReason> * mHlvcCurrentConfig = &mCfg.hlvcConfig;
    bool mIsQuiet = false;
    unsigned mInitialAllowedCarsSyncNeeded = true;
    QSet<CarT> mCarsAllowed;
    std::map<ClassT, QSet<CarT>> mClassesAllowed; //
    QSet<CarT> mAmbiguousLfsCars; // used when we set cars by ourself
    bool mIsExecutingCarsCommand = false;
    quint16 mUdpPort = 0;


    // draft checker
    std::unordered_map<byte, CompCar> mCarPositions;
    std::unordered_map<byte, qint64> mCarDraftTimestamps;


    // data providers
    HelpDataProvider mHelpDataProvider;
    HelpDataProvider mHelpDataProviderAdmin;
    PlayersDataProvider mPlayersDataProvider;
    LogDataProvider mLogDataProvider;
    CarsDataProvider mCarsDataProvider;
    std::map<std::string, CustomInfoDataProvider> mCustomInfoDataProviders;
    RecentDriversDataProvider mRecentDriversDataProvider;
    VersionDataProvider mVersionDataProvider;

    // track voting stuff
    TrackVoteDispatcher mVoteDispatcher;
    QByteArray mVotedTrackName;
    QTimer mTrackVoteTimer;
    QTimer mTrackVoteTimerSecondsCounter;

    // stints
    StintsDb mStintsDb;
};

#endif // LFSTOP_LFS_SERVER_HPP_INCLUDED
