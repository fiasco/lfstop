// Copyright 2015 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of LFSTop.
//
// LFSTop is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LFSTop is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LFSTop.  If not, see <http://www.gnu.org/licenses/>.


#include <QCoreApplication>
#include <QTimer>
#include <QtGlobal>
#include <QDir>
#include <QThread>
#include <QSharedMemory>
#include <QSystemSemaphore>
#include <QDirIterator>


#include "LfsTopVersion.hpp"
#include "LfsTop.hpp"
#include "SignalHandler.hpp"
#include "Logging/Log.hpp"
#include "Logging/LogStdOut.hpp"
#include "Utils/StdVectorUtils.hpp"


class Application : public SignalHandler {
public:
    Application (Log& pLog, LogStdOut &pStdOut, int argc, char *argv[])
        : SignalHandler (),
          mQtApp (argc, argv),
          mLog (pLog),
          mStdOut (pStdOut),
          mApp (mLog, mStdOut) {
        const std::vector<std::string> args(argv, argv+argc);
        const bool is_restartable =
            vector_contains(args, std::string("--restart-possible"));
        mApp.setRestartPossible(is_restartable);
        mIsRestartable = is_restartable;
    }

    int run () {
        QObject::connect(&mApp, SIGNAL(finished()), &mQtApp, SLOT(quit()));
        QTimer::singleShot(0, &mApp, SLOT(run()));



        int exit_status = mQtApp.exec();
        mRunning = false;

        if (mIsRestartable) {
            if (mApp.getIsRestartRequested()) {
                mLog(Log::INFO) << "restart requested (returning code 21)";
                return 21;
            }
        } else {
            mLog(Log::INFO) << "shut down";
        }

        if (exit_status == 0 && !mApp.allFinishedSuccessfuly()) {
            exit_status = -1;
        }


#ifdef _WIN32
        if (!mApp.allFinishedSuccessfuly()) {
            std::getchar();
        }
#endif // _WIN32

        return exit_status;
    }

    bool handleSignal (const int pSignal) override {
        (void) pSignal;
        mApp.shutdownRequested();
#ifdef _WIN32
        // when we get a signal in windows, we need to wait until main thread
        // returns, otherwise the process is shut down in case it is console
        // close
        while (mRunning) {
            QThread::sleep(1);
        }
#endif // _WIN32
        return true;
    }
private:
    QCoreApplication mQtApp;

    Log &mLog;
    LogStdOut &mStdOut;

    LFSTop mApp;

    bool mRunning = true;
    bool mIsRestartable = false;
};

bool check_file_is_present (const QString &pFileName) {
    QFileInfo checkFile(pFileName);
    return checkFile.exists() && checkFile.isFile();
}

void print_files_that_might_be_sufficient (const QString &pFileName,
                                           Log &log) {
    bool header_printed = false;
    QDirIterator dirIt(".");
    while (dirIt.hasNext()) {
        dirIt.next();
        QFileInfo info(dirIt.filePath());
        if (info.isFile()
            && info.fileName().startsWith(pFileName)) {
            if (!header_printed) {
                log(Log::INFO) << "maybe you want to rename this:";
                header_printed = true;
            }
            log(Log::INFO) << "    " << info.fileName();
        }
    }
}

void dump_language_file () {
    Translation::Translation tr;
    tr.dumpTranslationData();
}

void check_language_file (Log &log) {
    Translation::Translation tr;
    tr.validateTranslations(log);
}

int main (int argc, char *argv[]) {
    LogStdOut log_std_out;
    Log log("LfsTop", log_std_out, "%datetime %level %msg");

    if (argv[1] != nullptr && std::string(argv[1]) == "dump_lang") {
        dump_language_file();
        return 0;
    }

    if (argv[1] != nullptr && std::string(argv[1]) == "check_lang") {
        check_language_file(log);
        return 0;
    }

    QFileInfo inf(".");
    QString path = inf.canonicalFilePath();

    if (!check_file_is_present("config.ini")) {
        log(Log::ERROR) << "\"config.ini\" file is not present in " << path;
        print_files_that_might_be_sufficient("config.ini", log);
#ifdef _WIN32
        std::getchar();
#endif // WIN32
        return -1;
    }

    if (!check_file_is_present("data/IP2LOCATION-LITE-DB1.BIN")) {
        log(Log::ERROR) << "\"IP2LOCATION-LITE-DB1.BIN\" file is not present in " << path + "/data";
#ifdef _WIN32
        std::getchar();
#endif // WIN32
        return -1;
    }

    bool isRunning = false;
    QSystemSemaphore sema("LfsTopSemaphore " + path, 1);
    sema.acquire();

    {
        // cleanup the shared memory if we crashed
        QSharedMemory shmem("LfsTopMutex " + path);
        shmem.attach();
    }

    QSharedMemory shmem("LfsTopMutex " + path);
    if (shmem.attach()) {
        isRunning = true;
    } else {
        shmem.create(1);
    }

    sema.release();


    if (isRunning) {
        log(Log::ERROR) << "LfsTop (or importer) is running in " << path;
#ifdef _WIN32
		std::getchar();
#endif // WIN32
        return -1;
    }

    QDir dir("logs");
    if (!dir.exists()) {
        dir.mkpath(".");
    }

    LogFile log_file("logs/main.log");
    log.addDestination(log_file, "%datetime %level %msg");

    log() << "====================================";
    log() << "LfsTop " << LfsTopVersion::as_string << " starting up...";

    try {
        Application app(log, log_std_out, argc, argv);
        return app.run();
    }
    catch (const std::exception &e) {
        log(Log::ERROR) << e.what();
#ifdef _WIN32
		std::getchar();
#endif // _WIN32
		throw;
    }
	catch (...) {
		log(Log::ERROR) << "unhandled exception";
#ifdef _WIN32
		std::getchar();
#endif // _WIN32
		throw;
	}
    return -1;
}
