// Copyright 2016 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of LFSTop.
//
// LFSTop is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LFSTop is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LFSTop.  If not, see <http://www.gnu.org/licenses/>.


#ifndef LFSTOP_LFS_SERVER_CONFIG_HPP_INCLUDED
#define LFSTOP_LFS_SERVER_CONFIG_HPP_INCLUDED


#include <string>
#include <map>


#include <QSet>
#include <QByteArray>


#include "StintInfo.hpp"


class LfsServerConfigClutchTypes {
public:
    static constexpr quint8 Auto = 1;
    static constexpr quint8 Axis = 2;
    static constexpr quint8 Button = 4;
};


// to see default values, see LfsConfigLoader.hpp
struct LfsServerConfig {
    std::string host;
    uint16_t port;
    uint16_t udpPort;
    std::string password;

    bool forbidTc;
    bool forbidAutoGears;
    uint8_t clutchTypesAllowed;

    char commandPrefix;

    bool recentDriversAdminsOnly;
    size_t recentDriversLimit;

    bool logAdminsOnly;
    size_t logLimit;

    bool dataExportEnable;
    std::string dataExportPath;
    std::string dataExportUrlPrefix;

    std::string welcomeMessage;

    bool performanceLoggingEnabled;
    bool debugInsimPackets;

    QSet<StintInfo::Lap::ExcludeReason> hlvcConfig;
    QSet<StintInfo::Lap::ExcludeReason> hlvcOpenconfigConfig;
    std::map<QByteArray, QSet<StintInfo::Lap::ExcludeReason>> hlvcConfigPerTrack;

    bool trackVoteEnable;
    int trackVoteTime;
    int trackVoteTimeToCancel;

    // can be changed while running
    std::string carsConfig;

    std::vector<std::pair<std::string, std::string>> customCommands;

    size_t numStintsToSave;
    size_t stintsMinLapsToSave;

    bool isRestartPossible;
};


#endif // LFSTOP_LFS_SERVER_CONFIG_HPP_INCLUDED
