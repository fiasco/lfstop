// Copyright 2015 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of LFSTop.
//
// LFSTop is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LFSTop is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LFSTop.  If not, see <http://www.gnu.org/licenses/>.


#ifndef LFSTOP_COMMON_HPP_INCLUDED
#define LFSTOP_COMMON_HPP_INCLUDED


#include <QByteArray>


#include <string>


#include "Databases/LapsDbStorage.hpp"
#include "StintInfo.hpp"
#include "Translation.hpp"


class LapsDb;


std::vector<LaptimeT> stint_sections_to_splits (std::vector<LaptimeT> pVec);

std::string format_time_stdstring (const LaptimeT laptime);
std::string print_scs_stdstring (const LapsDbStorage::Laptime &lap);

QByteArray format_time (const LaptimeT laptime);

QByteArray print_sps (const QVector<LaptimeT> &splits,
                      const char sep = ' ');
QByteArray print_scs (const QVector<LaptimeT> &splits,
                      const LaptimeT laptime,
                      const char sep = ' ');
QByteArray print_scs (const LapsDbStorage::Laptime &lap, const char sep = ' ');
QByteArray print_sps (const LapsDbStorage::Laptime &lap, const char sep = ' ');

QByteArray qt_time_stamp_diff_to_str (const qint64 pDiff,
                                      const Translation::TranslationBinder &pTr);

QByteArray car_definition_to_string (LapsDb *db, const CarDefinition &def);

QByteArray set_to_string (const QSet<QByteArray> &set);

QByteArray print_tyre_compound (const TyreT tyre);

QByteArray stb_print_scs (const StintInfo::Lap &pLap,
                          const std::vector<size_t> &pHighlightScs,
                          const QByteArray &pRevertTo);

QByteArray stb_print_scs (const StintInfo::Lap &pLap,
                          const char pSeparator = ' ');

inline TyreT parse_tyre (const QByteArray &pString) {
    QByteArrayList tyres_vec(pString.split('-'));
    if (tyres_vec.size() == 1) {
        return TyreT::fromOne(TyreT::getTyreByName(pString));
    } else if (tyres_vec.size() == 2) {
        uint8_t t1 = TyreT::getTyreByName(tyres_vec[0]);
        uint8_t t2 = TyreT::getTyreByName(tyres_vec[1]);

        return TyreT(t1, t1, t2, t2);
    } else if (tyres_vec.size() == 4) {
        uint8_t t1 = TyreT::getTyreByName(tyres_vec[0]);
        uint8_t t2 = TyreT::getTyreByName(tyres_vec[1]);
        uint8_t t3 = TyreT::getTyreByName(tyres_vec[2]);
        uint8_t t4 = TyreT::getTyreByName(tyres_vec[3]);

        return TyreT(t1, t2, t3, t4);
    }

    return TyreT::unknownTyres();
}

inline bool clarify_top_query_tyre (TyreT &pTyres, const QByteArray &pString) {
    const auto t = parse_tyre(pString);

    if (t == TyreT::unknownTyres()) {
        return false;
    }

    pTyres = t;

    return true;
}


#endif // LFSTOP_COMMON_HPP_INCLUDED
