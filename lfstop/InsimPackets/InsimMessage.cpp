// Copyright 2015 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of LFSTop.
//
// LFSTop is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LFSTop is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LFSTop.  If not, see <http://www.gnu.org/licenses/>.


#include "InsimMessage.hpp"


#include <cassert>


InsimMessage::InsimMessage (const QByteArray &message,
                            const unsigned char conn_id,
                            const unsigned char ply_id,
                            const unsigned char sound) {
    mPacket->Type = ISP_MTC;
    mPacket->ReqI = 0;
    mPacket->Sound = sound;

    mPacket->UCID = conn_id;
    mPacket->PLID = ply_id;

    mPacket->Sp2 = 0;
    mPacket->Sp3 = 0;

    setText(message);
}


InsimMessage & InsimMessage::setConnectionId (const unsigned char pConnId) {
    mPacket->UCID = pConnId;
    return *this;
}

InsimMessage::InsimMessage () {
    mPacket->Type = ISP_MTC;
    mPacket->ReqI = 0;
    mPacket->Sound = 0;

    mPacket->UCID = 0;
    mPacket->PLID = 0;

    mPacket->Sp2 = 0;
    mPacket->Sp3 = 0;

    mPacket->Size = 0;
}

InsimMessage &InsimMessage::setText (const QByteArray &pMessage) {
    mPacket.setText(pMessage.constData(), pMessage.size());
    mPacket->Size = mPacket.getLength();

    return *this;
}

const char * InsimMessage::getPacket () const {
    assert(mPacket.getLength());
    return mPacket.getRawPtr();
}

int InsimMessage::getLength () const {
    return mPacket.getLength();
}
