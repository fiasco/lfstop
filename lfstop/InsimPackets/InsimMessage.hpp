// Copyright 2015 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of LFSTop.
//
// LFSTop is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LFSTop is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LFSTop.  If not, see <http://www.gnu.org/licenses/>.


#ifndef LFSTOP_INSIM_MESSAGE_HPP_INCLUDED
#define LFSTOP_INSIM_MESSAGE_HPP_INCLUDED


#include <QByteArray>


#include "IInsimPacket.hpp"
#include "VariableLengthInsimPacketStorage.hpp"
#include "CompilerUtils.hpp"
#include "LfsInsim.hpp"


class InsimMessage : public IInsimPacket {
public:
    InsimMessage (const QByteArray &message,
                  const unsigned char conn_id = 0,
                  const unsigned char ply_id = 0,
                  const unsigned char sound = 0);

    InsimMessage & setConnectionId (const unsigned char pConnId);

    InsimMessage ();

    InsimMessage & setText (const QByteArray &pMessage);

    // Implements IInsimPacket
    const char * getPacket () const override
        LFSTOP_FUNCTION_ATTRIBUTE_CONST;
    int getLength () const override
        LFSTOP_FUNCTION_ATTRIBUTE_CONST;

private:
    VariableLengthInsimPacketStorage<IS_MTC, int, 136> mPacket;
};

#endif // LFSTOP_INSIM_MESSAGE_HPP_INCLUDED
