// Copyright 2016 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of LFSTop.
//
// LFSTop is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LFSTop is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LFSTop.  If not, see <http://www.gnu.org/licenses/>.


#ifndef LFSTOP_VARIABLE_LENGTH_INSIM_PACKET_STORAGE_HPP_INCLUDED
#define LFSTOP_VARIABLE_LENGTH_INSIM_PACKET_STORAGE_HPP_INCLUDED


#include <cstddef>


template<typename T,
         typename SizeType,
         SizeType MaxPacketSize,
         SizeType PadTo = 4>
class VariableLengthInsimPacketStorage {
public:
    VariableLengthInsimPacketStorage () {
        static_assert(std::is_pod<T>::value,
                      "T must be pod");
        static_assert(MaxPacketSize >= sizeof(T) + PadTo,
                      "MaxPacketSize must be more or equal to "
                      "sizeof(T) + PadTo");
        static_assert(std::numeric_limits<SizeType>::max()
                      <= std::numeric_limits<size_t>::max(),
                      "SizeType must fit into size_t");

    }

    void setText (const char* pText, const SizeType pActualMessageSize) {
        SizeType message_size = pActualMessageSize + 1; // + 1 is to count null-char
        constexpr SizeType size_of_T = sizeof(T);
        if (message_size > MaxPacketSize - size_of_T - 1) {
            message_size = MaxPacketSize - size_of_T - 1;
        }
        const SizeType text_len = message_size + PadTo - 1 - (message_size - 1) % PadTo;

        mLength = size_of_T + text_len;

        strncpy(mData + static_cast<size_t>(size_of_T),
                pText,
                static_cast<size_t>(message_size));

        if (pActualMessageSize > message_size) {
            mData[MaxPacketSize - 2] = -123;
        }

        // for some reason lfs wants the very last byte
        // (not just the one that terminates the text itself) to be zero

        mData[mLength - 1] = 0;
    }

    T* operator-> () const {
        return (T*)(mData);
    }

    const char* getRawPtr () const {
        return mData;
    }

    SizeType getLength () const {
        return mLength;
    }

private:
    char mData[MaxPacketSize];
    SizeType mLength = 0;
};


#endif //LFSTOP_VARIABLE_LENGTH_INSIM_PACKET_STORAGE_HPP_INCLUDED
