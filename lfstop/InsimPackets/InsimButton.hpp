// Copyright 2015 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of LFSTop.
//
// LFSTop is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LFSTop is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LFSTop.  If not, see <http://www.gnu.org/licenses/>.


#ifndef LFSTOP_INSIM_BUTTON_HPP_INCLUDED
#define LFSTOP_INSIM_BUTTON_HPP_INCLUDED


#include <QByteArray>


#include "IInsimPacket.hpp"
#include "LfsInsim.hpp"
#include "InsimPacketsUtils.hpp"
#include "VariableLengthInsimPacketStorage.hpp"

class InsimButton : public IInsimPacket {
public:
    InsimButton (const QByteArray &pMessage,
                 const unsigned char pRequestId,
                 const unsigned char pLeft,
                 const unsigned char pTop,
                 const unsigned char pWidth,
                 const unsigned char pHeight,
                 const unsigned char pUcid,
                 const unsigned char pClickId);

    enum Color { light_grey, yellow, black, white, green, red, blue, grey };
    enum Alignment { left, center, right};

    InsimButton & setColor (const InsimButton::Color pColor);
    InsimButton & setClick (const bool pClick) {
        InsimPacketsUtils::setFlag(mPacket->BStyle, ISB_CLICK, pClick);
        return *this;
    }
    InsimButton & setLight (const bool pLight) {
        InsimPacketsUtils::setFlag(mPacket->BStyle, ISB_LIGHT, pLight);
        return *this;
    }
    InsimButton & setDark (const bool pDark) {
        InsimPacketsUtils::setFlag(mPacket->BStyle, ISB_DARK, pDark);
        return *this;
    }
    InsimButton & setLeft () {
        InsimPacketsUtils::setFlag(mPacket->BStyle, ISB_LEFT, true);
        InsimPacketsUtils::setFlag(mPacket->BStyle, ISB_RIGHT, false);
        return *this;
    }
    InsimButton & setRight () {
        InsimPacketsUtils::setFlag(mPacket->BStyle, ISB_RIGHT, true);
        InsimPacketsUtils::setFlag(mPacket->BStyle, ISB_LEFT, false);
        return *this;
    }
    InsimButton & setCenter () {
        InsimPacketsUtils::setFlag(mPacket->BStyle, ISB_RIGHT, false);
        InsimPacketsUtils::setFlag(mPacket->BStyle, ISB_LEFT, false);
        return *this;
    }
    InsimButton & setAlignment (const Alignment pAlign);

    InsimButton & setClickId (const unsigned char pClickId) {
        mPacket->ClickID = pClickId;
        return *this;
    }

    InsimButton & setCoords (const unsigned char pLeft,
                             const unsigned char pTop,
                             const unsigned char pWidth,
                             const unsigned char pHeight) {
        mPacket->L = pLeft;
        mPacket->T = pTop;
        mPacket->W = pWidth;
        mPacket->H = pHeight;
        return *this;
    }

    InsimButton & setText (const QByteArray &pText);
    InsimButton & setUcid (const unsigned char pUcid) {
        mPacket->UCID = pUcid;
        return *this;
    }

    // Implements IInsimPacket
    const char * getPacket () const override {
        return mPacket.getRawPtr();
    }
    int getLength () const override {
        return mPacket.getLength();
    }

private:
    VariableLengthInsimPacketStorage<IS_BTN, int, 252> mPacket;
};

#endif // LFSTOP_INSIM_BUTTON_HPP_INCLUDED
