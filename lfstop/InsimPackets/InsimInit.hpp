// Copyright 2015 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of LFSTop.
//
// LFSTop is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LFSTop is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LFSTop.  If not, see <http://www.gnu.org/licenses/>.


#ifndef LFSTOP_INSIM_INIT_HPP_INCLUDED
#define LFSTOP_INSIM_INIT_HPP_INCLUDED


#include <string>


#include "IInsimPacket.hpp"
#include "LfsInsim.hpp"
#include "CompilerUtils.hpp"


class InsimInit : public IInsimPacket {
public:
    InsimInit (const std::string &pProgramName,
               const std::string &pPassword = "");

    InsimInit & setGetVersion (const bool pGetVersion = true);
    InsimInit & setUdpPort (const unsigned short pPort);
    InsimInit & setLocal (const bool pLocal = true);
    InsimInit & setKeepColors (const bool pKeepColors);
    InsimInit & setNlp (const bool pNlp = true);
    InsimInit & setMci (const bool pMci = true);
    InsimInit & setCon (const bool pCon = true );
    InsimInit & setObh (const bool pObh = true);
    InsimInit & setHlv (const bool pHlv = true);
    InsimInit & setAxmLoad (const bool pAxmLoad = true);
    InsimInit & setAxmEdit (const bool pAxmEdit = true);
    InsimInit & setHostPrefix (const unsigned char pHostPrefix);
    InsimInit & setInterval (const unsigned short pInterval);
    InsimInit & setProcessJoinRequests (const bool pProcess = true);
    InsimInit & setVersion (const unsigned char pVersion);

    // Implements IInsimPacket
    const char * getPacket () const override
        LFSTOP_FUNCTION_ATTRIBUTE_CONST;
    int getLength () const override
        LFSTOP_FUNCTION_ATTRIBUTE_CONST;

private:
    IS_ISI mPacket;
};

#endif // LFSTOP_INSIM_INIT_HPP_INCLUDED
