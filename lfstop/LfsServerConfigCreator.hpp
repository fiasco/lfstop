// Copyright 2016 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of LFSTop.
//
// LFSTop is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LFSTop is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LFSTop.  If not, see <http://www.gnu.org/licenses/>.


#ifndef LFSTOP_LFS_SERVER_CONFIG_CREATOR_HPP_INCLUDED
#define LFSTOP_LFS_SERVER_CONFIG_CREATOR_HPP_INCLUDED


#include "LfsServerConfig.hpp"
#include "Utils/MKeyFile.hpp"
#include "Logging/Log.hpp"


namespace LfsServerConfigLoader {
const LfsServerConfig loadLfsServerConfig (const MKeyFile &pConfigFile,
                                           Log &pLog,
                                           const bool pIsRestartPossible);
}


#endif // LFSTOP_LFS_SERVER_CONFIG_CREATOR_HPP_INCLUDED
