// Copyright 2017 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of LFSTop.
//
// LFSTop is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LFSTop is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LFSTop.  If not, see <http://www.gnu.org/licenses/>.


#ifndef LFSTOP_UTILS_RECENT_PACKETS_CONTAINER_HPP_INCLUDED
#define LFSTOP_UTILS_RECENT_PACKETS_CONTAINER_HPP_INCLUDED


#include "Utils/OverwritableLimitedContainer.hpp"


class RecentPacketsContainer {
public:
    void push_back (const unsigned char* pData, const size_t pSize) {
        // TODO2: can try to avoid temporaries, need to check if it's worth
        // trouble though
        mData.push_back(std::vector<unsigned char>(pData, pData + pSize));
    }

private:
    OverwritableLimitedContainer<std::vector<unsigned char>> mData
        = OverwritableLimitedContainer<std::vector<unsigned char>>(10);
};


#endif // LFSTOP_UTILS_RECENT_PACKETS_CONTAINER_HPP_INCLUDED
