// Copyright 2015 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of LFSTop.
//
// LFSTop is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LFSTop is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LFSTop.  If not, see <http://www.gnu.org/licenses/>.


#ifndef LFSTOP_STD_VECTOR_UTILS_HPP_INCLUDED
#define LFSTOP_STD_VECTOR_UTILS_HPP_INCLUDED

#include <vector>
#include <algorithm>

template <typename T>
long int vector_find (const std::vector<T> &vec, const T &el) {
    long int pos = std::find(vec.begin(), vec.end(), el) - vec.begin();
    return pos;
}

template <typename T>
bool vector_contains (const std::vector<T> &vec, const T &el) {
    return std::find(vec.begin(), vec.end(), el) != vec.end();
}

template <typename T>
bool sorted_vector_contains (const std::vector<T> &vec, const T &el) {
    return std::binary_search(vec.begin(), vec.end(), el);
}

template <typename T, typename Separator>
T vector_join (const std::vector<T> &vec,
               const Separator &&sep) {
    T ret; // consider reserve
    typename std::vector<T>::const_iterator it = vec.cbegin();
    const typename std::vector<T>::const_iterator end = vec.cend();

    for (;it != end; ++it) {
        ret += *it;

        if ((it + 1) != end) {
            ret += sep;
        }
    }

    return ret;
}

#endif // LFSTOP_STD_VECTOR_UTILS_HPP_INCLUDED

