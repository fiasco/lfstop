// Copyright 2015 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of LFSTop.
//
// LFSTop is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LFSTop is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LFSTop.  If not, see <http://www.gnu.org/licenses/>.


#include "LfsTcpSocket.hpp"


qint64 LfsTcpSocket::write (const IInsimPacket &pPacket) {
    const qint64 written =
            QIODevice::write(pPacket.getPacket(), pPacket.getLength());
    mSentBytes += pPacket.getLength();

    return written;
}

qint64 LfsTcpSocket::writeBytes (const QByteArray &pBytes) {
    const qint64 written =
            QIODevice::write(pBytes);
    mSentBytes += pBytes.size();
    return written;
}

qint64 LfsTcpSocket::writeBytes (const char *pData, qint64 pLen) {
    const qint64 written =
            QIODevice::write(pData, pLen);
    mSentBytes += pLen;

    return written;
}
