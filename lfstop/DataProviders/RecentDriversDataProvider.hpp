// Copyright 2015 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of LFSTop.
//
// LFSTop is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LFSTop is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LFSTop.  If not, see <http://www.gnu.org/licenses/>.


#ifndef LFSTOP_RECENT_DRIVERS_DATA_PROVIDER_HPP_INCLUDED
#define LFSTOP_RECENT_DRIVERS_DATA_PROVIDER_HPP_INCLUDED


#include "Translation.hpp"
#include "INonSwitchableInsimWindowDataProvider.hpp"


class UsersDb;


struct RecentDriver {
    QByteArray username;
    qint64 timestamp;
    unsigned char reason;
};


class RecentDriversDataProvider : public INonSwitchableInsimWindowDataProvider {
public:
    // implements INonSwitchableInsimWindowDataProvider
    void prepareData () override {}
    size_t getNumRows () const override {
        return mData.size();
    }
    size_t getNumColumns () const override {
        return 3;
    }
    const std::vector<QByteArray>
    getRow (const size_t pN,
            const Translation::TranslationBinder &pTr) const override;
    const std::vector<InsimWindowColumnFormat> getColumnsFormat () const override {
        return {
            { InsimButton::left, 40 },
            { InsimButton::left, 28 },
            { InsimButton::right, 30 },
        };
    }

    void setLimit (const size_t pLimit) {
        mLimit = pLimit;
    }

    void connected (const QByteArray &pUsername);

    void disconnected (const QByteArray pUsername, const unsigned char pReason);

    void setUsersDb (UsersDb *pDb) {
        mUsersDb = pDb;
    }

private:
    void removeUser (const QByteArray &pUsername);

    std::vector<RecentDriver> mData;

    UsersDb * mUsersDb = nullptr;

    size_t mLimit = 100;
};


#endif // LFSTOP_RECENT_DRIVERS_DATA_PROVIDER_HPP_INCLUDED
