// Copyright 2015 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of LFSTop.
//
// LFSTop is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LFSTop is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LFSTop.  If not, see <http://www.gnu.org/licenses/>.


#ifndef LAPSDB_CARS_DATA_PROVIDER_HPP_INCLUDED
#define LAPSDB_CARS_DATA_PROVIDER_HPP_INCLUDED


#include "INonSwitchableInsimWindowDataProvider.hpp"
#include "Databases/LapsDb.hpp"


class CarsDataProvider final : public INonSwitchableInsimWindowDataProvider {
public:

    void setLapsDb (std::shared_ptr<LapsDb> pDb);

    // INonSwitchableInsimWindowDataProvider interface
    void prepareData () override {}
    size_t getNumRows () const override {
        return mData.size();
    }
    size_t getNumColumns () const override {
        return 1;
    }
    const std::vector<QByteArray>
    getRow (const size_t pN,
            const Translation::TranslationBinder& pTr) const override {
        (void) pTr;
        return { mData[pN] };
    }
    const std::vector<InsimWindowColumnFormat> getColumnsFormat () const override {
        return { { InsimButton::left, 98 } };
    }

private:
    std::vector<QByteArray> mData;
    std::shared_ptr<LapsDb> mDb;
};

#endif // LAPSDB_CARS_DATA_PROVIDER_HPP_INCLUDED
