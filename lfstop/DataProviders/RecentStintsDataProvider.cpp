// Copyright 2016 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of LFSTop.
//
// LFSTop is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LFSTop is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LFSTop.  If not, see <http://www.gnu.org/licenses/>.


#include "RecentStintsDataProvider.hpp"


#include "LfsServer.hpp"
#include "LfsInsimUtils.hpp"


using LfsInsimUtils::insertStr;


void RecentStintsDataProvider::prepareData () {
    mRunningStints.clear();
    for (const auto &el : mLfsServer->mConns) {
        const auto &ci = el.second;
        if (ci.stintInfo.laps.size() != 0) {
            mRunningStints.push_back({ ci.username,
                                       ci.currentCar,
                                       ci.currentTyres,
                                       ci.stintInfo.laps.size() });
        }
    }
}

const std::vector<QByteArray>
RecentStintsDataProvider::getRow (const size_t pN,
                                  const Translation::TranslationBinder &pTr) const {
    if (pN < mRunningStints.size()) {
        return { mUsersDb->userGetFullName(mRunningStints[pN].username),
                    mLapsDb->getCarName(mRunningStints[pN].car),
                    mRunningStints[pN].tyres.toString(),
                    insertStr(pTr.get(Translation::StringIds::NUM_LAPS),
                              QByteArray::number((uint) mRunningStints[pN].numLaps)),
                    pTr.get(Translation::StringIds::ON_TRACK),
        };
    }

    const size_t n = pN - mRunningStints.size();

    assert(n < static_cast<size_t>(std::numeric_limits<long int>::max()));
    auto it = std::next((*mStints).rend(), static_cast<long int>(n) + 1);
    const auto &el = *(it);
    const auto &username = el.username;
    const auto &car_track = mLapsDb->getCarName(el.car)
            + '@' + el.stintInfo.trackName;
    const auto &tyres = el.tyres.toString();
    const auto &stint_info = el.stintInfo;
    QByteArray datetime =
            qt_time_stamp_diff_to_str (QDateTime::currentMSecsSinceEpoch()
                                       - el.timeStamp,
                                       pTr);

    return { mUsersDb->userGetFullName(username),
                car_track,
                tyres,
                insertStr(pTr.get(Translation::StringIds::NUM_LAPS),
                          QByteArray::number((uint) stint_info.laps.size())),
                datetime,
    };
}

void RecentStintsDataProvider::rowClicked (const size_t pWindowRowNum,
                                           const size_t pTableRowNum,
                                           const unsigned short pMouseButtonClickFlags) {
    (void) pWindowRowNum;
    if (pTableRowNum < mRunningStints.size()) {
        mLfsServer->processUserCommand(mUcid,
                                       { "stint",
                                         mRunningStints[pTableRowNum].username });
        return;
    }

    const size_t n = pTableRowNum - mRunningStints.size();

    (void) pMouseButtonClickFlags;
    const QByteArray id =
            QByteArray::number((uint) (mStints->size() - n - 1));
    mLfsServer->processUserCommand(mUcid, { "stint",
                                            '\\'
                                            + id });
}
