// Copyright 2015 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of LFSTop.
//
// LFSTop is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LFSTop is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LFSTop.  If not, see <http://www.gnu.org/licenses/>.


#ifndef LFSTOP_PB_DATA_PROVIDER_HPP_INCLUDED
#define LFSTOP_PB_DATA_PROVIDER_HPP_INCLUDED


#include "ISwitchableInsimWindowDataProvider.hpp"
#include "Databases/LapsDbQuery.hpp"
#include "Translation.hpp"

class LapsDb;


class PbDataProvider final : public ISwitchableInsimWindowDataProvider {
public:
    PbDataProvider (const std::shared_ptr<LapsDb> &pDb,
                    QByteArray pUsername,
                    bool * const pShowSplitsPtr,
                    const Translation::TranslationBinder * const pTr)
        : mDb (pDb),
          mRequestedUsername (pUsername),
          mShowSplits (pShowSplitsPtr),
          mTr (pTr) {
    }

    void setQuery (const LapsdbQuery &pQuery) {
        mData.clear();
        mLaptimesCache.clear();

        mQuery = pQuery;
        getData("top", mTr->get(Translation::StringIds::BEST_LAP));
        getData("tbs", mTr->get(Translation::StringIds::BEST_STINT_LAP_POSSIBLE));
        getData("tb", mTr->get(Translation::StringIds::BEST_LAP_POSSIBLE_EVER));
        getData("avg", mTr->get(Translation::StringIds::BEST_AVERAGE_OF_3_LAPS));
    }

    void setUsername (QByteArray pUsername) {
        mRequestedUsername = pUsername;
    }

    // ISwitchableInsimWindowDataProvider interface
    void prepareData () override {
        resetSplitsSectorsSwitch();
    }
    size_t getNumRows () const override{
        return mData.size();
    }
    size_t getNumColumns () const override {
        return 2;
    }
    const std::vector<QByteArray> getRow (const size_t pN,
                                          const Translation::TranslationBinder& pTr) const override {
        (void) pTr;
        return mData[pN];
    }
    const std::vector<InsimWindowColumnFormat> getColumnsFormat () const override {
        return {
            { InsimButton::left, 40 },
            { InsimButton::left, 58 },
        };
    }
    const QByteArray getRowColumn (const size_t pRow,
                                   const size_t pColumn) const override;
    const QMap<quint8, QByteArray> *getSwitches () const override {
        return &mSwitches;
    }
    std::vector<size_t> switchClicked (const quint8 pSwitchId) override {
        std::vector<size_t> ret;
        if (pSwitchId == 0) {
            ret.push_back(1);
            *mShowSplits = !*mShowSplits;
            resetSplitsSectorsSwitch();
        }
        return ret;
    }

private:
    void resetSplitsSectorsSwitch () {
        using LfsInsimUtils::format;
        using LfsInsimUtils::FormatPair;
        const QByteArray str_to_format = *mShowSplits ? "[ ^7%splits^8 | %sections ]"
                                                      : "[ %splits | ^7%sections^8 ]";

        mSwitches[0] =
            format(str_to_format,
                   FormatPair{ "%splits",
                               mTr->get(Translation::StringIds::SPLITS) },
                   FormatPair{ "%sections",
                               mTr->get(Translation::StringIds::SECTIONS) });
    }

    void getData (const QByteArray &pDbName,
                  const QByteArray &pHeader);

    const std::shared_ptr<LapsDb> &mDb;

    LapsdbQuery mQuery;

    QByteArray mRequestedUsername;

    std::vector<std::vector<QByteArray>> mData;

    QMap<quint8, QByteArray> mSwitches;

    std::vector<QPair<LapsDbStorage::Laptime, TyreT>> mLaptimesCache;

    bool *mShowSplits = nullptr;

    const Translation::TranslationBinder * const mTr;
};

#endif // LFSTOP_PB_DATA_PROVIDER_HPP_INCLUDED
