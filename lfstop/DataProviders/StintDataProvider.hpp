// Copyright 2015 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of LFSTop.
//
// LFSTop is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LFSTop is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LFSTop.  If not, see <http://www.gnu.org/licenses/>.


#ifndef LFSTOP_STINT_DATA_PROVIDER_HPP_INCLUDED
#define LFSTOP_STINT_DATA_PROVIDER_HPP_INCLUDED


#include "INonSwitchableInsimWindowDataProvider.hpp"
#include "Translation.hpp"
#include "StintInfo.hpp"
#include <unordered_map>


class StintDataProvider final: public INonSwitchableInsimWindowDataProvider {
public:
    void setStintInfo (StintInfo *pStintInfo) {
        mStintInfo = pStintInfo;
    }

    const StintInfo * getStintInfo () const {
        return mStintInfo;
    }

    void prepareData () override;

    // IInsimWindowDataProvider interface
    size_t getNumRows () const override {
        return mDataRows.size();
    }
    size_t getNumColumns () const override {
        return 4;
    }
    const std::vector<QByteArray>
    getRow (const size_t pN,
            const Translation::TranslationBinder &pTr) const override;
    const std::vector<InsimWindowColumnFormat> getColumnsFormat () const override {
        const unsigned short pos_w = 16;
        const unsigned short exc_w = 23;
        const unsigned short scs_w = 44;
        const unsigned short lap_w = 15;

        return { { InsimButton::right, pos_w },
                 { InsimButton::center, exc_w },
                 { InsimButton::right, scs_w },
                 { InsimButton::center, lap_w }
        };
    }

    bool isExportable () override {
        return true;
    }

    virtual const QString
    getRowToExport (const size_t pN,
                    LfsInsimUtils::StringDecoder &pDecoder,
                    const Translation::TranslationBinder &pTr) const override;
private:
    const std::vector<QByteArray>
    getRowImpl (const size_t pN,
                const bool pColored,
                const Translation::TranslationBinder &pTr) const;
    void prepareDataAverages (StintInfo::Lap &pTotal,
                              StintInfo::Lap &pAvg,
                              size_t &i,
                              Translation::StringIds::Ids pIdTotal,
                              Translation::StringIds::Ids pIdAverage,
                              const size_t pLapsNum);

    StintInfo *mStintInfo;

    std::vector<StintInfo::Lap> mDataRows;
    std::unordered_map<size_t, Translation::StringIds::Ids> mNamedFields;
    std::unordered_map<size_t, std::vector<size_t>> mLapsToHighlight;
};

#endif // LFSTOP_STINT_DATA_PROVIDER_HPP_INCLUDED
