// Copyright 2015 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of LFSTop.
//
// LFSTop is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LFSTop is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LFSTop.  If not, see <http://www.gnu.org/licenses/>.


#include "RecentDriversDataProvider.hpp"

#include <QDateTime>


#include "Common.hpp"
#include "Databases/UsersDb.hpp"


using Translation::StringIds;
using LfsInsimUtils::insertStr;


QByteArray reasonToString (const unsigned char pReason,
                           const Translation::TranslationBinder &pBind) {
    switch (pReason) {
    case LEAVR_DISCO:
        return pBind.get(StringIds::DISCONNECTED);
    case LEAVR_TIMEOUT:
        return pBind.get(StringIds::TIMED_OUT);
    case LEAVR_LOSTCONN:
        return pBind.get(StringIds::LOST_CONNECTION);
    case LEAVR_KICKED:
        return pBind.get(StringIds::KICKED);
    case LEAVR_BANNED:
        return pBind.get(StringIds::BANNED);
    case LEAVR_SECURITY:
        return pBind.get(StringIds::SECURITY);
    case LEAVR_CPW:
        return pBind.get(StringIds::CHEAT_PROTECTION);
    case LEAVR_OOS:
        return pBind.get(StringIds::OUT_OF_SYNC);
    case LEAVR_JOOS:
        return pBind.get(StringIds::INITIAL_SYNC_FAILED);
    case LEAVR_HACK:
        return pBind.get(StringIds::INVALID_PACKET);
    case 255:
        return pBind.get(StringIds::CONNECTED);
    }
    return "u wot m8?";
}


const std::vector<QByteArray>
RecentDriversDataProvider::getRow (const size_t pN,
                                   const Translation::TranslationBinder &pTr) const {
    const qint64 diff =
            QDateTime::currentMSecsSinceEpoch() - mData[pN].timestamp;

    QByteArray username;

    if (mUsersDb) {
        username =
                mUsersDb->userGetFullName(mData[pN].username);
    } else {
        username = mData[pN].username;
    }

    return { username,
             reasonToString(mData[pN].reason, pTr),
             insertStr(pTr.get(StringIds::AGO),
                       qt_time_stamp_diff_to_str(diff, pTr)) };

}

void RecentDriversDataProvider::connected (const QByteArray &pUsername) {
    if (pUsername.size() == 0) {
        return;
    }

    removeUser(pUsername);

    mData.insert(mData.begin(),
                 { pUsername,
                   QDateTime::currentMSecsSinceEpoch(),
                   255 });
}

void RecentDriversDataProvider::disconnected (const QByteArray pUsername,
                                              const unsigned char pReason) {
    removeUser(pUsername);

    mData.insert(std::begin(mData),
                 { pUsername,
                   QDateTime::currentMSecsSinceEpoch(),
                   pReason });

    if (mData.size() > mLimit) {
        auto it = std::find_if(mData.rbegin(),
                               mData.rend(),
                               [&pUsername] (const RecentDriver &el) {
            return el.reason != 255;
        }).base();

        if (it != mData.begin()) {
            mData.erase(it);
        }
    }
}

void RecentDriversDataProvider::removeUser (const QByteArray &pUsername) {
    mData.erase(std::remove_if(std::begin(mData),
                               std::end(mData),
                               [&pUsername] (const RecentDriver &el) {
                                   return el.username == pUsername;
                               }),
                std::end(mData));
}
