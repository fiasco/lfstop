// Copyright 2015 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of LFSTop.
//
// LFSTop is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LFSTop is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LFSTop.  If not, see <http://www.gnu.org/licenses/>.


#include "StintDataProvider.hpp"
#include "Common.hpp"
#include "StintInfo.hpp"

using Translation::StringIds;
using LfsInsimUtils::insertStr;


void vector_sum (std::vector<LaptimeT> &pList,
                 const std::vector<LaptimeT> &pOther) {
    assert(pList.size() == pOther.size());
    for (size_t i = 0; i < pList.size(); ++i) {
        pList[i] += pOther[i];
    }
}

void vector_divide (std::vector<LaptimeT> &pDest,
                    const std::vector<LaptimeT> &pList,
                    const size_t num) {
    assert(pDest.size() == pList.size());
    for (size_t i = 0; i < pList.size(); ++i) {
        pDest[i] = static_cast<LaptimeT>(pList[i] / num);
    }
}

const std::pair<const StintInfo::Lap,
                const std::unordered_map<size_t, std::vector<size_t>>>
stint_info_get_theoretical_lap (const StintInfo& pStintInfo,
                                const bool pClean) {
    // return theoretical lap and list of laps to be highlighted

    std::pair<StintInfo::Lap,
              std::unordered_map<size_t, std::vector<size_t>>> ret;
    auto &theor_lap = ret.first;
    auto &laps_to_highlight = ret.second;

    const auto &sc = pStintInfo.sc[pClean];
    if (std::find(std::begin(sc), std::end(sc),
                  std::numeric_limits<uint32_t>::max()) == std::end(sc)) {
        for (size_t i = 0; i < sc.size(); ++i) {
            const StintInfo::Lap &lap = pStintInfo.laps[sc[i]];
            if (!lap.exclude) { // probably redundant even when we process clean
                theor_lap.sections.push_back(lap.sections[i]);
                theor_lap.laptime += lap.sections[i];
                laps_to_highlight[sc[i]].push_back(i);
            }
        }
    }

    for (const auto &el : laps_to_highlight) {
        const auto lapnum = el.first;
        const auto laptime = pStintInfo.laps[lapnum].laptime;
        if (laptime == theor_lap.laptime) {
            laps_to_highlight = { { lapnum, {} } };
            break;
        }
    }

    return ret;
}

void StintDataProvider::prepareDataAverages (StintInfo::Lap &pTotal,
                                             StintInfo::Lap &pAvg,
                                             size_t &i,
                                             StringIds::Ids pIdTotal,
                                             StringIds::Ids pIdAverage,
                                             const size_t pLapsNum) {
    vector_divide(pAvg.sections, pTotal.sections, pLapsNum);
    pAvg.laptime = static_cast<LaptimeT> (pTotal.laptime / pLapsNum);

    mDataRows.push_back(pTotal);
    mNamedFields[i++] = pIdTotal;

    mDataRows.push_back(pAvg);
    mNamedFields[i++] = pIdAverage;
}

void StintDataProvider::prepareData () {

    mDataRows.clear();
    mNamedFields.clear();

    const auto theor_lap_ = stint_info_get_theoretical_lap(*mStintInfo, true);
    auto &theor_lap = theor_lap_.first;
    mLapsToHighlight = theor_lap_.second;

    const size_t num_splits = mStintInfo->sc[true].size() - 1;
    StintInfo::Lap total_clean(num_splits);
    StintInfo::Lap avg_clean(num_splits);
    StintInfo::Lap total(num_splits);
    StintInfo::Lap avg(num_splits);

    mDataRows = mStintInfo->laps;
    for (const auto l:  mDataRows) {
        if (l.exclude != StintInfo::Lap::ExcludeReason::LFS_INVALID) {
            if (l.exclude == StintInfo::Lap::ExcludeReason::NONE) {
                vector_sum(total_clean.sections, l.sections);
                total_clean.laptime += l.laptime;
            }
            vector_sum(total.sections, l.sections);
            total.laptime += l.laptime;
        }
    }


    size_t i = mDataRows.size();
    if (theor_lap.laptime > 0 && mLapsToHighlight.size() > 1) {
        mDataRows.push_back(theor_lap);
        mNamedFields[i++] = StringIds::CLEAN_TB;
    }


    if (mStintInfo->cleanLapsNum > 1) {
        prepareDataAverages(total_clean,
                            avg_clean,
                            i,
                            StringIds::CLEAN_TOTAL,
                            StringIds::CLEAN_AVERAGE,
                            mStintInfo->cleanLapsNum);
    }


    const size_t lfs_valid_laps_num =
        mStintInfo->cleanLapsNum + mStintInfo->noncleanLapsNum;

    if (lfs_valid_laps_num > 1
        && (lfs_valid_laps_num > mStintInfo->cleanLapsNum)) {
        prepareDataAverages(total,
                            avg,
                            i,
                            StringIds::TOTAL,
                            StringIds::AVERAGE,
                            lfs_valid_laps_num);
    }
}

const std::vector<QByteArray>
StintDataProvider::getRow (const size_t pN,
                           const Translation::TranslationBinder &pTr) const {
    return getRowImpl(pN, true, pTr);
}

const QString
StintDataProvider::getRowToExport(const size_t pN,
                                  LfsInsimUtils::StringDecoder &pDecoder,
                                  const Translation::TranslationBinder &pTr) const {
    QString ret;
    const auto &row = getRowImpl(pN, false, pTr);
    ret += pDecoder.lfsStringToUnicode(row[0]) + '\t';
    ret += pDecoder.lfsStringToUnicode(row[1]) + '\t';
    ret += row[2] + '\t';
    ret += row[3];

    return ret;
}

const std::vector<QByteArray>
StintDataProvider::getRowImpl(const size_t pN,
                              const bool pColored,
                              const Translation::TranslationBinder &pTr) const {
    const auto &l = mDataRows[pN];

    QByteArray header =
        mNamedFields.find(pN) != std::end(mNamedFields)
            ? pTr.get(mNamedFields.at(pN))
            : insertStr(pTr.get(StringIds::LAP),
                        QByteArray::number(static_cast<qulonglong>(pN)));

    char color = '8'; // for lapnum and lapinfo;
    char lap_color = '8'; // for laptime
    QByteArray exclude_reason;
    QByteArray sections;


    if (l.exclude == StintInfo::Lap::ExcludeReason::LFS_INVALID) {
        color = '1';
        lap_color = '1';
        exclude_reason = "INVALID";
        stb_print_scs(l);
    } else {
        if (l.exclude == StintInfo::Lap::ExcludeReason::NONE) {
            lap_color = (mStintInfo->cleanBest == pN
                         && mStintInfo->cleanLapsNum > 1)
                        ? '6' : '7';
            color = '7';

        } else {
            color = '8';
            lap_color = (mStintInfo->noncleanBest == pN
                     && mStintInfo->noncleanLapsNum > 1)
                    ? '4' : '8';
            exclude_reason = StintInfo::Lap::excludeReasonToString(l.exclude,
                                                                   pTr);
        }

    }

    QByteArray color_ = "^";
    color_.append(color);
    QByteArray lap_color_ = "^";
    lap_color_.append(lap_color);

    if (pColored) {
        if (mLapsToHighlight.count(pN)) {
            sections = stb_print_scs(l, mLapsToHighlight.at(pN), color_);;
        } else {
            sections = stb_print_scs(l);
        }
    }
    else {
        sections = stb_print_scs(l, '\t');
    }

    QByteArray laptime = format_time(l.laptime);

    if (pColored) {
        return { header.prepend(color_),
                 exclude_reason,
                 sections.prepend(color_),
                 laptime.prepend(lap_color_) };
    } else {
        return { header, exclude_reason, sections, laptime };
    }

}
