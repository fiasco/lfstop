// Copyright 2016 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of LFSTop.
//
// LFSTop is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LFSTop is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LFSTop.  If not, see <http://www.gnu.org/licenses/>.


#ifndef LFSTOP_RECENT_STINTS_DATA_PROVIDER_HPP_INCLUDED
#define LFSTOP_RECENT_STINTS_DATA_PROVIDER_HPP_INCLUDED


#include "INonSwitchableInsimWindowDataProvider.hpp"


#include <vector>
#include <tuple>

#include <QByteArray>

#include "Translation.hpp"
#include "StintInfo.hpp"
#include "Databases/UsersDb.hpp"
#include "Databases/LapsDb.hpp"
#include "Common.hpp"
#include "Databases/StintsDb.hpp"


class LfsServer;

struct RunningStintInfoInfoRow {
    QByteArray username;
    CarT car;
    TyreT tyres;
    size_t numLaps;
};


class RecentStintsDataProvider final
    : public INonSwitchableInsimWindowDataProvider {
public:
    RecentStintsDataProvider (const UsersDb * const pUsersDb,
                              const std::shared_ptr<LapsDb> &pLapsDb,
                              const StintInfoList * const pStints,
                              LfsServer * const pLfsServer,
                              const uint8_t pUcid)
        : mUsersDb (pUsersDb),
          mLapsDb (pLapsDb),
          mStints (pStints),
          mLfsServer (pLfsServer),
          mUcid (pUcid) {
    }

    void prepareData () override;
    size_t getNumRows () const override {
        return mStints->size() + mRunningStints.size();
    }
    size_t getNumColumns () const override {
        return 5;
    }
    const std::vector<QByteArray> getRow (const size_t pN,
                                          const Translation::TranslationBinder &pTr) const override;
    const std::vector<InsimWindowColumnFormat> getColumnsFormat () const override {
        return { { InsimButton::left, 38 },
                 { InsimButton::center, 14 },
                 { InsimButton::center, 7 },
                 { InsimButton::right, 11 },
                 { InsimButton::center, 28 },
        };
    }
    bool isInteractive () override {
        return true;
    }
    virtual void rowClicked (const size_t pWindowRowNum,
                             const size_t pTableRowNum,
                             const unsigned short pMouseButtonClickFlags) override;

private:
    const UsersDb * const mUsersDb;
    const std::shared_ptr<LapsDb> &mLapsDb;

    const StintInfoList * const mStints;
    LfsServer * const mLfsServer;
    std::vector<RunningStintInfoInfoRow> mRunningStints;
    const uint8_t mUcid;
};


#endif // LFSTOP_RECENT_STINTS_DATA_PROVIDER_HPP_INCLUDED
