// Copyright 2015 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of LFSTop.
//
// LFSTop is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LFSTop is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LFSTop.  If not, see <http://www.gnu.org/licenses/>.


#ifndef LFSTOP_LFS_TOP_HPP_INCLUDED
#define LFSTOP_LFS_TOP_HPP_INCLUDED


#include <QObject>
#include <QSet>


#include "Databases/DatabasesManager.hpp"
#include "Databases/UsersDb.hpp"
#include "Utils/MKeyFile.hpp"
#include "Settings.hpp"
#include "Translation.hpp"


class LfsServer;


class LFSTop : public QObject
{
    Q_OBJECT
public:
    LFSTop (Log &pLog,
            LogDestination &pServerAdditionalLogDestination);

    qint32 getNumServers () {
        return mServers.size();
    }

    qint64 getReceivedBytes ();
    qint64 getSentBytes ();

	bool allFinishedSuccessfuly () const {
		return mAllFinishedSuccessfully;
	}

    void setRestartPossible (bool pIsRestartPossible) {
        mIsRestartPossible = pIsRestartPossible;
    }

    bool getIsRestartRequested () {
        return mIsRestartRequested;
    }

public slots:
    void run ();
    void shutdownRequested ();
    void instanceFinished (const QByteArray &pInstanceName, const bool pSuccessfully);
    void serverThreadFinished ();
    void restartRequested ();

signals:
    void shutdownAll ();
    void finished ();

private:
    void invokeServer (const MKeyFile &pConfigFile,
                       const QByteArray &pInstanceName);

    QSet<LfsServer*> mServers;
    QSet<QByteArray> mInstanceNames;
    QSet<QThread*> mServerThreadPtrs;

    MKeyFile mConfigFile;
    Settings::SettingsMap mSettingsMap;

    Log &mLog;
    LogDestination &mServerLogDestination;

    UsersDb mUsersDb;

    LfsDatabasesManager mDbManager;

    LogFile mPerformanceLogFile;
    Log mPerformanceLog;

    Translation::Translation mTr;

    bool mAllFinishedSuccessfully = true;
    bool mIsRestartRequested = false;
    bool mIsRestartPossible = false;
};

#endif // LFSTOP_LFS_TOP_HPP_INCLUDED
