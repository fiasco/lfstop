// Copyright 2015 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of LFSTop.
//
// LFSTop is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LFSTop is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LFSTop.  If not, see <http://www.gnu.org/licenses/>.


#include "LfsTopVersion.hpp"
#include "LfsServer.hpp"
#include "LfsInsimUtils.hpp"
#include "InsimPackets/InsimInit.hpp"
#include "InsimPackets/InsimTiny.hpp"
#include "InsimPackets/InsimMessage.hpp"
#include "InsimPackets/InsimButton.hpp"
#include "InsimPackets/InsimButtonFunction.hpp"
#include "InsimPackets/InsimSmall.hpp"
#include "InsimPackets/InsimJoinRequestResponse.hpp"

#include "LfsInsim.hpp"
#include "Utils/MathUtils.hpp"

#include <cassert>
#include <cstdio>
#include <algorithm>

#include <QDateTime>
#include <QFile>
#include <QThread>
#include <QElapsedTimer>

#include "Utils/StringUtils.hpp"
#include "Common.hpp"


using Translation::StringIds;
using LfsInsimUtils::insertStr;
using LfsInsimUtils::format;
using LfsInsimUtils::FormatPair;
using LfsInsimUtils::coloriseStr;


LfsServer::LfsServer (LFSTop* pLfsTop,
                      const LfsServerConfig pCfg,
                      LogDestination &pLogDestination,
                      Log &pPerformanceLog,
                      const QByteArray &pInstanceName,
                      LfsDatabasesManager *pDbManager,
                      UsersDb *pUsersDb,
                      const Translation::Translation &pTr)
    : mInstanceName (pInstanceName),
      mCfg (pCfg),
      mCarsConfig(pCfg.carsConfig),
      mLogFile ("logs/" + pInstanceName + ".log"),
      mLog (pInstanceName,
            pLogDestination, "%datetime %level [%name] %msg",
            mLogFile, "%datetime %level %msg"),
      mChatLogFile ("logs/" + pInstanceName + "_chat.log"),
      mChatLog (pInstanceName + "_chat", mChatLogFile, "%datetime %msg"),
      mInsimLogFile ("logs/" + pInstanceName + "_insim.log"),
      mInsimLog (pInstanceName + "_insim", mInsimLogFile, "%datetime %msg"),
      mPerformanceLog (pPerformanceLog),
      mTr (pTr),
      mDbManager (pDbManager),
      mUsersDb (pUsersDb),
      mHelpDataProvider(mCustomInfoDataProviders, mCfg.isRestartPossible),
      mHelpDataProviderAdmin(mCustomInfoDataProviders, mCfg.isRestartPossible),
      mPlayersDataProvider(&mConns),
      mLogDataProvider(pCfg.logLimit),
      mVersionDataProvider (this, pLfsTop, mTr),
      mStintsDb ("stints/" + mInstanceName, mLog) {
    mRecentDriversDataProvider.setUsersDb(mUsersDb);
    mTrackVoteTimerSecondsCounter.setSingleShot(true);
    mTrackVoteTimerSecondsCounter.setInterval(1000);
    connect(&mTrackVoteTimerSecondsCounter, SIGNAL(timeout()),
            this, SLOT(trackVoteTimerSecondsTimerTimeOut()));
    mTrackVoteTimerSecondsCounter.setParent(this);

    mTrackVoteTimer.setSingleShot(true);
    mTrackVoteTimer.setInterval(1000 * mCfg.trackVoteTime);
    connect(&mTrackVoteTimer, SIGNAL(timeout()),
            this, SLOT(trackVoteTimerTimeOut()));
    mTrackVoteTimer.setParent(this);

    mUdpReceivalCheckTimer.setParent(this);
    mUdpReceivalCheckTimer.setInterval(1000);
    mUdpReceivalCheckTimer.setSingleShot(true);
    connect(&mUdpReceivalCheckTimer, SIGNAL(timeout()),
            this, SLOT(udpReceivalCheckTimeOut()));

    mRecentDriversDataProvider.setLimit(mCfg.logLimit);
    mHelpDataProvider.setRecentAdminsOnly(mCfg.recentDriversAdminsOnly);
    mHelpDataProvider.setLogAdminsOnly(mCfg.logAdminsOnly);
    mHelpDataProvider.setIsAdmin(false);

    mHelpDataProviderAdmin.setRecentAdminsOnly(mCfg.recentDriversAdminsOnly);
    mHelpDataProviderAdmin.setLogAdminsOnly(mCfg.logAdminsOnly);
    mHelpDataProviderAdmin.setIsAdmin(true);

    for (const auto &el : mCfg.customCommands) {
        mCustomInfoDataProviders.emplace(
            el.first,
            CustomInfoDataProvider(QByteArray::fromStdString(el.second)));
    }
}

LfsServer::~LfsServer () {

}

void LfsServer::loadDatabase () {
    for (auto &conn : mConns) {
        conn.second.window.clearWindow();
    }
    if (!mLoadedTrackName.isEmpty()) {
        mDbManager->freeDatabase(mLoadedTrackName.toStdString());
    }

    const QByteArray track_to_load =
            (mTrackConfig.size() ? mTrackConfig : mTrack);

    mLog() << "loading track database: " << track_to_load;

    mDb = mDbManager->getDatabase(track_to_load.toStdString());

    mLoadedTrackName = track_to_load;

    mCarsDataProvider.setLapsDb(mDb);
}

void LfsServer::sendNewlapMessage (const ConnectionInfo &pConn,
                                   const LapsDbAddAnswer &pAnswer,
                                   const UserSettings::LaptimeUpdates::Types pType,
                                   const LapsDbStorage::Laptime &pLap,
                                   const ConnectionInfo &pDriverConn) {

    const auto type = UserSettings::LaptimeUpdates::typeToStr(pType);

    const QByteArray diff =
            (pAnswer.difference != 0
            ? ("^8 (-" + format_time(pAnswer.difference) + ")")
            : "");

    QByteArray tyre_lap;
    QByteArray best_lap;
    if (pAnswer.isBestLap) {
        best_lap = " ^8(" + pDriverConn.currentTyres.toString() + ")";
    } else {
        tyre_lap = "^8" + pDriverConn.currentTyres.toString() + "^7 ";
    }

    mSock->write(InsimMessage(
        format(pConn.tr->get(StringIds::NEW_LAP_BY),
            FormatPair{ "%laptime", format_time(pLap.laptime) + diff},
            FormatPair{ "%player", mUsersDb->userGetFullName(pLap.username) },
            FormatPair{"%type", tyre_lap + type },
            FormatPair{"%car", mDb->getCarName(pDriverConn.currentCar) })
        + ' ' + best_lap,
        pConn.id));
}


void
LfsServer::sendNewlapMessages (const LapsDbAddAnswer &pAnswer,
                               const LapsDbStorage::Laptime &pLap,
                               const ConnectionInfo &pConn,
                               const UserSettings::LaptimeUpdates::Types pType) {
    for (const auto &el : mConns) {
        const unsigned char id = el.first;
        const ConnectionInfo &ci = el.second;

        if (id == 0) {
            continue;
        }
        if (id == pConn.id) {
            if (ci.userSettings->onTrackSelf[pType]) { // self packet
                sendNewlapMessage(ci, pAnswer, pType, pLap, pConn);
            }
        }
        else if (mDrivers.count(id)) {
            if (ci.userSettings->onTrackOthers[pType]) { // on track others
                sendNewlapMessage(ci, pAnswer, pType, pLap, pConn);
            }
        }
        else if (ci.userSettings->offTrack[pType]) { // off track others
            sendNewlapMessage(ci, pAnswer, pType, pLap, pConn);
        }
    }
}

bool LfsServer::newLap (const QByteArray &pDbName,
                        const LapsDbStorage::Laptime &pLap,
                        const bool pClean,
                        const ConnectionInfo &pConn,
                        const bool pSendMessage) {
    const auto answer = mDb->addLaptime (pDbName,
                                         pConn.currentCar,
                                         pConn.currentTyres,
                                         pLap,
                                         pClean);
    if (answer.isAdded && pSendMessage) {
        QByteArray dbname = "";
        UserSettings::LaptimeUpdates::Types type;
        if (pDbName == "top") {
            type = UserSettings::LaptimeUpdates::Types::PB;
        }  else if (pDbName == "tbs") {
            type = UserSettings::LaptimeUpdates::Types::TBS;
        } else if (pDbName == "tb") {
            type = UserSettings::LaptimeUpdates::Types::TB;
        } else if (pDbName == "avg") {
            type = UserSettings::LaptimeUpdates::Types::AVG;
        } else {
            return false;
        }

        if (!mIsQuiet) {
            sendNewlapMessages(answer, pLap, pConn, type);
        }
    }

    return answer.isAdded;
}


void LfsServer::dataReceived () {
    char buf[256];

    while (true) {
        unsigned bytes = mSock->bytesAvailable();
        mSock->read((char*) buf, 1);
        unsigned char len = (unsigned char) buf[0];
        if (len > bytes) {
            mSock->ungetChar(buf[0]);
            if (mCfg.debugInsimPackets) {
                mInsimLog() << "skipping unfinished packet ("
                            << len
                            << " bytes length, received "
                            << bytes
                            << ")";
            }
            break;
        }
        mSock->read((char*) buf + 1, len - 1);
        unsigned char* cur = (unsigned char*) buf;

        mRecentPackets.push_back(cur, len);

        mReceivedBytes += len;

        using LfsInsimUtils::getPacketName;
        if (mCfg.debugInsimPackets) {
            mInsimLog() << getPacketName(cur, true)
                        << " (" << unsigned(len) << " bytes)";
        }
        // keep alive
        if (cur[1] == 3 && cur[3] == 0) {
            mSock->write(InsimTiny(TINY_NONE));
        } else {
            this->gotPacket(cur);
        }
        if (len == bytes) {
            break;
        }
    }
}



void LfsServer::udpDataReceived () {
    while (mUdpSock->hasPendingDatagrams()) {
        QByteArray datagram;
        datagram.resize(mUdpSock->pendingDatagramSize());
        QHostAddress data_sender;
        quint16 sender_port;

        mUdpSock->readDatagram(datagram.data(),
                               datagram.size(),
                               &data_sender,
                               &sender_port);

        const IS_MCI * const data = (IS_MCI*) datagram.constData();

        mReceivedBytes += datagram.size();

        if (data->Size != datagram.size()) {
            mLog(Log::WARNING) << "Incomplete or damaged UDP packet received, "
                                  "ignoring";
        } else {
            for (byte i = 0; i < data->NumC; ++i) {
                mCarPositions[data->Info[i].PLID] = data->Info[i];
                collectConnectionQualityData(data->Info[i].PLID,
                                             data->Info[i].Info & CCI_LAG);
            }

            mUdpReceivalCheckTimer.stop();

            checkDraft();
        }
    }

}

void LfsServer::shutdown () {
    for (auto &ci : mConns) {
        mSock->write(InsimMessage("^7LFSTop^8 "
                                  + ci.second.tr->get(StringIds::SHUTDOWN),
                                  ci.first));
    }

    // next 2 lines are needed because otherwise LFS detects
    // disconnect sooner than it reads the packets we send
    mSock->waitForBytesWritten(-1);
    QThread::sleep(1);

    mSock->disconnectFromHost();
    mUdpSock->close();
}

bool LfsServer::clarifyTopQueryCar (LapsdbQuery &pQuery,
                                    const QByteArray &pString) {
    const CarT car = mDb->getCarByName(pString);
    if (car != CarT::unknownCar()) {
        pQuery.setCar(car);
        return true;
    }
    return false;
}

bool LfsServer::clarifyTopQueryClass (LapsdbQuery &pQuery,
                                      const QByteArray &pString) {
    const ClassT cl = mDb->getClassByName(pString);
    if (cl != ClassT::notFound()) {
        pQuery.setClass(cl);
        return true;
    }

    return false;
}


// returns if query was clarified
bool LfsServer::clarifyTopQuery (LapsdbQuery &pQuery,
                                 const QByteArray &pString) {
    if (clarifyTopQueryCar(pQuery, pString)) {
        return true;
    } else if (clarify_top_query_tyre(pQuery.tyres, pString)) {
        return true;
    } else if (clarifyTopQueryClass(pQuery, pString)) {
        return true;
    }
    return false;
}



void LfsServer::initializeDefaultQuery (LapsdbQuery &pQuery,
                                        const ConnectionInfo &pConn,
                                        const QByteArray &pDbName) {
    if (pConn.lastDrivenCar != CarT::unknownCar()) {
        ClassT most_used_class = ClassT::notFound();
        int class_laptimes_num = -1;
        for (const auto cl : mClassesAllowed) {
            if (cl.second.contains(pConn.lastDrivenCar)) {
                const int num =
                    mDb->getClassLaptimesCount(pDbName,
                                               cl.first,
                                               pQuery.tyres,
                                               false);
                if (num > class_laptimes_num) {
                    most_used_class = cl.first;
                    class_laptimes_num = num;
                }
            }
        }
        if (most_used_class != ClassT::notFound()) {
            pQuery.setClass(most_used_class);
            return;
        }

        pQuery.setCar(pConn.lastDrivenCar);
        return;
    }

    const ClassT most_used_class =
        getMostUsedFromAllowedClasses(pDbName, pQuery.tyres);

    if (most_used_class != ClassT::notFound()) {
        pQuery.carsClass = most_used_class;
        return;
    }

    pQuery.car =
        getMostUsedFromAllowedCars(pDbName, pQuery.tyres);
    assert(pQuery.car != CarT::unknownCar());
}

void LfsServer::initializeDefaultCarQuery (LapsdbQuery &pQuery,
                                           const ConnectionInfo &pConn,
                                           const QByteArray &pDbName) {
    if (pConn.lastDrivenCar != CarT::unknownCar()) {
        pQuery.car = pConn.lastDrivenCar;
        return;
    }

    const CarT most_used_car =
        getMostUsedFromAllowedCars(pDbName, pQuery.tyres);
    if (most_used_car != CarT::unknownCar()) {
        pQuery.car = most_used_car;
        return;
    }
    assert(false);
}

void LfsServer::processLogRequested (ConnectionInfo &pConn) {
    const size_t num_rows = mLogDataProvider.getNumRows();
    const size_t min_rewind = std::min(mLogDataProvider.getNumRows(), size_t(20));
    pConn.window.setStartFrom(num_rows - min_rewind);
    pConn.window.setHeader(insertStr("^7LFSTop ^8%s:",
                                     pConn.tr->get(StringIds::CHAT_LOGS)));
    pConn.window.setDataProvider(&mLogDataProvider);
    pConn.windowManager.openWindow(&pConn.window);
}

void LfsServer::processPlayersRequested (ConnectionInfo &pConn) {
    pConn.window.setStartFrom(0);
    pConn.window.setHeader(insertStr("^7LFSTop ^8%s:",
                                     pConn.tr->get(StringIds::PLAYERS_INFO)));
    pConn.window.setDataProvider(&mPlayersDataProvider);
    pConn.windowManager.openWindow(&pConn.window);
}

void LfsServer::processUserCommand (ConnectionInfo &pConn,
                                    const std::vector<QByteArray> &pCommand) {




    if (pCommand[0] == "top") {
        processDbQuery(pConn,
                       pCommand.cbegin() + 1,
                       pCommand.cend(),
                       "top",
                       pConn.tr->get(StringIds::LAPSDB_QUERY_TOP_LAPS_HEADER));
    } else if (pCommand[0] == "tbs" && pCommand.size() <= 3) {
        processDbQuery(pConn,
                       pCommand.cbegin() + 1,
                       pCommand.end(),
                       "tbs",
                       pConn.tr->get(StringIds::LAPSDB_QUERY_TBS_LAPS_HEADER));
    } else if (pCommand[0] == "tb" && pCommand.size() <= 3) {
        processDbQuery(pConn,
                       pCommand.cbegin() + 1,
                       pCommand.end(),
                       "tb",
                       pConn.tr->get(StringIds::LAPSDB_QUERY_TB_LAPS_HEADER));
    } else if (pCommand[0] == "avg" && pCommand.size() <= 3) {
        processDbQuery(pConn,
                       pCommand.cbegin() + 1,
                       pCommand.end(),
                       "avg",
                       pConn.tr->get(StringIds::LAPSDB_QUERY_AVG_LAPS_HEADER));
    } else if (pCommand[0] == "pb") {
        processPbRequested(pConn, pCommand.begin() + 1, pCommand.end());
    } else if (pCommand[0] == "stint") {
        processStintRequested(pConn, pCommand);
    } else if (pCommand[0] == "help") {
        processHelpRequested(pConn);
    } else if (pCommand[0] == "opt") {
        pConn.windowManager.openWindow(&pConn.settingsWindow);
    } else if (pCommand[0] == "tr") {
        mSock->write(InsimMessage(mTrackConfig.size() ? mTrackConfig : mTrack,
                                  pConn.id));
    } else if (pCommand[0] == "cars"){
        pConn.window.setStartFrom(0);
        pConn.window.setHeader(
            insertStr("^7LFSTop ^8%s:",
                      pConn.tr->get(StringIds::DEFINED_CARS_AND_CLASSES)));
        pConn.window.setDataProvider(&mCarsDataProvider);
        pConn.windowManager.openWindow(&pConn.window);
    } else if (pCommand[0] == "recent"){
        processRecentRequested(pConn);
    } else if (pCommand[0] == "ver"){
        pConn.window.setStartFrom(0);
        pConn.window.setHeader(
            insertStr("^7LFSTop ^8%s:",
                      pConn.tr->get(StringIds::VERSION_INFO_AND_SOME_STATS)));
        mVersionDataProvider.setLanguage(pConn.userSettings->getLanguage());
        pConn.window.setDataProvider(&mVersionDataProvider);
        pConn.windowManager.openWindow(&pConn.window);
    } else if (pCommand[0] == "log") {
        processLogRequested(pConn);
    } else if (pCommand[0] == "pl") {
        processPlayersRequested(pConn);
    } else if (mCustomInfoDataProviders.count(pCommand[0].toStdString())) {
        auto & dp = mCustomInfoDataProviders.at(pCommand[0].toStdString());
        pConn.window.setDataProvider(&dp);
        pConn.window.setHeader(dp.getHeader());
        pConn.windowManager.openWindow(&pConn.window);
    } else if (pCommand[0] == "stints") {
        processStintsRequested(pConn);
    } else {
        mSock->write(InsimMessage(
            "^7LFSTop^8: " +
            format(mConns[pConn.id].tr->get(StringIds::COMMAND_NOT_FOUND),
                   FormatPair { "%command", "^7" + pCommand[0] + "^8" },

                   FormatPair { "%help_command",
                                "^7" +
                                QByteArray::fromStdString(
                                    std::string("help").insert(0,
                                                               1,
                                                               mCfg.commandPrefix))
                                + "^8"}),
            pConn.id));
    }
}

void LfsServer::processUserCommand (const uint8_t pUcid,
                                    const std::vector<QByteArray> &pCommand) {
    if (mIsQuiet && !vector_contains(mAdminUcids, pUcid)) {
        return;
    }

    if (mTrackLoadingIsInProcess
        || mVoteDispatcher.currentMode != TrackVoteDispatcher::Mode::CLOSED) {
        mSock->write(
            InsimMessage(mConns[pUcid].tr->get(StringIds::WAIT_FOR_TRACK_VOTE),
                         pUcid));
        return;
    }

    processUserCommand(mConns[pUcid], pCommand);
}

void LfsServer::processDbQuery (ConnectionInfo &pConn,
                                std::vector<QByteArray>::const_iterator pParamsIt,
                                const std::vector<QByteArray>::const_iterator &pParamsEnd,
                                const QByteArray &pDbName,
                                const QByteArray &pHeaderFormat) {
    LapsdbQuery query;

    query.tyres = TyreT::unknownTyres();

    std::vector<QByteArray> non_query_params;

    using StringUtils::allcapsed;


    for (;pParamsIt != pParamsEnd; ++pParamsIt) {
        const auto param = *pParamsIt;
        if (!clarifyTopQuery(query, param.toUpper())) {
            non_query_params.push_back(param);
        }
    }

    QByteArray error_msg;
    QByteArray hint;

    if (non_query_params.size() > 0) {
        if (non_query_params.size() > 1) {
            error_msg +=
                insertStr(pConn.tr->get(StringIds::QUERY_PARAMETERS_IGNORED),
                          vector_join(non_query_params, ' '));
        }
        hint = non_query_params[0];
    }

    if (query.car == CarT::unknownCar()
        && query.carsClass == ClassT::notFound()) {
        // TODO consider initializing with a whole class
        initializeDefaultQuery(query, pConn, "top");
    }

    if (pConn.mTopDataProvider.getNumRecordsByQuery(pDbName, query) > 0) {
        pConn.query = query;

        QByteArray buff =
            format(pHeaderFormat,
                   FormatPair { "%car_or_class",
                           coloriseStr(
                               query.carsClass == ClassT::notFound()
                               ? mDb->getCarName(query.car)
                               : mDb->getClassName(query.carsClass),
                               '7') },
                   FormatPair{"%track", coloriseStr(mLoadedTrackName, '7') },
                   FormatPair{"%with_some_tyres",
                           query.tyres == TyreT::unknownTyres()
                           ? ""
                           : insertStr(pConn.tr->get(StringIds::WITH_SOME_TYRES),
                                       coloriseStr(query.tyres.toString(), '7')) });


        QElapsedTimer timer;
        timer.start();

        pConn.mTopDataProvider.setDbName(pDbName);

        bool is_query_success = false;
        bool is_query_clean = false;

        std::tie(is_query_success, is_query_clean) =
            pConn.mTopDataProvider.setQuery(query);

        size_t start_from = 0;
        size_t highlight_row = std::numeric_limits<size_t>::max();
        if (hint.size()) {
            bool is_number;
            size_t n = hint.toULong(&is_number);
            if (is_number && n > 0) {
                start_from = n - 1;
            } else {
                if (hint == "me") {
                    hint = pConn.username;
                }
                size_t un;
                const int _un =
                    mDb->findUserLaptimeNumByQuery(pDbName,
                                                   hint,
                                                   query,
                                                   is_query_clean);
                if (_un != LapsDbStorage::not_found) {
                    un = static_cast<size_t>(_un);
                    start_from = un - 10;
                    highlight_row = un;
                }
            }
        }

        const size_t total_rows_num = pConn.mTopDataProvider.getNumRows();
        if (start_from > total_rows_num - 20) {
            start_from = total_rows_num - 20;
        }

        pConn.window.setStartFrom(start_from);
        pConn.window.setHighlightRow(highlight_row);

        pConn.window.setHeader(buff);
        pConn.window.setDataProvider(&pConn.mTopDataProvider);
        pConn.windowManager.openWindow(&pConn.window);


        const qint64 millisecs = timer.nsecsElapsed();

        if (mCfg.performanceLoggingEnabled) {
            mPerformanceLog()
                    << "Database request took " + QByteArray::number(millisecs)
                    << " ("
                    << mDb->getLaptimesCountByQuery(pDbName, query)
                    << " laptimes available)";
        }
    } else {
        const QByteArray s =
            format(pConn.tr->get(StringIds::NO_DATA_FOR_REQUEST),
                   FormatPair{ "%table_type", "^7" + pDbName.toUpper() + "^8" },
                   FormatPair{ "%car_track_tyres",
                           mDb->queryToString(pDbName,
                                              query,
                                              *pConn.tr.get())});
        mSock->write(InsimMessage(s, pConn.id));
    }
    if (error_msg.size()) {
        mSock->write(InsimMessage(error_msg, pConn.id));
    }
}

void LfsServer::processPbRequested (ConnectionInfo &pConn,
                                    std::vector<QByteArray>::const_iterator pParamsIt,
                                    const std::vector<QByteArray>::const_iterator &pParamsEnd) {
    LapsdbQuery query;


    QByteArray requested_username;

    QByteArray error_msg;

    using StringUtils::allcapsed;

    std::vector<QByteArray> non_query_params;
    for (;pParamsIt != pParamsEnd; ++pParamsIt) {
        const auto param = *pParamsIt;
        if (!clarifyTopQuery(query, param.toUpper())) {
            non_query_params.push_back(param);
        }
    }

    if (non_query_params.size() > 0) {
        if (non_query_params.size() > 1) {
            error_msg +=
                insertStr(pConn.tr->get(StringIds::QUERY_PARAMETERS_IGNORED),
                          vector_join(non_query_params, ' '));
        }
        const QByteArray usr =
                mUsersDb->findUsernameByString(non_query_params[0]);
        if (usr.size()) {
            requested_username = usr;
        } else {
            error_msg +=
                error_msg.size() ? " " : "" +
                insertStr(pConn.tr->get(StringIds::NOT_FOUND),
                          non_query_params[0]);
        }
    } else {
        requested_username = pConn.username;
    }



    if (query.car == CarT::unknownCar()) {
        initializeDefaultCarQuery(query, pConn, "top");
    }


    if (requested_username.size()) {
        const QByteArray header_ =
                mUsersDb->userGetFullName(requested_username)
                + " ^7" + mDb->getCarName(query.car)
                + (query.tyres == TyreT::unknownTyres()
                   ? ""
                   : " " + query.tyres.toString())
                + "^8 %s";

        QByteArray header = insertStr(header_, pConn.tr->get(StringIds::LAPS));


        PbDataProvider &dp = pConn.mPbDataProvider;
        SwitchableInsimWindow &w = pConn.window;

        dp.setQuery(query);
        dp.setUsername(requested_username);
        if (dp.getNumRows() > 0) {
            w.setStartFrom(0);
            w.setHeader(header);
            w.setDataProvider(&dp);
            pConn.windowManager.openWindow(&w);

        } else {
            QByteArray s =
                format(
                    pConn.tr->get(StringIds::NO_DATA_FOR_PB_REQUEST),
                    FormatPair{ "%name",
                                mUsersDb->userGetFullName(requested_username) },
                    FormatPair{ "%car_track_tyres",
                                mDb->queryToString("top",
                                                   pConn.query,
                                                   *pConn.tr.get())});

            mSock->write(InsimMessage(s, pConn.id));
        }
    } else {
        mSock->write(
            InsimMessage(pConn.tr->get(StringIds::INCORRECT_REQUEST_FORMAT),
                         pConn.id));
    }
    if (error_msg.size()) {
        mSock->write(InsimMessage(error_msg, pConn.id));
    }
}




quint8 LfsServer::findUcidByString (const QByteArray &pString) const {
    using LfsInsimUtils::stringNonEscapedNonColored;

    for (const auto &el : mConns) {
        if (stringNonEscapedNonColored(el.second.nickname).toUpper()
                .contains(stringNonEscapedNonColored(pString).toUpper())
            || el.second.username.toUpper()
                .contains(stringNonEscapedNonColored(pString).toUpper())) {
            return el.first;
        }
    }
    return 0;
}

void LfsServer::gotAutoXClearedPacket () {
    mTrackConfig = "";
    if (mLoadedTrackName != mTrack) {
        loadDatabase();
    }
}

void LfsServer::gotRaceEndPacket () {
    if (mCfg.trackVoteEnable) {
        mVoteDispatcher.currentMode = TrackVoteDispatcher::Mode::SITE;
        mVoteDispatcher.prepareData();
        mTrackVoteTimer.start();
        mTrackVoteTimerSecondsCounter.start();

        IS_MST comm = { 68, ISP_MST, 0, 0, "/clear" };
        mSock->writeBytes((const char*) &comm, 68);

        for (auto &el : mConns) {
            auto &ci = el.second;
            ci.trackVoteDataProvider.startVote();
            ci.window.setDataProvider(&ci.trackVoteDataProvider);
            ci.window.setHeader(ci.tr->get(StringIds::VOTE_FOR_AUTODROME));
            ci.windowManager.openWindow(&ci.window);
        }
    }
}

void LfsServer::gotVoteNotifyPacket (const IS_VTN * const pPacket) {
    if (pPacket->UCID == 0) {
        // the vote notify is an indication of admin command
        return;
    }
    if (mRaceLaps == 0) { // means practice mode
        auto &ci = mConns[pPacket->UCID];
        if (ci.currentVote != pPacket->Action) {
            ci.currentVote = pPacket->Action;
        } else {
            ci.currentVote = VOTE_NONE;
            mSock->write(
                InsimMessage("^7LFSTop: ^8"
                             + ci.tr->get(StringIds::VOTE_CANCELLED),
                             ci.id));
        }

        size_t num_votes = 0;
        for (const auto &cii : mConns) {
            if (pPacket->Action == cii.second.currentVote) {
                ++num_votes;
            }
        }
        if (num_votes < mConns.size() - 1) { // - 1 coz host
            mSock->write(InsimTiny(TINY_VTC, 2));
            mChatLog() << "sent vote cancel packet";

            mSock->write(
                InsimMessage("^7LFSTop: ^8"
                             + ci.tr->get(StringIds::VOTE_UNANIMOUS_REQUIRED),
                             ci.id));

        } else {
            for (auto &cii : mConns) {
                cii.second.currentVote = VOTE_NONE;
            }
        }
    }
}

void LfsServer::trackVoteTimerTimeOut () {
    mTrackVoteTimerSecondsCounter.stop();
    InsimButtonFunction bc (BFN_DEL_BTN, 255, 239);
    mSock->write(bc);

    if (mVoteDispatcher.currentMode == TrackVoteDispatcher::Mode::SITE) {
        if (mVoteDispatcher.totalVotesCast == 0) {
            for (auto &el : mConns) {
                auto &ci = el.second;
                ci.window.clearWindow();
                mSock->write(
                    InsimMessage(ci.tr->get(StringIds::VOTING_CANCELLED),
                                 el.first));
            }
            mVoteDispatcher.currentMode = TrackVoteDispatcher::Mode::CLOSED;

            return;
        }
        mVoteDispatcher.concludeVote();

        mVoteDispatcher.currentMode = TrackVoteDispatcher::Mode::TRACK;
        mVoteDispatcher.prepareData();
        mTrackVoteTimer.start();
        mTrackVoteTimerSecondsCounter.start();

        for (auto &el : mConns) {
            auto &ci = el.second;
            ci.trackVoteDataProvider.startVote();
            ci.window.setDataProvider(&ci.trackVoteDataProvider);
            ci.window.setHeader(ci.tr->get(StringIds::VOTE_FOR_TRACK));
            ci.windowManager.openWindow(&ci.window);
        }
    } else if (mVoteDispatcher.currentMode == TrackVoteDispatcher::Mode::TRACK) {
        mVoteDispatcher.concludeVote();

        mVotedTrackName =
            Tracks::lfsTracksSitesLists[static_cast<size_t>(mVoteDispatcher.selectedSite)]
                .second[static_cast<size_t>(mVoteDispatcher.selectedTrack)].shortName;

        for (auto &el : mConns) {
            auto &ci = el.second;
            ci.window.clearWindow();
            mSock->write(
                InsimMessage(insertStr(ci.tr->get(StringIds::VOTED_FOR),
                                       mVotedTrackName),
                             el.first));
        }



        if (mVotedTrackName != mLoadedTrackName) {
            QByteArray command = "/track " + mVotedTrackName;

            IS_MST comm = { 68, ISP_MST, 0, 0, {0} };
            qstrncpy(comm.Msg, command.constData(), 63);
            mSock->writeBytes((const char*) &comm, 68);
            mTrackLoadingIsInProcess = true;
        }

        mVoteDispatcher.currentMode = TrackVoteDispatcher::Mode::CLOSED;
    } else {
        mLog(Log::ERROR) << "Logical error: called Vote conclusion method "
                            "while voting was not running";
    }
}

void LfsServer::trackVoteTimerSecondsTimerTimeOut () {
    InsimButton btn (QByteArray::number(mTrackVoteTimer.remainingTime() / 1000),
                     1,
                     20,
                     100,
                     30,
                     20,
                     255,
                     239);


    mTrackVoteTimerSecondsCounter.start();
    mSock->write(btn);

    if (!mTrackVoteTimer.isActive()) {
        return;
    }
    if ((mTrackVoteTimer.interval() - mTrackVoteTimer.remainingTime()
         > 1000 * mCfg.trackVoteTimeToCancel)
        && mVoteDispatcher.currentMode == TrackVoteDispatcher::Mode::SITE
        && mVoteDispatcher.totalVotesCast == 0) {
        mTrackVoteTimer.stop();
        trackVoteTimerTimeOut();
        return;
    }

    for (auto &el : mConns) {
        auto &ci = el.second;
        for (const auto v : mVoteDispatcher.updatedVotes) {
            ci.window.updateRowColumnContent(v, mVoteDispatcher.currentPrintableFields[0].size() - 1);
        }
//        ci.windowManager.openWindow(&ci.window);
    }



    mVoteDispatcher.updatedVotes.clear();
}

void LfsServer::udpReceivalCheckTimeOut () {
    mLog(Log::WARNING) << "not receiving any udp data on port "
                       << mUdpSock->localPort();
}

void LfsServer::gotPacket (const unsigned char* pPacket) {
    switch (pPacket[1]) {
    case ISP_NCN:
        return gotNewConnectionPacket((IS_NCN*) pPacket);
    case ISP_NCI:
        return gotNewConnectionInfoPacket((IS_NCI*) pPacket);
    case ISP_CNL:
        return gotConnectionLeavePacket((IS_CNL*) pPacket);
    case ISP_NPL:
        return gotNewPlayerPacket((IS_NPL*) pPacket);
    case ISP_PFL:
        return gotPlayerFlagsPacket((IS_PFL*) pPacket);
    case ISP_PLL:
        return gotPlayerLeavePacket((IS_PLL*) pPacket);
    case ISP_PLP:
        return gotPlayerPitPacket((IS_PLP*) pPacket);
    case ISP_STA:
        return gotStatePackPacket((IS_STA*) pPacket);
    case ISP_CPR:
        return gotPlayerRenamePacket((IS_CPR*) pPacket);
    case ISP_VER:
        return gotInsimVersionPacket((IS_VER*) pPacket);
    case ISP_LAP:
        return gotLapPacket((IS_LAP*) pPacket);
    case ISP_SPX:
        return gotSplitPacket((IS_SPX*) pPacket);
    case ISP_HLV:
        return gotHotlapValidationPacket((IS_HLV*) pPacket);
    case ISP_CON:
        return gotCarsContactPacket((IS_CON*) pPacket);
    case ISP_OBH:
        return gotObjectHitPacket((IS_OBH*) pPacket);
    case ISP_RST:
        return gotRaceStatePacket((IS_RST*) pPacket);
    case ISP_MSO:
        return gotMessageOutPacket((IS_MSO*) pPacket);
    case ISP_BTC:
        return gotButtonClickPacket((IS_BTC*) pPacket);
    case ISP_PLA:
        return gotPlayerPitlanePacket((IS_PLA*) pPacket);
    case ISP_CRS:
        return gotCarResetPacket((IS_CRS*) pPacket);
    case ISP_ACR:
        return gotAdminCommandReportPacket((IS_ACR*) pPacket);
    case ISP_AXI:
        return gotAutoXInfoPacket((IS_AXI*) pPacket);
    case ISP_TOC:
        return gotTakeOverPacket((IS_TOC*) pPacket);
    case ISP_BFN:
        return gotButtonFunctionPacket((IS_BFN*) pPacket);
    case ISP_VTN:
        return gotVoteNotifyPacket((IS_VTN*) pPacket);
    case ISP_TINY:
        switch (pPacket[3]) {
        case TINY_AXC:
            return gotAutoXClearedPacket();
        case TINY_REN:
            return gotRaceEndPacket();
        default:
            return;
        }
    case ISP_SMALL:
        switch(pPacket[3]) {
        case SMALL_ALC:
            return gatherLfsCarsSetting((IS_SMALL*) pPacket);
        }
    }
}

void LfsServer::gotNewConnectionPacket (const IS_NCN * const cn) {
//    Q_ASSERT(!mConns.count(cn->UCID));

    if (cn->UCID == 0) {
        //return;
    }

    mConns.emplace(std::make_pair(cn->UCID,
                   ConnectionInfo(mDb,
                                  mUsersDb,
                                  mSock,
                                  cn->UName,
                                  cn->PName,
                                  mTr,
                                  mCfg.dataExportEnable,
                                  mCfg.dataExportPath,
                                  mCfg.dataExportUrlPrefix,
                                  cn->UCID,
                                  &mStintsDb.stintsList,
                                  this,
                                  &mLog)));
    auto &conn = mConns[cn->UCID];

    mUsersDb->userSetNickname(cn->UName, cn->PName);
    conn.userSettings->load(mUsersDb->userGetSettings(cn->UName));

    conn.trackVoteDataProvider.setVoteDispatcher(&mVoteDispatcher);


    if (cn->Admin) {
        mAdminUcids.push_back(cn->UCID);
    }

    mRecentDriversDataProvider.connected(conn.username);

    std::istringstream iss(mCfg.welcomeMessage);
    for (std::string line; std::getline(iss, line); ) {
        mSock->write(InsimMessage(QByteArray::fromStdString(line), cn->UCID));
    }
}

void LfsServer::gotNewConnectionInfoPacket (const IS_NCI * const cn) {
    Q_ASSERT(mConns.count(cn->UCID));

    static std::unordered_map<int, QByteArray> langs_map {
        { LFS_ENGLISH, "ENG" },
        { LFS_DEUTSCH, "DEU" },
        { LFS_PORTUGUESE, "POR" },
        { LFS_FRENCH, "FRE" },
        { LFS_SUOMI, "FIN" },
        { LFS_NORSK, "NOR" },
        { LFS_NEDERLANDS, "DUT" },
        { LFS_CATALAN, "CAT" },
        { LFS_TURKISH, "TUR" },
        { LFS_CASTELLANO, "SPA" },
        { LFS_ITALIANO, "ITA" },
        { LFS_DANSK, "DAN" },
        { LFS_CZECH, "CZE" },
        { LFS_RUSSIAN, "RUS" },
        { LFS_ESTONIAN, "EST" },
        { LFS_SERBIAN, "SRP" },
        { LFS_GREEK, "GRE" },
        { LFS_POLSKI, "POL" },
        { LFS_CROATIAN, "HRV" },
        { LFS_HUNGARIAN, "HUN" },
        { LFS_BRAZILIAN, "BRA" },
        { LFS_SWEDISH, "SWE" },
        { LFS_SLOVAK, "SLO" },
        { LFS_GALEGO, "GLG" },
        { LFS_SLOVENSKI, "SLV" },
        { LFS_BELARUSSIAN, "BEL" },
        { LFS_LATVIAN, "LAV" },
        { LFS_LITHUANIAN, "LIT" },
        { LFS_TRADITIONAL_CHINESE, "CHL" },
        { LFS_SIMPLIFIED_CHINESE, "CHI" },
        { LFS_JAPANESE, "JPN" },
        { LFS_KOREAN, "KOR" },
        { LFS_BULGARIAN, "BUL" },
        { LFS_LATINO, "LAT" },
        { LFS_UKRAINIAN, "UKR" },
        { LFS_INDONESIAN, "IND" },
        { LFS_ROMANIAN, "RUM" }
    };

    auto &ci = mConns[cn->UCID];
    if (!vector_contains(mTr.getLanguages(),
                         ci.userSettings->getLanguage())) {
        if (langs_map.find(cn->Language) != langs_map.end()
              && vector_contains(mTr.getLanguages(), langs_map.at(cn->Language))) {
            ci.userSettings->setLanguage(langs_map.at(cn->Language));
        }
        else {
            ci.userSettings->setLanguage("?");
        }
    }

    ci.countryName = mDbManager->getIpLocation(cn->IPAddress);
}

void LfsServer::gotConnectionLeavePacket (const IS_CNL * const cn) {
    if (mConns.count(cn->UCID)) {
        mRecentDriversDataProvider.disconnected(mConns[cn->UCID].username,
                                                cn->Reason);

        saveStintToStintsDb(mConns[cn->UCID]);

        mUsersDb->userSetSettings(
                    mConns[cn->UCID].username,
                    mConns[cn->UCID].userSettings->getUserSettings());

        mConns.erase(cn->UCID);

        auto it = std::find(mAdminUcids.begin(), mAdminUcids.end(), cn->UCID);
        if (it != mAdminUcids.end()) {
            mAdminUcids.erase(it);
        }

        if (mConns.size() == 1) { // means only host left
            if (mVoteDispatcher.currentMode
                != TrackVoteDispatcher::Mode::CLOSED) {
                mVoteDispatcher.currentMode = TrackVoteDispatcher::Mode::CLOSED;
                mTrackVoteTimerSecondsCounter.stop();
                mTrackVoteTimer.stop();
                InsimButtonFunction bc (BFN_DEL_BTN, 255, 239);
                mSock->write(bc);
            }
        }
    } else {
        mLog(Log::ERROR) << "disconnected, yet we didn't know he "
                            "was online here";
    }
}

void LfsServer::spectatePlayer (const ConnectionInfo &pConn) {
    QByteArray comm("/spec ");
    comm += pConn.username;
    IS_MST spec_msg {68, ISP_MST, 0, 0, {} };
    assert(comm.size() < 64);
    memcpy(spec_msg.Msg, comm.constData(), (size_t) comm.size());
    mSock->writeBytes((const char*) &spec_msg, 68);
}

std::vector<LaptimeT> LfsServer::getTb (const ConnectionInfo &pConn,
                                        const bool pClean) {
    std::vector<LaptimeT> tb;

    LapsDbStorage::Laptime tb_laptime;

    try {
        tb_laptime = mDb->findUsernameLaptime("tb",
                                              pConn.username,
                                              pConn.currentCar,
                                              pConn.currentTyres,
                                              pClean);
    } catch (LapsDb::LapNotFoundException) {
        return {};
    }

    LaptimeT prev_split = 0;
    for (int i = 0; i < tb_laptime.splits.size(); ++i) {
        tb.push_back(tb_laptime.splits[i] - prev_split);
            prev_split = tb_laptime.splits[i];
    }
    tb.push_back(tb_laptime.laptime - prev_split);

    return tb;
}


MathUtils::Vector2 rotatePoint (const float pX,
                                const float pY,
                                const float pAngle) {
    MathUtils::Vector2 ret;

    ret.x = pX * std::cos(pAngle) + pY * std::sin(pAngle);
    ret.y = -pX * std::sin(pAngle) + pY * std::cos(pAngle);

    return ret;
}

void LfsServer::checkDraft (const CompCar &pCar1, const CompCar &pCar2) {
    using namespace MathUtils;

    constexpr float draft_angle_tolerance_degrees = 45.f;
    constexpr float draft_speed = 60.f;
    constexpr float draft_length = 30.f;
    constexpr float draft_width = 1.8f;
    constexpr qint64 draft_forgivable_duration = 2000;

    const float car1_direction =
            degreesToRadians(((float) pCar1.Direction) / 65536.f * 360);
    const float car2_direction =
            degreesToRadians(((float) pCar2.Direction) / 65536.f * 360);

    if (angleDistance2f(car1_direction, car2_direction)
            < degreesToRadians(draft_angle_tolerance_degrees)
        && distance2f(pCar1.X, pCar1.Y, pCar2.X, pCar2.Y)
            < ((draft_length + 0.1) * 65536.f)
        && (pCar2.Speed / 100.f) > kmPerHourToMetersPerSec(draft_speed)) {

        const Vector2 car1_local_pos =
                rotatePoint(pCar1.X, pCar1.Y, car2_direction);
        const Vector2 car2_local_pos =
                rotatePoint(pCar2.X, pCar2.Y, car2_direction);
        const Vector2 pos_diff = (car2_local_pos - car1_local_pos) / 65536.f;

        if (absf(pos_diff.x) < draft_width / 2
            && pos_diff.y > 0.f
            && pos_diff.y < draft_length) {
            const qint64 clock = QDateTime::currentMSecsSinceEpoch();
            if (mCarDraftTimestamps[pCar1.PLID] == 0) {
                mCarDraftTimestamps[pCar1.PLID] = clock;
            } else if (clock - mCarDraftTimestamps[pCar1.PLID]
                           > draft_forgivable_duration) {
                invalidatePlayerLap(pCar1.PLID,
                                    StintInfo::Lap::ExcludeReason::HLVC_DRAFT);
                mCarDraftTimestamps[pCar1.PLID] = 0;
            }
        }
    }
}

void LfsServer::checkDraft () {
    auto end = mCarPositions.end();
    for (auto it_i = mCarPositions.begin(); it_i != end; ++it_i) {
        auto it_j = it_i;
        while (++it_j != end) {
            const CompCar &car_1 = it_i->second;
            const CompCar &car_2 = it_j->second;

            if (car_1.Info & CCI_LAG || car_2.Info & CCI_LAG) {
                continue;
            }

            checkDraft(car_1, car_2);
            checkDraft(car_2, car_1);
        }
    }
}

void LfsServer::resetMciInterval () {
        mSock->write(InsimSmall(SMALL_NLI, 100, 12));
}

bool LfsServer::checkPlayerClutchSetup (const word pFlags) {
    if (pFlags & PIF_AUTOCLUTCH) {
        if (!(mCfg.clutchTypesAllowed & LfsServerConfigClutchTypes::Auto)) {
            return false;
        }
    } else if (pFlags & PIF_AXIS_CLUTCH) {
        if (!(mCfg.clutchTypesAllowed & LfsServerConfigClutchTypes::Axis)) {
            return false;
        }
    } else {
        if (!(mCfg.clutchTypesAllowed & LfsServerConfigClutchTypes::Button)) {
            return false;
        }
    }
    return true;
}

void LfsServer::collectConnectionQualityData (const unsigned char pPlid,
                                              const bool pIsLagging) {
    if (mDrivers.find(pPlid) == std::end(mDrivers)
        || mConns.find(mDrivers[pPlid]) == std::end(mConns)) {
        return;
    }
    auto &q_data = mConns[mDrivers[pPlid]].connectionQualityData;
    size_t i = 0;
    if (q_data.size() == 0) {
        q_data.push_back(ConnectionInfo::ConnectionQualityData());
    } else if (q_data.size() == 2) {
        i = 1;
    }

    auto &current_data = q_data[i];

    if (current_data.startTime.msecsTo(QDateTime::currentDateTime())
        > 1000 * 60 * 5) { // if older than 5 minutes
        q_data.push_back(ConnectionInfo::ConnectionQualityData());
    }

    if (!pIsLagging) {
        ++current_data.onlineQuantity;
    } else {
        ++current_data.laggingQuantity;
    }
}

QByteArray clutch_setting_to_string (const quint8 pClutchTypes,
                                     const Translation::TranslationBinder &pTr) {

    QByteArray ret;
    if (pClutchTypes & LfsServerConfigClutchTypes::Auto) {
        ret = pTr.get(StringIds::AUTO);
    }
    if (pClutchTypes & LfsServerConfigClutchTypes::Axis) {
        if (ret.size()) {
            ret += " | ";
        }
        ret += pTr.get(StringIds::AXIS);
    }
    if (pClutchTypes & LfsServerConfigClutchTypes::Button) {
        if (ret.size()) {
            ret += " | ";
        }
        ret += pTr.get(StringIds::BUTTON);
    }
    return ret;
}


bool LfsServer::checkPlayerFlags (std::vector<QByteArray> &pFixSetupMessages,
                                  const word pFlags,
                                  const byte pUcid) {
    auto &ci = mConns[pUcid];
    bool setup_is_ok = true;
    if (mCfg.forbidAutoGears && (pFlags & PIF_AUTOGEARS)) {
        setup_is_ok = false;
        if (!mIsQuiet) {
            pFixSetupMessages.push_back(
                ci.tr->get(StringIds::AUTOMATIC_GEAR_SHIFTING_FORBIDDEN));
        }
    }

    if(!checkPlayerClutchSetup(pFlags)) {
        setup_is_ok = false;
        if (!mIsQuiet) {
            pFixSetupMessages.push_back(
                insertStr(ci.tr->get(StringIds::SET_CLUTCH_TO),
                          clutch_setting_to_string(
                              mCfg.clutchTypesAllowed,
                              *ci.tr.get())));
        }
    }
    return setup_is_ok;
}

void LfsServer::gotNewPlayerPacket (const IS_NPL * const pl) {
    if (mDrivers.size() == 0 && mIsInRacingMode) {
        mUdpReceivalCheckTimer.start();
    }
    auto &ci = mConns[pl->UCID];
    bool is_join_request = (pl->NumP == 0);
    if (mTrackLoadingIsInProcess
        || mVoteDispatcher.currentMode != TrackVoteDispatcher::Mode::CLOSED) {
        if (is_join_request) {
            mSock->write(InsimJoinRequestResponse(0, pl->UCID, JRR_REJECT));
        } else {
            spectatePlayer(mConns[pl->UCID]);
        }
        mSock->write(
            InsimMessage(ci.tr->get(StringIds::WAIT_FOR_TRACK_VOTE),
                         pl->UCID));
    }

    if (is_join_request) {
        checkPlayerCarAndSetup (pl, is_join_request);
        return;
    }

    constexpr byte PTYPE_AI = 2;
    if (!(pl->PType & PTYPE_AI)) {
        if (!checkPlayerCarAndSetup (pl, is_join_request)) {
            return;
        }

        mDrivers[pl->PLID] = pl->UCID;

        mConns[pl->UCID].previousUcid = 0;

        mConns[pl->UCID].currentCar =
            mDb->getCarByCarDefinition({ mDb->getCarByName(pl->CName),
                                         pl->H_TRes,
                                         pl->H_Mass });

        mConns[pl->UCID].currentTyres = TyreT::fromArr(pl->Tyres);
        mConns[pl->UCID].stintInfo.trackName = mLoadedTrackName;



        if (mConns.count(pl->UCID)) {
            mConns[pl->UCID].lastDrivenCar = mConns[pl->UCID].currentCar;
            mConns[pl->UCID].currentLapNum = 0;
            mConns[pl->UCID].currentLap = StintInfo::Lap();

            if (pl->ReqI == 3) {
                // == 3 means we got it as response to initial request
                // of ISP_NPL, therefore if driver is on section 1 we
                // couldn't check if he violated the HLVC
                invalidatePlayerLap(
                    pl->PLID,
                    StintInfo::Lap::ExcludeReason::HLVC_NOT_CHECKED);
            }

            mConns[pl->UCID].tb[true] = getTb(mConns[pl->UCID]);
            mConns[pl->UCID].tb[false] = getTb(mConns[pl->UCID], false);

        } else {
            mLog(Log::ERROR) << "WARNING player joined on track but his "
                                "connection wasn't being tracked somehow";
        }

        if ((mConns[pl->UCID].currentCar.value
                >= CarT::customCarsOffset)
            && !mIsQuiet) {
            for (auto &cii : mConns) {
                mSock->write(
                    InsimMessage(
                        "^7LFSTop: ^8"
                        + format(cii.second.tr->get(StringIds::CUSTOM_CAR),
                                 FormatPair{"%driver", ci.nickname + "^8"},
                                 FormatPair{"%car", mDb->getCarName(ci.currentCar)}),
                        cii.first));
            }
        }
    }
}

bool LfsServer::checkPlayerCarAndSetup (const IS_NPL * const pl,
                                        const bool pIsProcessingJoinRequest) {
    auto &ci = mConns[pl->UCID];

    bool setup_is_ok = true;

    std::vector<QByteArray> fix_setup_messages;
    if (mCfg.forbidTc && (pl->SetF & SETF_TC_ENABLE)) {
        setup_is_ok = false;
        if (!mIsQuiet) {
            fix_setup_messages.push_back(
                        ci.tr->get(StringIds::DISABLE_TRACTION_CONTROL));
        }
    }

    if (!checkPlayerFlags(fix_setup_messages, pl->Flags, pl->UCID)) {
        setup_is_ok = false;
    }


    if (!setup_is_ok) {
        if (!mIsQuiet) {
            for (const QByteArray &mess: fix_setup_messages) {
                mSock->write(InsimMessage(mess, pl->UCID));
            }
        }
        if (pIsProcessingJoinRequest) {
            mSock->write(InsimJoinRequestResponse(0, pl->UCID, JRR_REJECT));
        } else {
            spectatePlayer(mConns[pl->UCID]);
        }
    }

    const CarT selected_car =
            mDb->getCarByCarDefinition({ mDb->getCarByName(pl->CName),
                                         pl->H_TRes,
                                         pl->H_Mass });

    if (mAmbiguousLfsCars.contains(mDb->getCarByName(pl->CName))) {
        // check if it matches any of allowed cars
        // and spectate the player if it doesn't

        if (!mCarsAllowed.contains(selected_car)) {
            QByteArray allowed_cars = "";
            for (const CarT c: mCarsAllowed) {
                allowed_cars += mDb->getCarName(c) + ' ';
            }
            if (allowed_cars.size()) {
                allowed_cars.remove(allowed_cars.size() - 1, 1);
            }

            mSock->write(
                InsimMessage(format(ci.tr->get(StringIds::SELECT_ONE_OF),
                                    FormatPair{"%s", allowed_cars}),
                pl->UCID));


            mSock->write(
                InsimMessage(
                    format(ci.tr->get(StringIds::TYPE_COMMAND_FOR_HELP),
                           FormatPair{"%s", QByteArray("^7")
                                            + mCfg.commandPrefix
                                            + QByteArray("cars^8")}),
                    pl->UCID));

            if (pIsProcessingJoinRequest) {
                mSock->write(InsimJoinRequestResponse(0, pl->UCID, JRR_REJECT));
            } else {
                spectatePlayer(mConns[pl->UCID]);
            }
            setup_is_ok = false;
        }
    }

    if (pIsProcessingJoinRequest) {
        mSock->write(InsimJoinRequestResponse(0, pl->UCID, JRR_SPAWN));
    }

    return setup_is_ok;
}

void LfsServer::saveStintToStintsDb (ConnectionInfo &pConn) {
    StintInfo &current_stint = pConn.stintInfo;
    if (current_stint.laps.size() >= mCfg.stintsMinLapsToSave) {
        mStintsDb.stintsList.push_back({ pConn.username,
                                         pConn.currentCar,
                                         pConn.currentTyres,
                                         current_stint,
                                         QDateTime::currentMSecsSinceEpoch() });

        if (mStintsDb.stintsList.size() > mCfg.numStintsToSave) {
            const auto diff =
                mStintsDb.stintsList.size() - mCfg.numStintsToSave;
            mStintsDb.stintsList.erase(mStintsDb.stintsList.begin(),
                                       std::next(mStintsDb.stintsList.begin(),
                                                 static_cast<ptrdiff_t>(diff)));
        }
    }


    pConn.stintInfo = StintInfo(mLoadedTrackName, mNumSplits + 1);
}

void LfsServer::saveData () {
    for (auto &el: mConns) {
        mUsersDb->userSetSettings(
                    el.second.username,
                    el.second.userSettings->getUserSettings());
        saveStintToStintsDb(el.second);
    }
}

void LfsServer::fillAllowedClasses () {
    mClassesAllowed.clear();

    auto classes = mDb->getClasses();

    auto is_sufficient_class = [] (const QSet<CarT> &class_cars,
                                   const QSet<CarT> &cars) -> bool {
        for (auto car : class_cars) {
            if (!cars.contains(car)) {
                return false;
            }
        }
        return true;
    };

    for (const auto &cl : classes.keys()) {
        if (is_sufficient_class(classes.value(cl), mCarsAllowed)) {
            mClassesAllowed[cl] = classes.value(cl);
        }
    }
}

void LfsServer::gotPlayerFlagsPacket (const IS_PFL * const pfl) {
    // TODO make sure bot can't change flags
    std::vector<QByteArray> fix_setup_messages;
    if (!checkPlayerFlags(fix_setup_messages, pfl->Flags, mDrivers[pfl->PLID])) {
        if (!mIsQuiet) {
            for (const QByteArray &mess: fix_setup_messages) {
                mSock->write(InsimMessage(mess, mDrivers[pfl->PLID]));
            }
        }
        spectatePlayer(mConns[mDrivers[pfl->PLID]]);
        return;
    }
}

void LfsServer::gotPlayerLeavePacket (const IS_PLL * const pl) {
    mDrivers.erase(pl->PLID);

    mCarPositions.erase(pl->PLID);
    mCarDraftTimestamps.erase(pl->PLID);

    if (mDrivers.size() == 0) {
        mUdpReceivalCheckTimer.stop();
    }
}

void LfsServer::gotPlayerPitPacket (const IS_PLP * const pl) {
    mDrivers.erase(pl->PLID);

    mCarPositions.erase(pl->PLID);
    mCarDraftTimestamps.erase(pl->PLID);

    if (mDrivers.size() == 0) {
        mUdpReceivalCheckTimer.stop();
    }
}

void LfsServer::gotStatePackPacket (const IS_STA * const state) {
    mRaceLaps = state->RaceLaps;

    if ((!mIsInRacingMode) && (state->Flags & ISS_GAME)) {
        // means switched state to game
        if (mDrivers.size()) {
            mUdpReceivalCheckTimer.start();
        }
    }

    mIsInRacingMode = state->Flags & ISS_GAME;

    if (!mIsInRacingMode) {
        mUdpReceivalCheckTimer.stop();
    }

    const QByteArray new_track(state->Track);

    if (new_track != mTrack) {
        mTrack = new_track;
        mOpenConfig = false;

        if (mVoteDispatcher.currentMode != TrackVoteDispatcher::Mode::CLOSED) {
            for (auto &el : mConns) {
                auto &ci = el.second;
                mSock->write(
                InsimMessage(ci.tr->get(StringIds::VOTING_CANCELLED),
                             el.first));
            }
            mVoteDispatcher.currentMode = TrackVoteDispatcher::Mode::CLOSED;
            InsimButtonFunction bc (BFN_DEL_BTN, 255, 239);
            mSock->write(bc);
            mTrackVoteTimerSecondsCounter.stop();
            mTrackVoteTimer.stop();
        }
    }

    if (mLoadedTrackName.size()) {
        mSock->write(InsimTiny(TINY_AXI, 3));
    }
}

void LfsServer::gotPlayerRenamePacket (const IS_CPR * const cpr) {
    mConns[cpr->UCID].nickname = cpr->PName;
    mUsersDb->userSetNickname(mConns[cpr->UCID].username,
            mConns[cpr->UCID].nickname);
}

void LfsServer::gotInsimVersionPacket (const IS_VER * const ver) {
    mLog() << "Connect success";
    mSock->write(InsimTiny(TINY_SST, 1));
    mSock->write(InsimTiny(TINY_RST, 1));
    mSock->write(InsimTiny(TINY_AXI, 3));
    mSock->write(InsimTiny(TINY_NCN, 3));
    mSock->write(InsimTiny(TINY_NCI, 3));
    mSock->write(InsimTiny(TINY_NPL, 3));

    mSock->write(InsimMessage(QByteArray("^7LFSTop^8 ")
                              + LfsTopVersion::as_string,
                              255));

//    QFile f("hello.txt");
//    f.open(QIODevice::ReadOnly);
//    QTextStream in(&f);
//    QString line = in.readLine();

//    mSock->write(InsimMessage(mDecoder.unicodeStringToLfs(line), 255));

    mConnected = true;

    (void) ver;
}

void LfsServer::processAvgLap (const ConnectionInfo &pConn) {
    const auto &laps = pConn.stintInfo.laps;

    const StintInfo::Lap &l1 = laps[laps.size() - 3];
    const StintInfo::Lap &l2 = laps[laps.size() - 2];
    const StintInfo::Lap &l3 = laps[laps.size() - 1];

    if (l1.exclude == StintInfo::Lap::ExcludeReason::LFS_INVALID
        || l2.exclude == StintInfo::Lap::ExcludeReason::LFS_INVALID
        || l3.exclude == StintInfo::Lap::ExcludeReason::LFS_INVALID) {
        return;
    }

    std::vector<LaptimeT> avg_scs;
    for (size_t i = 0; i < mNumSplits + 1; ++i) {
        const LaptimeT sum =
                l1.sections[i] + l2.sections[i] + l3.sections[i];
        const LaptimeT avg = sum / 3 + ((sum % 3) > 1 ? 1 : 0);
        avg_scs.push_back(avg);
    }
    const LapsDbStorage::Laptime avg =
            LapsDbStorage::Laptime::createBySections(avg_scs,
                                                     pConn.username);

    newLap("avg",
           avg,
           !(l1.exclude || l2.exclude || l3.exclude),
           pConn);
}

void LfsServer::finalizeLap (const IS_LAP* const pLap) {
    const quint8 pUcid = mDrivers[pLap->PLID];
    if (mConns[pUcid].currentLapNum == 0) {
        saveStintToStintsDb(mConns[pUcid]);
    }

    StintInfo::Lap &current_lap = mConns[pUcid].currentLap;

    current_lap.laptime = pLap->LTime;

    // append last sc
    if (mConns[pUcid].lastSplitTime != 0) {
        current_lap.sections.push_back(pLap->LTime - mConns[pUcid].lastSplitTime);
        mConns[pUcid].lastSplitTime = 0;
        if (current_lap.sections.back() == 0
                || current_lap.sections.size() != (mNumSplits + (unsigned char)1)) {
            current_lap.exclude = StintInfo::Lap::ExcludeReason::LFS_INVALID;
        }
    } else if (mConns[pUcid].previousUcid != 0
               && mConns[pUcid].currentLapNum == 0) {
        if (mConns.count(mConns[pUcid].previousUcid)) {
            ConnectionInfo &prev_ci = mConns[mConns[pUcid].previousUcid];
            prev_ci.currentLap.sections.push_back(pLap->LTime -
                                                  prev_ci.lastSplitTime);
            prev_ci.currentLap.laptime = pLap->LTime;
            prev_ci.stintInfo.laps.push_back(prev_ci.currentLap);
        }
        //return;
    } else {
        current_lap.exclude = StintInfo::Lap::ExcludeReason::LFS_INVALID;
    }

    // will just disregard outlap ffs
    if (pLap->LapsDone == 1) {
        current_lap.exclude = StintInfo::Lap::ExcludeReason::LFS_INVALID;
    }

    auto &laps = mConns[pUcid].stintInfo.laps;
    laps.push_back(current_lap);

    mConns[pUcid].currentLap = StintInfo::Lap();
}

void LfsServer::processStintInfoBestlaps (ConnectionInfo &pConn) {
    StintInfo &stint_info = pConn.stintInfo;
    const StintInfo::Lap &current_lap = stint_info.laps.back();


    if (current_lap.exclude == StintInfo::Lap::ExcludeReason::NONE) {
        if (stint_info.cleanBest == 0
                || current_lap.laptime
                < stint_info.laps[stint_info.cleanBest].laptime) {
            stint_info.cleanBest = pConn.currentLapNum;
            if (stint_info.noncleanBest != 0
                    && stint_info.laps[stint_info.cleanBest].laptime
                    < stint_info.laps[stint_info.noncleanBest].laptime) {
                stint_info.noncleanBest = 0;
            }
        }
        ++stint_info.cleanLapsNum;
    } else {
        if (stint_info.noncleanBest == 0
                || current_lap.laptime
                < stint_info.laps[stint_info.noncleanBest].laptime) {
            stint_info.noncleanBest = pConn.currentLapNum;
        }
        ++stint_info.noncleanLapsNum;
    }
}

void LfsServer::processTheoreticalBestLap (ConnectionInfo &pConn,
                                           const LapsDbStorage::Laptime &pLaptime,
                                           const bool pIsPbPrinted) {
    const StintInfo::Lap &current_lap = pConn.stintInfo.laps.back();

    assert(current_lap.exclude != StintInfo::Lap::ExcludeReason::LFS_INVALID);

    auto process_tb = [&] (const bool pClean) {
        auto &tb = pConn.tb[pClean];

        if (tb.empty()) {
            tb = current_lap.sections;
            newLap("tb",
                   pLaptime,
                   pClean,
                   pConn,
                   (!pIsPbPrinted) && pClean);
        } else {
            bool is_enhanced = false;
            for (size_t i = 0; i < tb.size(); ++i) {
                if (current_lap.sections[i] < tb[i]) {
                    tb[i] = current_lap.sections[i];
                    is_enhanced = true;
                }
            }
            if (is_enhanced) {
                const LapsDbStorage::Laptime new_tb =
                    LapsDbStorage::Laptime::createBySections(
                            pConn.tb[pClean],
                            pConn.username);
                newLap("tb",
                       new_tb,
                       pClean,
                       pConn,
                       (!pIsPbPrinted) && pClean);
            }
        }
        // do the job here for each type of lap
    };

    const bool clean = (current_lap.exclude ==
                        StintInfo::Lap::ExcludeReason::NONE);

    if (clean) {
        process_tb(true);
        process_tb(false);
    } else {
        process_tb(false);
    }
}

void LfsServer::processTheoreticalBestlapStintwise (ConnectionInfo &pConn,
                                                    bool &pIsPbPrinted) {
    const StintInfo::Lap &current_lap = pConn.stintInfo.laps.back();

    assert(current_lap.exclude != StintInfo::Lap::ExcludeReason::LFS_INVALID);

    auto process_tbs = [&] (const bool pClean) {
        auto &sc = pConn.stintInfo.sc[pClean];
        for (size_t i = 0; i < current_lap.sections.size(); ++i) {
            if(sc[i] == std::numeric_limits<uint32_t>::max()) {
                sc[i] = pConn.currentLapNum;
                continue;
            }
            const LaptimeT &section_time = current_lap.sections[i];
            const LaptimeT &best_section = pConn.stintInfo.laps[sc[i]].sections[i];
            if(best_section > section_time) {
                sc[i] = pConn.currentLapNum;
            }
        }

        std::vector<LaptimeT> tbs_sections;
        for (size_t i = 0; i < sc.size(); ++i) {
            if (sc[i] == std::numeric_limits<uint32_t>::max()) {
                return;
            }
            tbs_sections.push_back(pConn.stintInfo.laps[sc[i]].sections[i]);
        }

        const LapsDbStorage::Laptime theor_lap =
            LapsDbStorage::Laptime::createBySections(tbs_sections, pConn.username);

        const bool result = newLap("tbs",
                                   theor_lap,
                                   pClean,
                                   pConn,
                                   (!pIsPbPrinted) && pClean);
        if (pClean) {
            pIsPbPrinted = result;
        }
    };

    const bool clean = (current_lap.exclude ==
                        StintInfo::Lap::ExcludeReason::NONE);

    if (clean) {
        process_tbs(true);
        process_tbs(false);
    } else {
        process_tbs(false);
    }
}

void LfsServer::gotLapPacket (const IS_LAP * const lap) {
    if (!mDrivers.count(lap->PLID)) {
        return;
    }

    auto &conn = mConns[mDrivers[lap->PLID]];

    finalizeLap(lap);

    bool pb_printed = false;

    const auto &current_lap = conn.stintInfo.laps.back();
    const auto &laps = conn.stintInfo.laps;

    const LapsDbStorage::Laptime lt {
        stint_sections_to_splits(current_lap.sections), current_lap.laptime, conn.username };

    if (current_lap.exclude != StintInfo::Lap::ExcludeReason::LFS_INVALID) {
        pb_printed = newLap("top",
                            lt,
                            !current_lap.exclude,
                            conn);

        processTheoreticalBestlapStintwise(conn, pb_printed);

        processTheoreticalBestLap(conn, lt, pb_printed);

        if (laps.size() > 3) { // 1st lap is always excluded
            processAvgLap(conn);
        }

        processStintInfoBestlaps(conn);
    }

    ++conn.currentLapNum;
}

void LfsServer::gotSplitPacket (const IS_SPX * const spl) {
    if (mDrivers.count(spl->PLID)) {
        const quint8 ucid = mDrivers[spl->PLID];

        StintInfo::Lap &lap = mConns[ucid].currentLap;

        const size_t snum = lap.sections.size();
        if (snum != 0) {
            lap.sections.push_back(spl->STime - mConns[ucid].lastSplitTime);
        } else {
            lap.sections.push_back(spl->STime);
        }
        if (lap.sections.back() == 0) {
            lap.exclude = StintInfo::Lap::ExcludeReason::LFS_INVALID;
        }

        mConns[ucid].lastSplitTime = spl->STime;
    }
}

void LfsServer::invalidatePlayerLap (const quint8 pPlid,
                                     const StintInfo::Lap::ExcludeReason pReason,
                                     bool pNotify) {
    if (!mDrivers.count(pPlid)) {
        return;
    }
    auto &ci = mConns[mDrivers[pPlid]];
    if (!mHlvcCurrentConfig->contains(pReason)) {
        return;
    }
    if (!ci.currentLap.exclude) {
        if (pNotify
            && ci.userSettings->hlvcReasons
            && !mIsQuiet
            && pReason != StintInfo::Lap::ExcludeReason::HLVC_CAR_RESET
            && pReason != StintInfo::Lap::ExcludeReason::HLVC_SPEEDING
            && pReason != StintInfo::Lap::ExcludeReason::HLVC_PITLANE
            && ci.stintInfo.laps.size() != 0) {
            mSock->write(InsimMessage(
                insertStr(ci.tr->get(StringIds::HLVC_VIOLATION),
                          StintInfo::Lap::excludeReasonToString(pReason,
                                                                *ci.tr.get())),
                mDrivers[pPlid]));
        }
        ci.currentLap.exclude = pReason;
    }
}

bool LfsServer::isAdminCommand (const std::vector<QByteArray> &pCommand) {
    if ((pCommand[0] == "shutdown")
        || ((pCommand[0] == "restart") && mCfg.isRestartPossible)
        || (pCommand[0] == "cars" && pCommand.size() > 1)
        || (mCfg.recentDriversAdminsOnly && pCommand[0] == "recent")
        || (mCfg.logAdminsOnly && pCommand[0] == "log")
        || (pCommand[0] == "quiet")
        || (pCommand[0] == "notquiet")) {
        return true;
    }
    return false;
}

void LfsServer::gotHotlapValidationPacket (const IS_HLV * const hlv) {
    if (mDrivers.count(hlv->PLID)) {
        StintInfo::Lap::ExcludeReason reason =
                StintInfo::Lap::ExcludeReason::NONE;
        switch(hlv->HLVC) {
        case 0:
            reason = StintInfo::Lap::ExcludeReason::HLVC_GROUND;
            break;
        case 1:
            reason = StintInfo::Lap::ExcludeReason::HLVC_WALL;
            break;
        case 4:
            reason = StintInfo::Lap::ExcludeReason::HLVC_SPEEDING;
            break;
        case 5:
            reason = StintInfo::Lap::ExcludeReason::HLVC_OUT_OF_BOUNDS;
            break;
        }

        invalidatePlayerLap(hlv->PLID, reason);
    }
}

void LfsServer::gotCarsContactPacket (const IS_CON * const con) {
    if (mDrivers.count(con->A.PLID)) {
        invalidatePlayerLap(con->A.PLID,
                            StintInfo::Lap::ExcludeReason::HLVC_CONTACT);
    }
    if (mDrivers.count(con->B.PLID)) {
        invalidatePlayerLap(con->B.PLID,
                            StintInfo::Lap::ExcludeReason::HLVC_CONTACT);
    }
}

void LfsServer::gotObjectHitPacket (const IS_OBH * const obh) {
    if (mDrivers.count(obh->PLID)) {
        invalidatePlayerLap(obh->PLID,
                            StintInfo::Lap::ExcludeReason::HLVC_OBJECT_HIT);
    }
}

void LfsServer::gotPlayerPitlanePacket (const IS_PLA * const pit) {
    if (mDrivers.count(pit->PLID)) {
        invalidatePlayerLap(pit->PLID,
                            StintInfo::Lap::ExcludeReason::HLVC_PITLANE,
                            false);
    }
}

void LfsServer::gotCarResetPacket (const IS_CRS * const crs) {
    if (mDrivers.count(crs->PLID)) {
        invalidatePlayerLap(crs->PLID,
                            StintInfo::Lap::ExcludeReason::HLVC_CAR_RESET,
                            false);
    }
}

void LfsServer::gotAdminCommandReportPacket (const IS_ACR * const acr) {
    QByteArray command_text (acr->Text);
    if (acr->Result == 1) {
        notifyAdmin(QByteArray("Admin command: ") + command_text);

        if (command_text.startsWith("/cars ")) {
            gatherLfsCarsSetting(command_text);
        }
    }
}

void LfsServer::gotAutoXInfoPacket (const IS_AXI * const pPacket) {
    mTrackConfig = QByteArray(pPacket->LName).toUpper();

    if (pPacket->NumCP) {
        mOpenConfig = true;
        mNumSplits = pPacket->NumCP;

        if (!mTrackConfig.size()) {
            notifyAdmin("Unnamed open config track. Run /axsave and /axload");
        }
    }


    if (mOpenConfig) {
        mHlvcCurrentConfig = &mCfg.hlvcOpenconfigConfig;
    } else {
        mHlvcCurrentConfig = &mCfg.hlvcConfig;
    }

    if (mTrackConfig.size()) {
        if (mLoadedTrackName != mTrackConfig) {
            loadDatabase();
            if (mCfg.hlvcConfigPerTrack.count(mTrackConfig)) {
                mHlvcCurrentConfig = &mCfg.hlvcConfigPerTrack.at(mTrackConfig);
            }
        }
    } else {
        if (mLoadedTrackName != mTrack) {
            loadDatabase();
            if (mCfg.hlvcConfigPerTrack.count(mTrack)) {
                mHlvcCurrentConfig = &mCfg.hlvcConfigPerTrack.at(mTrack);
            }
        }
    }

    resetMciInterval();

    if (mCarsConfig.size()) {
        executeCarsCommand(parseCarsCommand(QByteArray::fromStdString(mCarsConfig)));
    } else {
        mSock->write(InsimTiny(TINY_ALC, 29));
    }

    mCarsConfig = ""; // so we don't run command twice :3

    mTrackLoadingIsInProcess = false;
}

void LfsServer::gotTakeOverPacket (const IS_TOC * const pPacket) {
    mDrivers[pPacket->PLID] = pPacket->NewUCID;

    ConnectionInfo &ci_new = mConns[pPacket->NewUCID];
    ConnectionInfo &ci_old = mConns[pPacket->OldUCID];

    ci_new.previousUcid = pPacket->OldUCID;

    ci_new.currentCar = ci_old.currentCar;
    ci_new.currentTyres = ci_old.currentTyres;
    ci_new.lastDrivenCar = ci_new.currentCar;

    ci_new.currentLapNum = 0;
    ci_new.currentLap = StintInfo::Lap();

    ci_new.tb[true] = getTb(ci_new);
    ci_new.tb[false] = getTb(ci_new, false);
}

void LfsServer::gotButtonFunctionPacket (const IS_BFN * const pPacket) {
    if (mConns.count(pPacket->UCID)) {
        auto &ci = mConns[pPacket->UCID];

        if (pPacket->SubT == BFN_USER_CLEAR) {
            ci.windowManager.clearedByUser();
        } else if (pPacket->SubT == BFN_REQUEST) {
            if (!mIsQuiet) {
                ci.windowManager.openWindow(&ci.settingsWindow);
            }
        }
    }
}

void LfsServer::notifyAdmin (const QByteArray &pMsg) {
    InsimMessage msg (pMsg);
    for (const unsigned char ucid : mAdminUcids) {
        msg.setConnectionId(ucid);
        mSock->write(msg);
    }
    mLog() << pMsg;
}

QByteArray LfsServer::getAllowedCarsMsg () const {
    QByteArray ret;
    for (const CarT c: mCarsAllowed) {
        ret += " " + mDb->getCarName(c);
    }

    return ret;
}

void LfsServer::gatherLfsCarsSetting (const IS_SMALL * const pPacket) {
    if (mIsExecutingCarsCommand) {
        return;
    }
    if (!mInitialAllowedCarsSyncNeeded) {
        return;
    }

    mInitialAllowedCarsSyncNeeded = false;

    mCarsAllowed.clear();
    using LFSCAR = LfsInsimUtils::LFS_ACL_CARS;
    if (pPacket->UVal & LFSCAR::UF1) {
        mCarsAllowed << CarT::UF1();
    }
    if (pPacket->UVal & LFSCAR::XFG) {
        mCarsAllowed << CarT::XFG();
    }
    if (pPacket->UVal & LFSCAR::XRG) {
        mCarsAllowed << CarT::XRG();
    }
    if (pPacket->UVal & LFSCAR::LX4) {
            mCarsAllowed << CarT::LX4();
    }
    if (pPacket->UVal & LFSCAR::LX6) {
        mCarsAllowed << CarT::LX6();
    }

    if (pPacket->UVal & LFSCAR::RB4) {
        mCarsAllowed << CarT::RB4();
    }
    if (pPacket->UVal & LFSCAR::FXO) {
        mCarsAllowed << CarT::FXO();
    }
    if (pPacket->UVal & LFSCAR::XRT) {
        mCarsAllowed << CarT::XRT();
    }
    if (pPacket->UVal & LFSCAR::RAC) {
        mCarsAllowed << CarT::RAC();
    }
    if (pPacket->UVal & LFSCAR::FZ5) {
        mCarsAllowed << CarT::FZ5();
    }

    if (pPacket->UVal & LFSCAR::UFR) {
        mCarsAllowed << CarT::UFR();
    }
    if (pPacket->UVal & LFSCAR::XFR) {
        mCarsAllowed << CarT::XFR();
    }
    if (pPacket->UVal & LFSCAR::FXR) {
        mCarsAllowed << CarT::FXR();
    }
    if (pPacket->UVal & LFSCAR::XRR) {
        mCarsAllowed << CarT::XRR();
    }
    if (pPacket->UVal & LFSCAR::FZR) {
        mCarsAllowed << CarT::FZR();
    }

    if (pPacket->UVal & LFSCAR::MRT) {
        mCarsAllowed << CarT::MRT();
    }
    if (pPacket->UVal & LFSCAR::FBM) {
        mCarsAllowed << CarT::FBM();
    }
    if (pPacket->UVal & LFSCAR::FOX) {
        mCarsAllowed << CarT::FOX();
    }
    if (pPacket->UVal & LFSCAR::FO8) {
        mCarsAllowed << CarT::FO8();
    }
    if (pPacket->UVal & LFSCAR::BF1) {
        mCarsAllowed << CarT::BF1();
    }

    refillCarsFromLfsCars();

    const IS_HCP hcp =
            LfsInsimUtils::createHandicapsPacket(QMap<CarT, CarDefinition>());
    mSock->writeBytes((const char*) &hcp, 68);

    fillAllowedClasses();
    const QByteArray cars_allowed_msg = getAllowedCarsMsg();
    for (auto &el : mConns) {
        auto &ci = el.second;
        mSock->write(
            InsimMessage(insertStr(ci.tr->get(StringIds::AVAILABLE_CARS),
                                   cars_allowed_msg),
                         el.first));
    }
}

void LfsServer::gatherLfsCarsSetting (const QByteArray &pCommand) {
    if (!mIsExecutingCarsCommand) {

        mCarsAllowed.clear();

        QByteArray command_args =
                pCommand.mid(6).toUpper().split(' ')[0];

        std::vector<QPair<bool, QByteArray>> args =
                LfsInsimUtils::getCarCommandArguments(command_args);

        for (const QPair<bool, QByteArray> &arg : args) {
            if (LapsdbTypeInfo::lfs_cars_aliases.contains(arg.second)) {
                if (arg.first) {
                    mCarsAllowed.unite(LapsdbTypeInfo::lfs_cars_aliases[arg.second]);
                } else {
                    mCarsAllowed.subtract(LapsdbTypeInfo::lfs_cars_aliases[arg.second]);
                }
            } else {
                if (arg.first) {
                    mCarsAllowed << mDb->getCarByName(arg.second);
                } else {
                    mCarsAllowed -= mDb->getCarByName(arg.second);
                }
            }
        }

        refillCarsFromLfsCars();

        const IS_HCP hcp =
                LfsInsimUtils::createHandicapsPacket(QMap<CarT, CarDefinition>());
        mSock->writeBytes((const char*) &hcp, 68);
    }
    else {
        mIsExecutingCarsCommand = false;
    }


    fillAllowedClasses();
    const QByteArray cars_allowed_msg = getAllowedCarsMsg();
    for (auto &el : mConns) {
        auto &ci = el.second;
        mSock->write(
            InsimMessage(insertStr(ci.tr->get(StringIds::AVAILABLE_CARS),
                                   cars_allowed_msg),
                         el.first));
    }
}

void LfsServer::refillCarsFromLfsCars () {
    QMapIterator<QByteArray, CarDefinition > customCars =
            mDb->getCarsIterator();

    const auto allowed_by_host = mCarsAllowed;

    while (customCars.hasNext()) {
        customCars.next();
        if (allowed_by_host.contains(customCars.value().lfsCar)) {
            mCarsAllowed << mDb->getCarByName(customCars.key());
        }
    }
}

CarT LfsServer::getMostUsedFromAllowedCars (const QByteArray &pDbName,
                                            const TyreT pTyres) {
    CarT ret_car = CarT::unknownCar();

    int num = -1;

    for (const CarT c : mCarsAllowed) {
        int n = mDb->getLaptimesCount(pDbName, c, pTyres, false);
        if (n > num) {
            ret_car = c;
            num = n;
        }
    }
    return ret_car;
}

ClassT LfsServer::getMostUsedFromAllowedClasses(const QByteArray &pDbName,
                                                const TyreT pTyres) {
    ClassT ret_class = ClassT::notFound();

    int num = -1;

    for (const auto &c : mClassesAllowed) {
        int n = mDb->getClassLaptimesCount(pDbName, c.first, pTyres, false);
        if (n > num) {
            ret_class = c.first;
            num = n;
        }
    }
    return ret_class;
}

QSet<CarT> LfsServer::parseCarsCommand (const QByteArray &pCommand) {
    std::vector<QPair<bool, QByteArray>> args =
            LfsInsimUtils::getCarCommandArguments(pCommand);

    QSet<CarT> ret;

    QByteArray error_msg;

    for (const QPair<bool, QByteArray> &arg : args) {
        if (LapsdbTypeInfo::lfs_cars_aliases.contains(arg.second)) {
            if (arg.first) {
                ret.unite(LapsdbTypeInfo::lfs_cars_aliases[arg.second]);
            } else {
                ret.subtract(LapsdbTypeInfo::lfs_cars_aliases[arg.second]);
            }
        } else if (mDb->getClassByName(arg.second)
                   != ClassT::notFound()) {
            const ClassT c = mDb->getClassByName(arg.second);
            if (arg.first) {
                ret.unite(mDb->getClassCars(c));
            } else {
                ret.subtract(mDb->getClassCars(c));
            }
        } else if (mDb->getCarByName(arg.second)
                   != CarT::unknownCar()){
            if (arg.first) {
                ret << mDb->getCarByName(arg.second);
            } else {
                ret -= mDb->getCarByName(arg.second);
            }
        } else {
            error_msg.append(arg.second + " not found; ");
        }
    }

    if (error_msg.size()) {
        notifyAdmin(error_msg);
    }

    return ret;
}

IS_MST LfsServer::createCarsCommand (const QSet<CarT> &cars) {
    QByteArray text ("/cars ");
    IS_MST ret { 68, ISP_MST, 0, 0, {0} };
    if (cars.size() > 10) {
        // put ALL<-CAR1, -CAR2, ...>
        text += "ALL";
        QSet<CarT> cars_to_exclude =
                LapsdbTypeInfo::lfs_cars_aliases["ALL"];
        cars_to_exclude.subtract(cars);

        for (const CarT c : cars_to_exclude) {
            text+= "-" + mDb->getCarName(c);
        }
    } else {
        for (const CarT c : cars) {
            text+= mDb->getCarName(c) + "+";
        }
        text.truncate(text.size() - 1);
    }
    assert(text.size() < 64);
    memcpy(ret.Msg, text.constData(), (size_t) text.size());
    return ret;
}

void LfsServer::executeCarsCommand (const QSet<CarT> &pCars) {
    if (pCars.empty()) {
        notifyAdmin("No proper cars requested, aborting");
        return;
    }

    mInitialAllowedCarsSyncNeeded = false;

    QMap<CarT, CarDefinition> handicaps;
    mCarsAllowed.clear();
    mAmbiguousLfsCars.clear();

    QSet<CarT> lfs_cars_llowed;

    for (const CarT &c : pCars) {
        mCarsAllowed << c;
        if (c.isStockLfsCar()) { // stock car
            handicaps[c] = CarDefinition(c);
            mAmbiguousLfsCars -= c;
            lfs_cars_llowed << c;
        } else {
            const CarT lfs_car = mDb->getCarDefinition(c).lfsCar;
            lfs_cars_llowed << lfs_car;
            if (handicaps.contains(lfs_car)) {
                if (mDb->getCarDefinition(c).overlaps(handicaps[lfs_car])) {
                    mLog(Log::WARNING) << "ambiguous restrictions: "
                                       << mDb->getCarDefinition(c).toString()
                                       << " "
                                       << handicaps[lfs_car].toString();
                    handicaps[lfs_car] = mDb->getCarDefinition(c)
                                         .combine(handicaps[lfs_car]);

                    mAmbiguousLfsCars << lfs_car;
                } else {
                    handicaps[lfs_car] = std::min(mDb->getCarDefinition(c),
                                                  handicaps[lfs_car]);
                    if (mAmbiguousLfsCars.contains(lfs_car)) {
                        mLog(Log::WARNING) << "ambiguity resolved by a faster car"
                                           << mDb->getCarName(c)
                                           << mDb->getCarDefinition(c).toString();
                    }
                    mAmbiguousLfsCars -= lfs_car;

                }
            } else {
                handicaps[lfs_car] = mDb->getCarDefinition(c);
            }
        }
    }

    QSet<CarT> slower_cars;
    for (const CarT ac : mCarsAllowed) {
        const CarDefinition ac_definition =
                mDb->getCarDefinition(ac);
        for (const CarT c : mDb->getCustomCarsByLfsCar(ac_definition.lfsCar)) {
            const CarDefinition car_to_check = mDb->getCarDefinition(c);
            if (ac_definition < car_to_check) {
                slower_cars << c;
            }
        }
    }

    mCarsAllowed += slower_cars;

    mIsExecutingCarsCommand = true;
    const IS_MST comm = createCarsCommand(lfs_cars_llowed);
    mSock->writeBytes((const char*) &comm, 68);

    const IS_HCP hcp = LfsInsimUtils::createHandicapsPacket(handicaps);
    mSock->writeBytes((const char*) &hcp, 68);

    for (const auto &el: mDrivers) {
        const ConnectionInfo &ci = mConns[el.second];
        if (!mCarsAllowed.contains(ci.currentCar)) {
            spectatePlayer(ci);
        }
    }
}

void LfsServer::gotRaceStatePacket (const IS_RST * const rst) {
    for (auto &el : mConns) {
        el.second.currentLapNum = 0;
        el.second.currentLap = StintInfo::Lap();
        saveStintToStintsDb(el.second);
    }

    if (!mOpenConfig) {
        mNumSplits = 3;
        const unsigned short illegal =
                std::numeric_limits<unsigned short>::max();
        if (rst->Split3 == illegal) {
            mNumSplits = 2;
        }
        if (rst->Split2 == illegal) {
            mNumSplits = 1;
        }
        if (rst->Split1 == illegal) {
            mNumSplits = 0;
        }
    }
}


void LfsServer::processStintRequested (ConnectionInfo &pConn,
                                       const std::vector<QByteArray> pCommand) {
    StintInfo *requested_stint_info;
    CarT car = CarT::unknownCar();
    QByteArray username;

    if (pCommand.size() == 1) {
        requested_stint_info = &pConn.stintInfo;
        car = pConn.lastDrivenCar;
        username = pConn.username;
    } else if (pCommand.size() == 2) {
        if (pCommand[1].startsWith('\\')) {
            const QByteArray n = pCommand[1].mid(1);
            bool ok;
            const size_t i = n.toULong(&ok);
            if (!ok
                || i > mStintsDb.stintsList.size()
                || i > static_cast<size_t>(std::numeric_limits<long int>::max())) {
                return;
            }

            auto it = std::next(mStintsDb.stintsList.begin(), static_cast<long int>(i));
            auto &el = *it;

            username = el.username;
            car = el.car;
            requested_stint_info = &el.stintInfo;
        } else {
            uint8_t requested_ucid = findUcidByString(pCommand[1]);
            if (requested_ucid == 0) {
                mSock->write(InsimMessage(pConn.tr->get(StringIds::DIDNT_FIND_SUCH_PLAYER),
                                          pConn.id));
                return;
            }
            requested_stint_info = &mConns[requested_ucid].stintInfo;
            car = mConns[requested_ucid].lastDrivenCar;
            username = mConns[requested_ucid].username;
        }
    } else {
        mSock->write(
            InsimMessage(pConn.tr->get(StringIds::INCORRECT_REQUEST_FORMAT),
                         pConn.id));
        return;
    }

    if (requested_stint_info->laps.size() > 0) {
        pConn.stintDataProvider.setStintInfo(requested_stint_info);
        pConn.window.setStartFrom(0);
        const QByteArray header =
                insertStr(pConn.tr->get(StringIds::STINT_INFO),
                          mUsersDb->userGetFullName(username)
                          + ' '
                          + mDb->getCarName(car));

        pConn.window.setHeader(header);
        pConn.window.setDataProvider(&pConn.stintDataProvider);
        pConn.windowManager.openWindow(&pConn.window);
        } else {
            mSock->write(InsimMessage(
                insertStr(pConn.tr->get(StringIds::HAVENT_DONE_ANY_LAPS_YET),
                          mUsersDb->userGetFullName(username)),
                pConn.id));
        }
}

void LfsServer::processStintsRequested (ConnectionInfo &pConn) {
    pConn.window.setStartFrom(0);
    pConn.window.setHeader(insertStr("^7LFSTop ^8%s:",
                                     pConn.tr->get(StringIds::RECENT_STINTS)));

    pConn.window.setDataProvider(&pConn.recentStintsDataProvider);
    pConn.windowManager.openWindow(&pConn.window);
}

void LfsServer::processHelpRequested (ConnectionInfo &pConn) {
    const bool is_admin =
        std::find(mAdminUcids.begin(), mAdminUcids.end(), pConn.id)
            != mAdminUcids.end();

    pConn.window.setStartFrom(0);
    pConn.window.setHeader(insertStr("^7LFSTop ^8%s:",
                                  pConn.tr->get(StringIds::COMMANDS)));

    if (is_admin) {
        pConn.window.setDataProvider(&mHelpDataProviderAdmin);
    } else {
        pConn.window.setDataProvider(&mHelpDataProvider);
    }

    pConn.windowManager.openWindow(&pConn.window);
}

void LfsServer::processRecentRequested (ConnectionInfo &pConn) {
    pConn.window.setStartFrom(0);
    pConn.window.setHeader(insertStr("^7LFSTop ^8%s:",
                                  pConn.tr->get(StringIds::RECENT_PLAYERS)));

    pConn.window.setDataProvider(&mRecentDriversDataProvider);
    pConn.windowManager.openWindow(&pConn.window);
}

void LfsServer::processAdminCommand (ConnectionInfo &pConn,
                                     const std::vector<QByteArray> &pCommand) {
    if (pCommand[0] == "cars") {
        if (pCommand.size() > 1) {
            mIsExecutingCarsCommand = true;
            executeCarsCommand(parseCarsCommand(pCommand[1].toUpper()));
        }
    } else if (pCommand[0] == "shutdown") {
        emit shutdownRequested();
    } else if (pCommand[0] == "restart" && mCfg.isRestartPossible) {
        emit restartRequested();
    } else if (pCommand[0] == "recent") {
        processRecentRequested(pConn);
    } else if (pCommand[0] == "log") {
        processLogRequested(pConn);
    } else if (pCommand[0] == "quiet") {
        mIsQuiet = true;
    } else if (pCommand[0] == "notquiet") {
        mIsQuiet = false;
    }

}

void LfsServer::gotMessageOutPacket (const IS_MSO * const mso) {
    const QByteArray mess (mso->Msg + mso->TextStart);
    const QByteArray full_mess (mso->Msg);

    mLogDataProvider.addMessage(full_mess);

    mChatLog() << mDecoder.lfsStringToUnicode(full_mess);

    if (mTrack.isEmpty()) {
        return; // in case we just connected...
    }

    if (mess[0] == mCfg.commandPrefix) {
        if (mess.size() == 1) {
            mConns[mso->UCID].windowManager.clearWindow();
            return;
        }

        StringUtils::QByteArrayTokenizer tok (mess.mid(1));
        std::vector<QByteArray> command = tok.getResultList();
        command[0] = command[0].toLower();

        if (isAdminCommand(command)) {
            if (mso->UCID == 0
                || std::find(mAdminUcids.begin(), mAdminUcids.end(), mso->UCID)
                    != mAdminUcids.end()) {
                processAdminCommand(mConns[mso->UCID], command);
            }
        } else {
            processUserCommand(mConns[mso->UCID], command);
        }
    }
}

void LfsServer::gotButtonClickPacket (const IS_BTC * const btc) {
    mConns[btc->UCID].windowManager.buttonCLicked(*btc);
}

void LfsServer::stateChanged (QAbstractSocket::SocketState pState) {
    if (pState == QAbstractSocket::ConnectedState) {
        mSock->write(InsimInit("LFSTop", mCfg.password)
                     .setGetVersion()
                     .setKeepColors(true)
                     .setHlv()
                     .setCon()
                     .setObh()
                     .setMci()
                     .setUdpPort(mUdpPort)
                     .setInterval(100)
                     .setProcessJoinRequests()
                     .setHostPrefix(mCfg.commandPrefix)
                     .setVersion(7));
        mConnectedTimeStamp = QDateTime::currentMSecsSinceEpoch();
    }
}

void LfsServer::errorOccured (QAbstractSocket::SocketError pError) {
    switch (pError) {
    case QAbstractSocket::RemoteHostClosedError:
        if (mConnected) {
            mLog() << "Server closed connection";
        } else {

            mLog(Log::ERROR)
                    << "Probably admin password is not configured properly";
        }
        emit finished(mInstanceName, false);
        break;
    default:
        mLog(Log::ERROR) << "Can't connect to LFS";
        mLog(Log::ERROR) << "Socket Error: "
                         << ((QIODevice*) mSock)->errorString();
        emit finished(mInstanceName, false);
        break;
    }
}

void LfsServer::disconnected () {
    Q_ASSERT(mSock);
    saveData();
    mLog() << "disconnected";
    emit finished(mInstanceName, true);
}

void LfsServer::run () {
    mIsStarted = true;
    mSock = new LfsTcpSocket(this);
    mSock->connectToHost(mCfg.host.c_str(), mCfg.port);
    connect(mSock, SIGNAL(readyRead()), this, SLOT(dataReceived()));
    connect(mSock, SIGNAL(stateChanged(QAbstractSocket::SocketState)),
            this, SLOT(stateChanged(QAbstractSocket::SocketState)));
    connect(mSock, SIGNAL(error(QAbstractSocket::SocketError)),
            this, SLOT(errorOccured(QAbstractSocket::SocketError)));
    connect(mSock, SIGNAL(disconnected()), this, SLOT(disconnected()));

    mUdpSock = new QUdpSocket(this);

    if (mCfg.udpPort != 0) {
        if (!mUdpSock->bind(QHostAddress::LocalHost, mCfg.udpPort)) {
            mLog() << "Failed to bind udp port: " << mCfg.udpPort;
            mSock->disconnectFromHost();
        }
        mUdpPort = mCfg.udpPort;
    } else {

        quint16 port = 1024u;
        do {
            ++port;
        } while(!mUdpSock->bind(QHostAddress::LocalHost, port));
        mUdpPort = port;
    }
    connect(mUdpSock, SIGNAL(readyRead()), this, SLOT(udpDataReceived()));
}
