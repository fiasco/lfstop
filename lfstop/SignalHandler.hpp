// Copyright 2015 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of LFSTop.
//
// LFSTop is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LFSTop is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LFSTop.  If not, see <http://www.gnu.org/licenses/>.


#ifndef LFSTOP_SIGNAL_HANDLER_HPP_INCLUDED
#define LFSTOP_SIGNAL_HANDLER_HPP_INCLUDED

// taken from here:
// http://stackoverflow.com/questions/16826097/equivalent-to-sigint-posix-signal-for-catching-ctrlc-under-windows-mingw

class SignalHandler {
public:
    SignalHandler (const int mask = DEFAULT_SIGNALS);
    virtual ~SignalHandler ();

    enum SIGNALS {
        // Physical signal not supported by this class
        SIG_UNHANDLED = 0,
        // The application is requested to do a no-op (only a target that
        // platform-specific signals map to when they can't be raised anyway)
        SIG_NOOP = 1,
        // Control+C (should terminate but consider that it's a normal way
        // to do so; can delay a bit)
        SIG_INT = 2,
        // Control+Break (should terminate now without regarding the consquences)
        SIG_TERM = 4,
        // Container window closed (should perform normal termination, like
        // Ctrl^C) [Windows only; on Linux it maps to SIG_TERM]
        SIG_CLOSE = 8,
        // Reload the configuration [Linux only, physical signal is SIGHUP;
        // on Windows it maps to SIG_NOOP]
        SIG_RELOAD = 16,
        DEFAULT_SIGNALS = SIG_INT | SIG_TERM | SIG_CLOSE,
    };

    static const int numSignals = 6;

    virtual bool handleSignal (const int signal) = 0;

private:
    int _mask;
};

#endif // LFSTOP_SIGNAL_HANDLER_HPP_INCLUDED
