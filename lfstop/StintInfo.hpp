// Copyright 2015 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of LFSTop.
//
// LFSTop is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LFSTop is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LFSTop.  If not, see <http://www.gnu.org/licenses/>.


#ifndef LFSTOP_STINT_INFO_HPP_INCLUDED
#define LFSTOP_STINT_INFO_HPP_INCLUDED


#include <vector>
#include <cstddef>


#include "Databases/LapsDbStorage.hpp"
#include "Translation.hpp"


struct StintInfo {
    explicit StintInfo (QByteArray pTrackName = "", const size_t pSectionsNum = 0);

    struct Lap {
        enum ExcludeReason : uint8_t { NONE,
                                       LFS_INVALID,
                                       HLVC_GROUND,
                                       HLVC_WALL,
                                       HLVC_SPEEDING,
                                       HLVC_CONTACT,
                                       HLVC_OBJECT_HIT,
                                       HLVC_PITLANE,
                                       HLVC_CAR_RESET,
                                       HLVC_DRAFT,
                                       HLVC_OUT_OF_BOUNDS,
                                       HLVC_NOT_CHECKED, };
        static QByteArray excludeReasonToString (const ExcludeReason pReason,
                                                 const Translation::TranslationBinder &pTr) {
            using Translation::StringIds;
            switch (pReason) {
            case NONE:
                return pTr.get(StringIds::EXCLUDE_REASON_NONE);
            case LFS_INVALID:
                return pTr.get(StringIds::EXCLUDE_REASON_INVALID);
            case HLVC_GROUND:
                return pTr.get(StringIds::EXCLUDE_REASON_GROUND);
            case HLVC_WALL:
                return pTr.get(StringIds::EXCLUDE_REASON_WALL);
            case HLVC_SPEEDING:
                return pTr.get(StringIds::EXCLUDE_REASON_SPEEDING);
            case HLVC_CONTACT:
                return pTr.get(StringIds::EXCLUDE_REASON_CONTACT);
            case HLVC_OBJECT_HIT:
                return pTr.get(StringIds::EXCLUDE_REASON_OBJECT_HIT);
            case HLVC_PITLANE:
                return pTr.get(StringIds::EXCLUDE_REASON_PITLANE);
            case HLVC_CAR_RESET:
                return pTr.get(StringIds::EXCLUDE_REASON_CAR_RESET);
            case HLVC_DRAFT:
                return pTr.get(StringIds::EXCLUDE_REASON_DRAFT);
            case HLVC_OUT_OF_BOUNDS:
                return pTr.get(StringIds::EXCLUDE_REASON_OUT_OF_BOUNDS);
            case HLVC_NOT_CHECKED:
                return pTr.get(StringIds::EXCLUDE_REASON_NOT_CHECKED);
            }
            return "U wot m8";
        }

        static ExcludeReason
        excludeReasonFromString (const QByteArray &pReason) {
            if (pReason == "INVALID") {
                return ExcludeReason::LFS_INVALID;
            } else if (pReason == "GROUND") {
                return ExcludeReason::HLVC_GROUND;
            } else if (pReason == "WALL") {
                return ExcludeReason::HLVC_WALL;
            } else if (pReason == "SPEEDING") {
                return ExcludeReason::HLVC_SPEEDING;
            } else if (pReason == "CONTACT") {
                return ExcludeReason::HLVC_CONTACT;
            } else if (pReason == "OBJECT_HIT") {
                return ExcludeReason::HLVC_OBJECT_HIT;
            } else if (pReason == "PITLANE") {
                return ExcludeReason::HLVC_PITLANE;
            } else if (pReason == "CAR_RESET") {
                return ExcludeReason::HLVC_CAR_RESET;
            } else if (pReason == "DRAFT") {
                return ExcludeReason::HLVC_DRAFT;
            } else if (pReason == "OUT_OF_BOUNDS") {
                return ExcludeReason::HLVC_OUT_OF_BOUNDS;
            } else if (pReason == "NOT_CHECKED") {
                return ExcludeReason::HLVC_NOT_CHECKED;
            }
            return ExcludeReason::NONE;
        }

        Lap () = default;
        Lap (const size_t pNumSplits)
            : sections (pNumSplits + 1, 0) {
        }

        std::vector<LaptimeT> sections;
        LaptimeT laptime = 0;
        ExcludeReason exclude = ExcludeReason::NONE;
    };
    QByteArray trackName;
    std::vector<Lap> laps;
    std::vector<std::vector<uint32_t>> sc; // vector of <sc# : lap number in laps list>
    size_t cleanBest = -0; // number of best lap in laps list
    size_t cleanLapsNum = 0;
    size_t noncleanBest = 0; // as the latter, will stay '-1' if cleanBestlap is faster
    size_t noncleanLapsNum = 0;
};





#endif // LFSTOP_STINT_INFO_HPP_INCLUDED
