// Copyright 2015 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of LFSTop.
//
// LFSTop is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LFSTop is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LFSTop.  If not, see <http://www.gnu.org/licenses/>.


#include "Settings.hpp"

namespace Settings {

SettingsMap get_settings_map (const MKeyFile &f) {
    SettingsMap cfg;
    MSectionsIterator sec_iter = f.getSectionsIterator();
    while (sec_iter.isElement()) {
        MSettingsIterator set_iter = sec_iter.getSettingsIterator();
        const std::string sec_name = sec_iter.getName();
        const std::string prefix =
                sec_name.empty() ? "" : sec_name + ".";
        while (set_iter.isElement()) {
            cfg[prefix + set_iter.getName()] =
                    set_iter.getValue();
            set_iter.peekNext();
        }
        sec_iter.peekNext();
    }
    return cfg;
}

} // namespace Settings
