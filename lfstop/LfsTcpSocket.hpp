// Copyright 2015 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of LFSTop.
//
// LFSTop is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LFSTop is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LFSTop.  If not, see <http://www.gnu.org/licenses/>.


#ifndef LFSTOP_LFS_TCP_SOCKET_HPP_INCLUDED
#define LFSTOP_LFS_TCP_SOCKET_HPP_INCLUDED


#include <QTcpSocket>


#include "InsimPackets/IInsimPacket.hpp"


class LfsTcpSocket : public QTcpSocket {
public:
    LfsTcpSocket (QObject* pParent)
        : QTcpSocket (pParent) {
    }

    qint64 write (const IInsimPacket &pPacket);
    qint64 writeBytes (const QByteArray &pBytes);
    qint64 writeBytes (const char *pData, qint64 pLen);

    qint64 getSentBytes () const {
        return mSentBytes;
    }

private:
    qint64 mSentBytes = 0;
};

#endif // LFSTOP_LFS_TCP_SOCKET_HPP_INCLUDED
