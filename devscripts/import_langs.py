#!/bin/python3



import sys;
import os;
from os import path;
from subprocess import call

infiles = filter(path.isfile, os.listdir("."))

code_pages = { "1252" : "windows-1252",
               "1251" : "windows-1251",
               "1253" : "windows-1253",
               "932"  : "cp932",
               "1250" : "windows-1250",
               "1254" : "windows-1254",
               "1257" : "windows-1257",
               "950"  : "cp950",
               "936"  : "cp936",
               "949"  : "cp949" }

if not os.path.exists("tmp"):
    os.makedirs("tmp")

if not os.path.exists("languages"):
    os.makedirs("languages")

for el in infiles:
    data = dict()
    f = open(el)
    print("processing: ", el)
    for l in f:
        if l[0] == ';' or l[0] == '#':
            continue
        space_pos = l.find('=')
        if space_pos > 0 and (space_pos + 1) < len(l):
            s = l[space_pos + 2:-2]
            data[l[:space_pos]] = s
    lang_code = data.get("LANGUAGE_CODE")
    code_page = data.get("CODE_PAGE")
    if lang_code == None or code_page == None:
        print("ERROR: not processing " + el + ". No CODE_PAGE or LANGUAGE_CODE")
    del data["LANGUAGE_CODE"]
    del data["CODE_PAGE"]

    if (code_page in code_pages and len(lang_code) != 0):
        with open("tmp/" + lang_code + ".txt", "w") as outfile:
            outfile.write("CODE_PAGE" + ' ' + code_page + '\n')
            outfile.write("LANGUAGE_CODE" + ' ' + lang_code + '\n')
            for key, value in sorted(data.items()):
                outfile.write(key + ' ' + value + '\n')

        command = ["iconv", "tmp/" + lang_code + ".txt", "-f", "utf8", "-t", code_pages[code_page], "-o", "languages/" + lang_code + ".txt"]
        print(command)

        call(command)

    else:
        print(f, " code page invalid: ", code_page)
