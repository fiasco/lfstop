// Copyright 2016 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of LFSTop.
//
// LFSTop is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LFSTop is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LFSTop.  If not, see <http://www.gnu.org/licenses/>.


#include <QtTest>
#include <QByteArray>


#include "Databases/StintsDb.hpp"
#include "Logging/Log.hpp"


class UnitTests : public QObject
{
    Q_OBJECT

public:
    UnitTests() {}
    ~UnitTests() {}

private Q_SLOTS:
    void StintsDbTest ();
};

template<typename T>
bool operator== (std::vector<T> &pFirst, std::vector<T> pSecond);


bool operator== (StintInfo::Lap pFirst, StintInfo::Lap pSecond) {
    return pFirst.sections == pSecond.sections
            && pFirst.laptime == pSecond.laptime
            && pFirst.exclude == pSecond.exclude;
}

template<typename T>
bool operator== (std::vector<T> &pFirst, std::vector<T> pSecond) {
    return pFirst.size() == pSecond.size()
            && std::equal(pFirst.begin(), pFirst.end(), pSecond.begin());
}

bool operator== (const StintInfo &pFirst,
                 const StintInfo &pSecond) {
    return pFirst.laps == pSecond.laps
            && pFirst.sc == pSecond.sc
            && pFirst.cleanBest == pSecond.cleanBest
            && pFirst.cleanLapsNum == pSecond.cleanLapsNum
            && pFirst.noncleanBest == pSecond.noncleanBest
            && pFirst.noncleanLapsNum == pSecond.noncleanLapsNum
            && pFirst.trackName == pSecond.trackName;
}

bool operator== (const StintInfoListRow &pFirst,
                 const StintInfoListRow &pSecond) {
    return pFirst.username == pSecond.username
            && pFirst.car == pSecond.car
            && pFirst.tyres == pSecond.tyres
            && pFirst.stintInfo == pSecond.stintInfo
            && pFirst.timeStamp == pSecond.timeStamp;
}

void UnitTests::StintsDbTest () {
    Log log("meh");

    StintInfoList l;
    StintInfo info1("BL1", 3);
    StintInfo info2("SO4", 2);

    info1.laps.push_back(StintInfo::Lap(3));

    l.push_back(StintInfoListRow { "user1",
                                   CarT::FBM(),
                                   TyreT::fromOne(123),
                                   info1,
                                   123 });
    l.push_back(StintInfoListRow { "user2",
                                   CarT::FBM(),
                                   TyreT::fromOne(123),
                                   info2,
                                   123 });

    {
        StintsDb db("test_stints_db", log);
        db.stintsList = l;
    }
    {
        StintsDb db("test_stints_db", log);
        QCOMPARE(db.stintsList.size(), l.size());
        for (size_t i = 0; i < l.size(); ++i) {
            auto it1 = std::next(l.begin(), static_cast<long int>(i));
            auto it2 = std::next(db.stintsList.begin(), static_cast<long int>(i));
            QCOMPARE(*it1, *it2);
        }
    }
}

QTEST_APPLESS_MAIN(UnitTests)


#include "TestStintsDb.moc"
