// Copyright 2015 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of LFSTop.
//
// LFSTop is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LFSTop is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LFSTop.  If not, see <http://www.gnu.org/licenses/>.


#include <random>
#include <cstdint>


#include <QtTest>
#include "Databases/LapsDb.hpp"

#include "Utils.hpp"

class BenchmarkLapsDb : public QObject {
    Q_OBJECT
private Q_SLOTS:

    void Benchmark ();
    void ClassesBenchmark ();
};

class mt_binder {
public:
    mt_binder (std::mt19937 &pMt)
        : mMt (pMt) {
    }

    size_t operator () (const size_t pNotMoreThan) {
        std::uniform_int_distribution<size_t> dist (0, pNotMoreThan);
        return dist(mMt);
    }

private:
    std::mt19937 &mMt;
};

void BenchmarkLapsDb::Benchmark () {
    Log l("meh");
    LapsDb db(l);
    db.loadFile("Benchmark");
    std::vector<TestLaptimeData> data;
    const size_t laps_num = 4000;


    std::mt19937 mt (18276);
    // here we actually need uint8_t but we can not get it from Microsoft:
    std::uniform_int_distribution<uint16_t> num_to_add_dist (3, 7);
    std::uniform_int_distribution<LaptimeT> laptime_dist (10, 200);

    for (size_t i = 0; i <= laps_num;) {
        const uint16_t rnd = num_to_add_dist(mt); // from 3 to 7 laptimes
        const QByteArray random_user (get_random_username(mt).c_str());
        for (uint16_t ii = 0; ii < rnd; ++ii) {
            data.push_back({ 1, 3, { { 20}, laptime_dist(mt), random_user } } );
            ++i;
        }
    }

    std::random_shuffle(data.begin(), data.end(), mt_binder(mt));

    QBENCHMARK_ONCE {
        for (const auto &el : data) {
            db.addLaptime("top", el.car, el.tyres, el.lt);
        }
    }
}



void BenchmarkLapsDb::ClassesBenchmark () {
    Log l("meh");
    LapsDb db(l);
    db.addClass("GTR", QSet<QByteArray>{"FXR","XRR","FZR"});
    db.loadFile("ClassesBenchmark");
    std::vector<TestLaptimeData> data;
    const size_t laps_num = 4000;

    std::mt19937 mt (18276);
    std::uniform_int_distribution<uint16_t> num_to_add_dist (3, 7);
    std::uniform_int_distribution<LaptimeT> laptime_dist (10, 200);
    std::uniform_int_distribution<CarT::InnerType> car_dist (12, 14);

    for (size_t i = 0; i <= laps_num;) {
        const QByteArray random_user (get_random_username(mt).c_str());
        for (uint16_t ii = 0; ii < num_to_add_dist(mt); ++ii) {
            data.push_back({ car_dist(mt),
                             3,
                             { { 20}, laptime_dist(mt), random_user } } );
            ++i;
        }
    }

    std::random_shuffle(data.begin(), data.end(), mt_binder(mt));

    QBENCHMARK_ONCE {
        for (const auto &el : data) {
            db.addLaptime("top", el.car, el.tyres, el.lt);
        }
    }
}


QTEST_APPLESS_MAIN(BenchmarkLapsDb)


#include "BenchmarkLapsDb.moc"
