// Copyright 2016 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of LFSTop.
//
// LFSTop is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LFSTop is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LFSTop.  If not, see <http://www.gnu.org/licenses/>.


#include <QtTest>
#include <QByteArray>



#include "Translation.hpp"
#include "Common.hpp"


class UnitTests : public QObject
{
    Q_OBJECT

public:
    UnitTests() {}
    ~UnitTests() {}

private Q_SLOTS:
    void qt_time_stamp_diff_to_str_test ();
    void stint_sections_to_splits_test ();
};


void UnitTests::qt_time_stamp_diff_to_str_test () {
    Translation::Translation tr_;
    Translation::TranslationBinder trbind(tr_, "ENG");

    QDateTime start = QDateTime::currentDateTime();
    QDateTime end =
        start
            .addDays(2)
            .addSecs(11 * 60 * 60) // add 11 hours
            .addSecs(9 * 60) // add 9 minutes
            .addSecs(15); // add 15 seconds


    QByteArray result =
        qt_time_stamp_diff_to_str(start.msecsTo(end), trbind);

    QCOMPARE(result, QByteArray("2 days 11 hours 9 minutes 15 seconds"));
}

void UnitTests::stint_sections_to_splits_test () {
    QCOMPARE(stint_sections_to_splits({}), {});

    const auto result =
        stint_sections_to_splits({ 10, 10, 10});
    const auto expected = std::vector<LaptimeT> { 10, 20 };
    QCOMPARE(expected, result);
}

QTEST_APPLESS_MAIN(UnitTests)


#include "TestsCommon.moc"
