// Copyright 2015 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of LFSTop.
//
// LFSTop is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LFSTop is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LFSTop.  If not, see <http://www.gnu.org/licenses/>.


#include <QtTest>
#include <QByteArray>

#include "Utils/OverwritableLimitedContainer.hpp"

class OverwritableLimitedContainerTest : public QObject
{
    Q_OBJECT
private Q_SLOTS:
    void Test_data ();
    void Test ();
};


void OverwritableLimitedContainerTest::Test_data () {
    QTest::addColumn<std::vector<QByteArray>>("data");

    std::vector<QByteArray> row1;
    for (int i = 0; i < 30; ++i) {
        row1.push_back(QByteArray("string ").append(QByteArray::number(i)));
    }

    QTest::newRow("0")
            << row1;
}

void OverwritableLimitedContainerTest::Test () {
    QFETCH(std::vector<QByteArray>, data);

    const size_t limit = 10;

    OverwritableLimitedContainer<QByteArray> container(limit);

    for (const QByteArray &el : data) {
        container.push_back(el);
    }

    for (size_t i = 0; i < container.size(); ++i) {
        QCOMPARE(container[i], data[data.size() - limit + i]);
    }
}


QTEST_APPLESS_MAIN(OverwritableLimitedContainerTest)

#include "OverwritableLimitedContainerTest.moc"
