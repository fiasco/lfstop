// Copyright 2015 Vitalii Minnakhmetov <restlessmonkey@ya.ru>
//
// This file is part of LFSTop.
//
// LFSTop is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LFSTop is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with LFSTop.  If not, see <http://www.gnu.org/licenses/>.


#include <algorithm>

#include <QList>
#include <QString>
#include <QtTest>


#include "Databases/LapsDb.hpp"
#include "Utils/StringUtils.hpp"
#include "LfsInsimUtils.hpp"
#include "Utils/MathUtils.hpp"

#include "Utils.hpp"

#include <string>

class UnitTests : public QObject
{
    Q_OBJECT

public:
    UnitTests();
    ~UnitTests();

private Q_SLOTS:
    void ParserTest_data();
    void ParserTest();

    void TokenizerTest_data();
    void TokenizerTest();
    void TokenizerTest2();

    void QByteArrayTokenizerTest_data();
    void QByteArrayTokenizerTest();
    void QByteArrayTokenizerTest2();

    void LapsdbTest_data();
    void LapsdbTest();

    void LapsdbClassesTest_data();
    void LapsdbClassesTest();

    void LapsdbTestNonClean_data();
    void LapsdbTestNonClean();

    void LapsdbTestNonCleanBestlaps_data();
    void LapsdbTestNonCleanBestlaps();

    void LapsdbTestNonCleanClassLaps_data();
    void LapsdbTestNonCleanClassLaps();

    void LapsdbTestNonCleanClassBestlaps_data();
    void LapsdbTestNonCleanClassBestlaps();

    void MathUtilsTest_data();
    void MathUtilsTest();
};

UnitTests::UnitTests()
{
}

UnitTests::~UnitTests()
{
    QFile("SO6.dat").remove();
    QFile("SO6_Classes.dat").remove();
    QFile("Benchmark.dat").remove();
    QFile("ClassesBenchmark.dat").remove();
}


typedef std::vector<QPair<bool, QByteArray>> LfsCarCommandParserReturnValue;

Q_DECLARE_METATYPE(LfsCarCommandParserReturnValue)
namespace QTest {
    template<>
    char *toString(const LfsCarCommandParserReturnValue &list)
    {
        QByteArray ba;
        ba.append('<');
        for (const QPair<bool, QByteArray> p : list) {
            ba.append('<');
            ba.append(p.first ? "true : " : "false : ");
            ba.append(p.second);
            ba.append('>');
        }
        ba.append('>');
        return qstrdup(ba.data());
    }
}



void UnitTests::ParserTest_data() {
    QTest::addColumn<QByteArray>("str");
    QTest::addColumn<LfsCarCommandParserReturnValue> ("result");

    QTest::newRow("0")
            << QByteArray("ROAD-RAC-FZ5-LX6")
            << LfsCarCommandParserReturnValue
    { {true, "ROAD"},
      {false, "RAC"},
      {false, "FZ5"},
      {false, "LX6"} };

    QTest::newRow("1")
            << QByteArray("TBO+LX4+XRT-FXO")
            << LfsCarCommandParserReturnValue
    { {true, "TBO"},
      {true, "LX4"},
      {true, "XRT"},
      {false, "FXO"} };

    QTest::newRow("3")
            << QByteArray("ROAD")
            << LfsCarCommandParserReturnValue
    { {true, "ROAD"} };
}

#include <iostream> // TODO tmp
void UnitTests::ParserTest() {
    QFETCH(QByteArray, str);
    QFETCH(LfsCarCommandParserReturnValue, result);

    std::cout << QTest::toString(LfsInsimUtils::getCarCommandArguments(str))
              << " and " << QTest::toString(result) << std::endl;

    QCOMPARE(LfsInsimUtils::getCarCommandArguments(str), result);
}

void UnitTests::TokenizerTest_data()
{
    QTest::addColumn<std::string>("str");
    QTest::addColumn<QList<std::string>> ("tokens");
    QTest::addColumn<size_t> ("num_tokens");


    QTest::newRow("0")
            << std::string("oneword")
            << QList<std::string> { "oneword" }
            << size_t(1);

    QTest::newRow("1")
            << std::string("two  words")
            << QList<std::string> { "two", "words" }
            << size_t(2);

    QTest::newRow("2")
            << std::string(" three words here  ")
            << QList<std::string> { "three", "words", "here" }
            << size_t(3);

    QTest::newRow("3")
            << std::string(" hell  of     a lot words here  ")
            << QList<std::string> { "hell", "of", "a", "lot", "words", "here" }
            << size_t(6);
}

Q_DECLARE_METATYPE(std::string)
namespace QTest {
    template<>
    char *toString(const std::string &pString)
    {
        QByteArray ba = QByteArray::fromStdString(pString);
        return qstrdup(ba.data());
    }
}

void UnitTests::TokenizerTest()
{
    QFETCH(std::string, str);
    QFETCH(QList<std::string>, tokens);
    QFETCH(size_t, num_tokens);

    StringUtils::Tokenizer tokenizer (str);
    QCOMPARE(tokenizer.getNumWords(), num_tokens);

    for (size_t i = 0; i < num_tokens; ++i) {
        QCOMPARE(tokenizer.getWord(i), tokens.at(static_cast<int>(i)));
    }

    size_t middle = num_tokens / 2;
    std::string a_word = tokens[static_cast<int>(middle)];
    std::string rest_got = tokenizer.getRestAt(middle);
    std::string rest = str.substr(str.find(a_word));
    QCOMPARE(rest_got, rest);
}

void UnitTests::TokenizerTest2() {
    StringUtils::Tokenizer tokenizer ("1 2 3 4 get this   part 3 4");
    QCOMPARE(tokenizer.getWords(4, 3), std::string("get this   part"));
}

void UnitTests::QByteArrayTokenizerTest_data()
{
    QTest::addColumn<QByteArray>("str");
    QTest::addColumn<QList<QByteArray>> ("tokens");
    QTest::addColumn<size_t> ("num_tokens");


    QTest::newRow("0")
            << QByteArray("oneword")
            << QList<QByteArray> { "oneword" }
            << size_t(1);

    QTest::newRow("1")
            << QByteArray("two  words")
            << QList<QByteArray> { "two", "words" }
            << size_t(2);

    QTest::newRow("2")
            << QByteArray(" three words here  ")
            << QList<QByteArray> { "three", "words", "here" }
            << size_t(3);

    QTest::newRow("3")
            << QByteArray(" hell  of     a lot words here  ")
            << QList<QByteArray> { "hell", "of", "a", "lot", "words", "here" }
            << size_t(6);

    QTest::newRow("3")
            << QByteArray(" hell  of     a lot words \"quo   ted\" here  ")
            << QList<QByteArray> { "hell", "of", "a", "lot", "words",
                                   "quo   ted", "here" }
            << size_t(7);
}

void UnitTests::QByteArrayTokenizerTest()
{
    QFETCH(QByteArray, str);
    QFETCH(QList<QByteArray>, tokens);
    QFETCH(size_t, num_tokens);



    StringUtils::QByteArrayTokenizer tokenizer (str);

    for (size_t i = 0; i < tokenizer.getNumWords(); ++i) {
        std::cout << ":" << tokenizer.getWord(i).toStdString();
    }
    std::cout << std::endl;

    QCOMPARE(tokenizer.getNumWords(), num_tokens);

    for (size_t i = 0; i < num_tokens; ++i) {
        QCOMPARE(tokenizer.getWord(i), tokens.at(static_cast<int>(i)));
    }

    size_t middle = num_tokens / 2;
    QByteArray a_word = tokens[static_cast<int>(middle)];
    QByteArray rest_got = tokenizer.getRestAt(middle);

    QByteArray rest = str.mid(str.indexOf(a_word));
    QCOMPARE(rest_got, rest);
}

void UnitTests::QByteArrayTokenizerTest2() {
    StringUtils::QByteArrayTokenizer tokenizer ("1 2 3 4 get this   part 3 4");
    QCOMPARE(tokenizer.getWords(4, 3), QByteArray("get this   part"));
}

void UnitTests::LapsdbTest_data()
{
    QTest::addColumn<QString>("track");
    QTest::addColumn<QList<TestLaptimeData> > ("data");


    QTest::newRow("0")
            << QString("SO6")

            << QList<TestLaptimeData>{
               // tyre 3 top:
               TestLaptimeData{ 1, 3, { { 20}, 23, "1" } }, // #1
               TestLaptimeData{ 1, 3, { { 20}, 64, "2" } }, // #2
               TestLaptimeData{ 1, 3, { { 20}, 66, "3" } }, // #3
               TestLaptimeData{ 1, 3, { { 20}, 68, "4" } }, // #4
               TestLaptimeData{ 1, 3, { { 20}, 69, "4.5" } }, // #4.5
               TestLaptimeData{ 1, 3, { { 20}, 70, "5" } }, // #5
               TestLaptimeData{ 1, 3, { { 20}, 120, "6" } }, // #6

               // tyre 2 top:
               TestLaptimeData{ 1, 2, { { 20}, 20, "1" } }, // #1
               TestLaptimeData{ 1, 2, { { 20}, 61, "2" } }, // #2
               TestLaptimeData{ 1, 2, { { 20}, 62, "3" } }, // #3
               TestLaptimeData{ 1, 2, { { 20}, 64, "3.5" } }, // #3.5
               TestLaptimeData{ 1, 2, { { 20}, 65, "4" } }, // #4
               TestLaptimeData{ 1, 2, { { 20}, 66, "5" } }, // #5
               TestLaptimeData{ 1, 2, { { 20}, 120, "6" } }, // #6

               // tyre 3 some random stuff
               TestLaptimeData{ 1, 3, { { 20}, 26, "1" } }, // #1
               TestLaptimeData{ 1, 3, { { 20}, 78, "2" } }, // #2
               TestLaptimeData{ 1, 3, { { 20}, 69, "3" } }, // #3
               TestLaptimeData{ 1, 3, { { 20}, 120, "4" } }, // #4
               TestLaptimeData{ 1, 3, { { 20}, 79, "4.5" } }, // #4.5
               TestLaptimeData{ 1, 3, { { 20}, 200, "5" } }, // #5
               TestLaptimeData{ 1, 3, { { 20}, 180, "6" } }, // #6

               // tyre 2 random stuff
               TestLaptimeData{ 1, 2, { { 20}, 33, "1" } }, // #1
               TestLaptimeData{ 1, 2, { { 20}, 77, "2" } }, // #2
               TestLaptimeData{ 1, 2, { { 20}, 333, "3" } }, // #3
               TestLaptimeData{ 1, 2, { { 20}, 222, "3.5" } }, // #3.5
               TestLaptimeData{ 1, 2, { { 20}, 333, "4" } }, // #4
               TestLaptimeData{ 1, 2, { { 20}, 120, "5" } }, // #5
               TestLaptimeData{ 1, 2, { { 20}, 190, "6" } }, // #6

    };
}

void UnitTests::LapsdbTest()
{
    QFETCH(QString, track);
    QFETCH(QList<TestLaptimeData>, data);

    Log l("meh");
    LapsDb db(l);
    db.loadFile(track);

    std::random_shuffle(data.begin(), data.end());

    for (auto el : data) {
        db.addLaptime("top", el.car, el.tyres, el.lt);
    }

    QCOMPARE(db.getLaptimesCount("top", CarT(1), TyreT::fromOne(2)), 7);
    QCOMPARE(db.getLaptimesCount("top", CarT(1), TyreT::fromOne(3)), 7);

    QCOMPARE(db.getLaptime("top", 0, CarT(1), TyreT::fromOne(3), true).username, QByteArray("1"));
    QCOMPARE(db.getLaptime("top", 1, CarT(1), TyreT::fromOne(3), true).username, QByteArray("2"));
    QCOMPARE(db.getLaptime("top", 2, CarT(1), TyreT::fromOne(3), true).username, QByteArray("3"));
    QCOMPARE(db.getLaptime("top", 3, CarT(1), TyreT::fromOne(3), true).username, QByteArray("4"));
    QCOMPARE(db.getLaptime("top", 4, CarT(1), TyreT::fromOne(3), true).username, QByteArray("4.5"));
    QCOMPARE(db.getLaptime("top", 5, CarT(1), TyreT::fromOne(3), true).username, QByteArray("5"));
    QCOMPARE(db.getLaptime("top", 6, CarT(1), TyreT::fromOne(3), true).username, QByteArray("6"));

    QCOMPARE(db.getLaptime("top", 0, CarT(1), TyreT::fromOne(2), true).username, QByteArray("1"));
    QCOMPARE(db.getLaptime("top", 1, CarT(1), TyreT::fromOne(2), true).username, QByteArray("2"));
    QCOMPARE(db.getLaptime("top", 2, CarT(1), TyreT::fromOne(2), true).username, QByteArray("3"));
    QCOMPARE(db.getLaptime("top", 3, CarT(1), TyreT::fromOne(2), true).username, QByteArray("3.5"));
    QCOMPARE(db.getLaptime("top", 4, CarT(1), TyreT::fromOne(2), true).username, QByteArray("4"));
    QCOMPARE(db.getLaptime("top", 5, CarT(1), TyreT::fromOne(2), true).username, QByteArray("5"));
    QCOMPARE(db.getLaptime("top", 6, CarT(1), TyreT::fromOne(2), true).username, QByteArray("6"));

    const TyreT any = TyreT::unknownTyres();
    QCOMPARE(db.getLaptime("top", 0, CarT(1), any,true).username, QByteArray("1"));
    QCOMPARE(db.getLaptime("top", 1, CarT(1), any,true).username, QByteArray("2"));
    QCOMPARE(db.getLaptime("top", 2, CarT(1), any,true).username, QByteArray("3"));
    QCOMPARE(db.getLaptime("top", 3, CarT(1), any,true).username, QByteArray("3.5"));
    QCOMPARE(db.getLaptime("top", 4, CarT(1), any,true).username, QByteArray("4"));
    QCOMPARE(db.getLaptime("top", 5, CarT(1), any,true).username, QByteArray("5"));
    QCOMPARE(db.getLaptime("top", 6, CarT(1), any,true).username, QByteArray("4.5"));
    QCOMPARE(db.getLaptime("top", 7, CarT(1), any,true).username, QByteArray("6"));
}




#include <iostream> // TODO tmp




void UnitTests::LapsdbClassesTest_data()
{
    QTest::addColumn<QString>("track");
    QTest::addColumn<QList<TestLaptimeData> > ("data");


    QTest::newRow("0")
            << QString("SO6_Classes")

            << QList<TestLaptimeData>{
               // car 1 top:
               TestLaptimeData{ 12, 3, { { 20}, 23, "1_1" } }, // #1
               TestLaptimeData{ 12, 2, { { 20}, 64, "2_1" } }, // #5
               TestLaptimeData{ 12, 3, { { 20}, 66, "3_1" } }, // #6

               // car 2 top:
               TestLaptimeData{ 13, 2, { { 20}, 24, "1_2" } }, // #2
               TestLaptimeData{ 13, 3, { { 20}, 62, "2_2" } }, // #4
               TestLaptimeData{ 13, 2, { { 20}, 67, "3_2" } }, // #7

               // car 3 top:
               TestLaptimeData{ 14, 2, { { 20}, 26, "1_3" } }, // #3
               TestLaptimeData{ 14, 2, { { 20}, 68, "2_3" } }, // #8
               TestLaptimeData{ 14, 3, { { 20}, 69, "3_3" } }, // #9

               // car 3 bad laps:
               TestLaptimeData{ 14, 3, { { 20}, 90, "1_3" } },
               TestLaptimeData{ 14, 3, { { 20}, 93, "2_3" } },
               TestLaptimeData{ 14, 3, { { 20}, 400, "3_3" } },
               TestLaptimeData{ 14, 3, { { 20}, 92, "1_3" } },
               TestLaptimeData{ 14, 3, { { 20}, 99, "2_3" } },
               TestLaptimeData{ 14, 3, { { 20}, 990, "3_3" } },

               // etc
               TestLaptimeData{ 13, 2, { { 20}, 90, "1_2" } }, // #2
               TestLaptimeData{ 13, 3, { { 20}, 400, "2_2" } }, // #4
               TestLaptimeData{ 13, 2, { { 20}, 69, "3_2" } }, // #7
    };
}

void UnitTests::LapsdbClassesTest()
{
    QFETCH(QString, track);
    QFETCH(QList<TestLaptimeData>, data);

    Log l("meh");
    LapsDb db(l);
    db.addClass("WTF", QSet<QByteArray>{"FXR","XRR","FZR"});

    db.loadFile(track);

    std::random_shuffle(data.begin(), data.end());

    for (auto el : data) {
        db.addLaptime("top", el.car, el.tyres, el.lt);

    }

    const TyreT any = TyreT::unknownTyres();
    QCOMPARE(db.getClassLaptimesCount("top", ClassT(0), any), 9);
    QCOMPARE(db.getClassLaptimesCount("top", ClassT(0), TyreT::fromOne(2)), 5);
    QCOMPARE(db.getClassLaptimesCount("top", ClassT(0), TyreT::fromOne(3)), 6);

    QCOMPARE(db.getClassLaptime("top", 0,ClassT(0), any, true).username,
             QByteArray("1_1"));
    QCOMPARE(db.getClassLaptime("top", 1,ClassT(0), any, true).username,
             QByteArray("1_2"));
    QCOMPARE(db.getClassLaptime("top", 2,ClassT(0), any, true).username,
             QByteArray("1_3"));
    QCOMPARE(db.getClassLaptime("top", 3,ClassT(0), any, true).username,
             QByteArray("2_2"));
    QCOMPARE(db.getClassLaptime("top", 4,ClassT(0), any, true).username,
             QByteArray("2_1"));
    QCOMPARE(db.getClassLaptime("top", 5,ClassT(0), any, true).username,
             QByteArray("3_1"));
    QCOMPARE(db.getClassLaptime("top", 6,ClassT(0), any, true).username,
             QByteArray("3_2"));
    QCOMPARE(db.getClassLaptime("top", 7,ClassT(0), any, true).username,
             QByteArray("2_3"));
    QCOMPARE(db.getClassLaptime("top", 8,ClassT(0), any, true).username,
             QByteArray("3_3"));
}


void UnitTests::LapsdbTestNonClean_data()
{
    QTest::addColumn<QString>("track");
    QTest::addColumn<QList<TestLaptimeData> > ("data");


    QTest::newRow("0")
            << QString("SO6")

            << QList<TestLaptimeData>{

               // significant data
               TestLaptimeData{ 1, 3, { { 20}, 28, "john" }, false },
               TestLaptimeData{ 1, 3, { { 20}, 34, "bob" }, false },
               TestLaptimeData{ 1, 3, { { 20}, 37, "jake" }, false },
               TestLaptimeData{ 1, 3, { { 20}, 40, "eugene" }, false },

               TestLaptimeData{ 1, 3, { { 20}, 30, "john" }, true },
               TestLaptimeData{ 1, 3, { { 20}, 32, "bob" }, true },
               TestLaptimeData{ 1, 3, { { 20}, 35, "alex" }, true },
               TestLaptimeData{ 1, 3, { { 20}, 38, "jake" }, true },

               // random data (slower than significant data)
               TestLaptimeData{ 1, 3, { { 20}, 29, "john" }, false },
               TestLaptimeData{ 1, 3, { { 20}, 39, "bob" }, false },
               TestLaptimeData{ 1, 3, { { 20}, 40, "jake" }, false },
               TestLaptimeData{ 1, 3, { { 20}, 41, "eugene" }, false },

               TestLaptimeData{ 1, 3, { { 20}, 38, "john" }, true },
               TestLaptimeData{ 1, 3, { { 20}, 39, "bob" }, true },
               TestLaptimeData{ 1, 3, { { 20}, 36, "alex" }, true },
               TestLaptimeData{ 1, 3, { { 20}, 4441, "jake" }, true },
    };
}

void UnitTests::LapsdbTestNonClean()
{
    QFETCH(QString, track);
    QFETCH(QList<TestLaptimeData>, data);

    for (int i = 0; i < 10; ++i) {

        Log l("meh");
        LapsDb db(l);

        std::random_shuffle(data.begin(), data.end());

        for (auto el : data) {
            db.addLaptime("top", el.car, el.tyres, el.lt, el.clean);
        }

        QCOMPARE(db.getLaptimesCount("top", CarT(1), TyreT::fromOne(3), true), 4);
        QCOMPARE(db.getLaptimesCount("top", CarT(1), TyreT::fromOne(3), false), 5);

        QCOMPARE(db.getLaptime("top", 0, CarT(1), TyreT::fromOne(3), true).username,
                 QByteArray("john"));
        QCOMPARE(db.getLaptime("top", 1, CarT(1), TyreT::fromOne(3), true).username,
                 QByteArray("bob"));
        QCOMPARE(db.getLaptime("top", 2, CarT(1), TyreT::fromOne(3), true).username,
                 QByteArray("alex"));
        QCOMPARE(db.getLaptime("top", 3, CarT(1), TyreT::fromOne(3), true).username,
                 QByteArray("jake"));

        QCOMPARE(db.getLaptime("top", 0, CarT(1), TyreT::fromOne(3), false).username,
                 QByteArray("john"));
        QCOMPARE(db.getLaptime("top", 1, CarT(1), TyreT::fromOne(3), false).username,
                 QByteArray("bob"));
        QCOMPARE(db.getLaptime("top", 2, CarT(1), TyreT::fromOne(3), false).username,
                 QByteArray("alex"));
        QCOMPARE(db.getLaptime("top", 3, CarT(1), TyreT::fromOne(3), false).username,
                 QByteArray("jake"));
        QCOMPARE(db.getLaptime("top", 4, CarT(1), TyreT::fromOne(3), false).username,
                 QByteArray("eugene"));

        db.wipe("top");

    }
}

void UnitTests::LapsdbTestNonCleanBestlaps_data()
{
    QTest::addColumn<QString>("track");
    QTest::addColumn<QList<TestLaptimeData> > ("data");


    QTest::newRow("0")
            << QString("SO6")

            << QList<TestLaptimeData>{

               // significant data
               TestLaptimeData{ 1, 3, { { 20}, 28, "john" }, false },
               TestLaptimeData{ 1, 3, { { 20}, 34, "bob" }, false },
               TestLaptimeData{ 1, 3, { { 20}, 37, "jake" }, false },
               TestLaptimeData{ 1, 3, { { 20}, 40, "eugene" }, false },

               TestLaptimeData{ 1, 3, { { 20}, 30, "john" }, true },
               TestLaptimeData{ 1, 3, { { 20}, 32, "bob" }, true },
               TestLaptimeData{ 1, 3, { { 20}, 35, "alex" }, true },
               TestLaptimeData{ 1, 3, { { 20}, 38, "jake" }, true },

               TestLaptimeData{ 1, 4, { { 20}, 30, "bob" }, false},
               TestLaptimeData{ 1, 4, { { 20}, 31, "john" }, false },
               TestLaptimeData{ 1, 4, { { 20}, 32, "adrian" }, false },
               TestLaptimeData{ 1, 4, { { 20}, 36, "alex" }, false },
               TestLaptimeData{ 1, 4, { { 20}, 40, "jake" }, false },

               TestLaptimeData{ 1, 4, { { 20}, 32, "john" }, true },
               TestLaptimeData{ 1, 4, { { 20}, 34, "bob" }, true },
               TestLaptimeData{ 1, 4, { { 20}, 36, "adrian" }, true },
               TestLaptimeData{ 1, 4, { { 20}, 37, "alex" }, true },
               TestLaptimeData{ 1, 4, { { 20}, 39, "jake" }, true },

               // slower data

               TestLaptimeData{ 1, 3, { { 20}, 29, "john" }, false },
               TestLaptimeData{ 1, 3, { { 20}, 344, "bob" }, false },
               TestLaptimeData{ 1, 3, { { 20}, 39, "jake" }, false },
               TestLaptimeData{ 1, 3, { { 20}, 48, "eugene" }, false },

               TestLaptimeData{ 1, 3, { { 20}, 32, "john" }, true },
               TestLaptimeData{ 1, 3, { { 20}, 33, "bob" }, true },
               TestLaptimeData{ 1, 3, { { 20}, 36, "alex" }, true },
               TestLaptimeData{ 1, 3, { { 20}, 39, "jake" }, true },

               TestLaptimeData{ 1, 4, { { 20}, 300, "bob" }, false},
               TestLaptimeData{ 1, 4, { { 20}, 311, "john" }, false },
               TestLaptimeData{ 1, 4, { { 20}, 322, "adrian" }, false },
               TestLaptimeData{ 1, 4, { { 20}, 363, "alex" }, false },
               TestLaptimeData{ 1, 4, { { 20}, 41, "jake" }, false },

               TestLaptimeData{ 1, 4, { { 20}, 33, "john" }, true },
               TestLaptimeData{ 1, 4, { { 20}, 39, "bob" }, true },
               TestLaptimeData{ 1, 4, { { 20}, 39, "adrian" }, true },
               TestLaptimeData{ 1, 4, { { 20}, 39, "alex" }, true },
               TestLaptimeData{ 1, 4, { { 20}, 44, "jake" }, true },
    };
}

void UnitTests::LapsdbTestNonCleanBestlaps()
{
    QFETCH(QString, track);
    QFETCH(QList<TestLaptimeData>, data);

    for (int i = 0; i < 10; ++i) {
        Log l("meh");
        LapsDb db(l);

        std::random_shuffle(data.begin(), data.end());

        for (auto el : data) {
            db.addLaptime("top", el.car, el.tyres, el.lt, el.clean);
        }

        QCOMPARE(db.getLaptimesCount("top", CarT(1), TyreT::unknownTyres(), true), 5);
        QCOMPARE(db.getLaptimesCount("top", CarT(1), TyreT::unknownTyres(), false), 6);


        QCOMPARE(db.getLaptime("top", 0, CarT(1), TyreT::unknownTyres(), false).username,
                 QByteArray("john"));
        QCOMPARE(db.getLaptime("top", 1, CarT(1), TyreT::unknownTyres(), false).username,
                 QByteArray("bob"));
        QCOMPARE(db.getLaptime("top", 2, CarT(1), TyreT::unknownTyres(), false).username,
                 QByteArray("adrian"));
        QCOMPARE(db.getLaptime("top", 3, CarT(1), TyreT::unknownTyres(), false).username,
                 QByteArray("alex"));
        QCOMPARE(db.getLaptime("top", 4, CarT(1), TyreT::unknownTyres(), false).username,
                 QByteArray("jake"));
        QCOMPARE(db.getLaptime("top", 5, CarT(1), TyreT::unknownTyres(), false).username,
                 QByteArray("eugene"));

        db.wipe("top");

    }
}


void UnitTests::LapsdbTestNonCleanClassLaps_data()
{
    QTest::addColumn<QString>("track");
    QTest::addColumn<QList<TestLaptimeData>> ("data");

    QTest::newRow("0")
            << QString("SO6")

            << QList<TestLaptimeData>{

               // car 1 clean
               TestLaptimeData{ 12,  3, { { 20}, 28, "alex" }, true },
               TestLaptimeData{ 12,  3, { { 20}, 31, "bill" }, true },
               TestLaptimeData{ 12,  3, { { 20}, 35, "jake" }, true },
               TestLaptimeData{ 12,  3, { { 20}, 41, "dale" }, true },

               // car 2 nonclean
               TestLaptimeData{ 12,  3, { { 20}, 30, "bill" }, false },
               TestLaptimeData{ 12,  3, { { 20}, 34, "jake" }, false },
               TestLaptimeData{ 12,  3, { { 20}, 39, "lary" }, false },

               // car 2 clean
               TestLaptimeData{ 13,  3, { { 20}, 29, "alex" }, true },
               TestLaptimeData{ 13,  3, { { 20}, 33, "jake" }, true },
               TestLaptimeData{ 13,  3, { { 20}, 37, "ivan" }, true },
               TestLaptimeData{ 13,  3, { { 20}, 38, "jeff" }, true },

               // car 2 nonclean
               TestLaptimeData{ 13,  3, { { 20}, 32, "jake" }, false },
               TestLaptimeData{ 13,  3, { { 20}, 36, "jeff" }, false },
               TestLaptimeData{ 13,  3, { { 20}, 42, "pete" }, false },
               TestLaptimeData{ 13,  3, { { 20}, 45, "stan" }, false },
    };
}

void UnitTests::LapsdbTestNonCleanClassLaps()
{
    QFETCH(QString, track);
    QFETCH(QList<TestLaptimeData>, data);

    for (int i = 0; i < 100; ++i) {
        QFile::remove("wut1.dat");

        Log l("meh");
        LapsDb db(l);
        db.addClass("yo", QSet<QByteArray>{"FXR","XRR","FZR"});
        db.loadFile("wut1");
        ClassT cl;

        std::random_shuffle(data.begin(), data.end());

        for (auto el : data) {
            db.addLaptime("top", el.car, el.tyres, el.lt, el.clean);
        }

        QCOMPARE(db.getClassLaptimesCount("top", cl, TyreT::fromOne(3), true), 8);
        QCOMPARE(db.getClassLaptimesCount("top", cl, TyreT::fromOne(3), false), 11);

        QList<QByteArray> non_clean_usernames =
        { "alex",
          "alex",
          "bill",
          "jake",
          "jake",
          "jeff",
          "ivan",
          "lary",
          "dale",
          "pete",
          "stan" };


        for (int j = 0; j < db.getClassLaptimesCount("top", cl, TyreT::fromOne(3), false); ++j) {
            QCOMPARE(db.getClassLaptime("top", j, cl, TyreT::fromOne(3), false).username,
                     non_clean_usernames[j]);
        }

        db.wipe("top");


    }
}

void UnitTests::LapsdbTestNonCleanClassBestlaps_data()
{
    QTest::addColumn<QString>("track");
    QTest::addColumn<QList<TestLaptimeData>> ("data");

    QTest::newRow("0")
            << QString("SO6")

            << QList<TestLaptimeData>{

               TestLaptimeData{ 12,  1, { { 20}, 20, "alex" }, true },
               TestLaptimeData{ 12,  1, { { 20}, 30, "bill" }, true },
               TestLaptimeData{ 12,  1, { { 20}, 40, "jeff" }, true },

               TestLaptimeData{ 12,  2, { { 20}, 22, "alex" }, true },
               TestLaptimeData{ 12,  2, { { 20}, 32, "bill" }, true },
               TestLaptimeData{ 12,  2, { { 20}, 42, "ozzy" }, true },

               TestLaptimeData{ 13,  1, { { 20}, 18, "adam" }, true },
               TestLaptimeData{ 13,  1, { { 20}, 25, "jake" }, true },
               TestLaptimeData{ 13,  1, { { 20}, 35, "bill" }, true },
               TestLaptimeData{ 13,  1, { { 20}, 35, "bill" }, false }, //

               TestLaptimeData{ 13,  2, { { 20}, 23, "adam" }, true },
               TestLaptimeData{ 13,  2, { { 20}, 27, "jake" }, true },
               TestLaptimeData{ 13,  2, { { 20}, 37, "ivan" }, true },


               TestLaptimeData{ 12,  1, { { 20}, 19, "alex" }, false },
               TestLaptimeData{ 12,  1, { { 20}, 29, "bill" }, false },
               TestLaptimeData{ 12,  1, { { 20}, 41, "sean" }, false },

               TestLaptimeData{ 12,  2, { { 20}, 21, "alex" }, false },
               TestLaptimeData{ 12,  2, { { 20}, 33, "bill" }, false },
               TestLaptimeData{ 12,  2, { { 20}, 39, "ozzy" }, false },

               TestLaptimeData{ 13,  1, { { 20}, 18, "adam" }, false },
               TestLaptimeData{ 13,  1, { { 20}, 24, "jake" }, false },
               TestLaptimeData{ 13,  1, { { 20}, 26, "olli" }, false },

               TestLaptimeData{ 13,  2, { { 20}, 17, "adam" }, false },
               TestLaptimeData{ 13,  2, { { 20}, 28, "jake" }, false },
               TestLaptimeData{ 13,  2, { { 20}, 34, "ivan" }, false },
    };
}

void UnitTests::LapsdbTestNonCleanClassBestlaps()
{
    QFETCH(QString, track);
    QFETCH(QList<TestLaptimeData>, data);

    for (int i = 0; i < 1; ++i) {
        QFile::remove("wut1.dat");

        Log l("meh");
        LapsDb db(l);
        db.addClass("yo", QSet<QByteArray>{"FXR","XRR","FZR"});
        db.loadFile("wut1");
        ClassT cl;

        std::random_shuffle(data.begin(), data.end());

        for (auto el : data) {
            db.addLaptime("top", el.car, el.tyres, el.lt, el.clean);
        }

        QCOMPARE(db.getClassLaptimesCount("top", cl, TyreT::unknownTyres(), false), 10);
        QCOMPARE(db.getClassLaptimesCount("top", cl, TyreT::unknownTyres(), true), 8);

        // TODO: make sure adam 18 is actually clean in non-clean table, coz
        // we want to have clean laps being in higher priority, issue #55
        const QVector<QPair<QByteArray, bool>> non_clean_usernames =
            { { "adam", false },
              { "alex", false },
              { "jake", false },
              { "olli", false },
              { "bill", false },
              { "ivan", false },
              { "bill", true },
              { "ozzy", false },
              { "jeff", true},
              { "sean", false } };

        const QVector<QByteArray> clean_usernames =
            { "adam",
              "alex",
              "jake",
              "bill",
              "bill",
              "ivan",
              "jeff",
              "ozzy" };


        const TyreT any = TyreT::unknownTyres();

        for (int j = 0; j < db.getClassLaptimesCount("top", cl, any, false); ++j) {
            const auto lt = db.getClassLaptime("top", j, cl, any, false);
            QCOMPARE(lt.username, non_clean_usernames[j].first);
            QCOMPARE(lt.clean, non_clean_usernames[j].second);

        }

        for (int j = 0; j < db.getClassLaptimesCount("top", cl, any, true); ++j) {
            QCOMPARE(db.getClassLaptime("top", j, cl, any, true).username,
                     clean_usernames[j]);
        }

        db.wipe("top");
    }
}

void UnitTests::MathUtilsTest_data()
{
    QTest::addColumn<float>("a");
    QTest::addColumn<float>("b");
    QTest::addColumn<float>("result");

    using MathUtils::degreesToRadians;

    QTest::newRow("1") << 10.f << 350.f << 20.f;
    QTest::newRow("2") << 20.f << 180.f << 160.f;
    QTest::newRow("3") << 190.f << 20.f << 170.f;
}

void UnitTests::MathUtilsTest ()
{
    QFETCH(float, a);
    QFETCH(float, b);
    QFETCH(float, result);

    using MathUtils::angleDistance2f;
    using MathUtils::degreesToRadians;

    QCOMPARE(angleDistance2f(degreesToRadians(a), degreesToRadians(b)),
             degreesToRadians(result));
}



QTEST_APPLESS_MAIN(UnitTests)


#include "TestLapsDb.moc"
